/* Ceci est le fichier javascript de YASSINE */
/* FICHIER MODIFIE */

$(document).ready(function () {

    // Change Password Form
    $("#change-password-form").submit(function () {

        console.log('change-password-form : preparing query ... ');

        $('#change-password-response').html("<p align=\"center\"> Changing your password ... please wait.<br><br><img src=\"../../..//etudiants/yassine/img/loading.gif\"><p>");

        var url = $("#change-password-form").attr("action");

        //start send the post request
        $.post(url, {
            user_id: $("#change_password_user_id").val(),
            old_password: $("#change_password_oldpassword").val(),
            new_password: $("#change_password_password1").val(),
        }, function (data) {
            //the response is in the data variable

            if (data.responseCode == 200) {
                $('#change-password-response').html(data.message);
            }
            else {
                //if we got to this point we know that the controller
                //did not return a json_encoded array. We can assume that           
                //an unexpected PHP error occured
                alert("Yassine : Something weird is going on :( ");

                //if you want to print the error:
                $('#change-password-response').html(data);
            }
        });

        //we dont what the browser to submit the form
        return false;
    });

    $("#update-account-form").submit(function () {

        console.log('update-account-form : preparing query ... ');

        $('#basic_account_response').html("<p align=\"center\"> Updating your account information ... please wait.<br><br><img src=\"../../..//etudiants/yassine/img/loading.gif\"><p>");

        var url = $("#update-account-form").attr("action");

        //start send the post request
        $.post(url, {
            basic_account_user_id: $("#basic_account_user_id").val(),
            basic_account_first_name: $("#basic_account_first_name").val(),
            basic_account_last_name: $("#basic_account_last_name").val(),
            basic_account_email: $("#basic_account_email").val(),
            basic_account_day: $("#basic_account_day").val(),
            basic_account_month: $("#basic_account_month").val(),
            basic_account_year: $("#basic_account_year").val(),
        }, function (data) {
            //the response is in the data variable

            if (data.responseCode == 200) {
                $('#basic_account_response').html(data.message);
                
                committed_first_name = $("#basic_account_first_name").val();
                committed_last_name = $("#basic_account_last_name").val(); 
                committed_birthday_day = $("#basic_account_day").val();
                committed_birthday_month = $("#basic_account_month").val();
                committed_birthday_year = $("#basic_account_year").val();
                
                committed_email = data.committed_email;
                
                console.log('\n committed_first_name : ' + committed_first_name + '\n, committed_last_name : ' + committed_last_name + '\n, committed_email : ' + committed_email + '\n, committed_birthday_year : ' + committed_birthday_year + '\n, committed_birthday_month : ' + committed_birthday_month + '\n, committed_birthday_day : ' +  committed_birthday_day);
                
                document.getElementById("basic_account_first_name").value = committed_first_name;
                document.getElementById("basic_account_last_name").value = committed_last_name;
                document.getElementById("basic_account_email").value = committed_email;
                document.getElementById("basic_account_year").value = committed_birthday_year;
                document.getElementById("basic_account_month").value = committed_birthday_month;
                document.getElementById("basic_account_day").value = committed_birthday_day;
            }
            else {
                //if we got to this point we know that the controller
                //did not return a json_encoded array. We can assume that           
                //an unexpected PHP error occured
                alert("Yassine : Something weird is going on :( ");

                //if you want to print the error:
                $('#basic_account_response').html(data);
            }
        });

        //we dont what the browser to submit the form
        return false;
    });



    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

});
