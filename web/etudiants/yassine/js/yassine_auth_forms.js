/* FICHIER MODIFIE */

$(document).ready(function () {
    // Login Form
    $("#login-form").submit(function () {

        $('#login-response').html("<p align=\"center\"> Logging in ... please wait.<br><br><img src=\"../../../etudiants/yassine/img/loading.gif\"><p>");

        var url = $("#login-form").attr("action");

        //start send the post request
        $.post(url, {
            login_email: $("#login-email").val(),
            login_password: $("#login-password").val()
        }, function (data) {
            //the response is in the data variable

            if (data.responseCode == 301) {
                // Success. Redirecting to client Area.
                $('#login-response').html("<b>login successful !</b><br> Please wait, redirecting to account area ... "); // Show the redirect url for debugging purposes ...
                location.href = data.redirectUrl;
            }
            else if (data.responseCode == 200) {
                $('#login-response').html(data.message);
            }
            else if (data.responseCode == 400) {//bad request
                $('#login-response').html(data.message);
            }
            else {
                //if we got to this point we know that the controller
                //did not return a json_encoded array. We can assume that           
                //an unexpected PHP error occured
                alert("Yassine : Something weird is going on :( ");

                //if you want to print the error:
                $('#login-response').html(data);
            }
        });

        //we dont what the browser to submit the form
        return false;
    });


    // Password Form
    $("#password-form").submit(function () {

        $('#password-response').html("<p align=\"center\"> Recovering your password ... please wait.<br><br><img src=\"../../../etudiants/yassine/img/loading.gif\"><p>");

        var url = $("#password-form").attr("action");

        //start send the post request
        $.post(url, {
            password_email: $("#password-email").val()
        }, function (data) {
            //the response is in the data variable

            if (data.responseCode == 200) {
                $('#password-response').html(data.message);
            }
            else if (data.responseCode == 400) {//bad request
                $('#password-response').html(data.message);
            }
            else {
                //if we got to this point we know that the controller
                //did not return a json_encoded array. We can assume that           
                //an unexpected PHP error occured
                alert("Yassine : Something weird is going on :( ");

                //if you want to print the error:
                $('#password-response').html(data);
            }
        });

        //we dont what the browser to submit the form
        return false;
    });

    // Email Form
    $("#email-form").submit(function () {

        $('#email-response').html("<p align=\"center\"> Searching for your email ... please wait.<br><br><img src=\"../../../etudiants/yassine/img/loading.gif\"><p>");

        var url = $("#email-form").attr("action");

        //start send the post request
        $.post(url, {
            email_firstname: $("#email-firstname").val(),
            email_lastname: $("#email-lastname").val(),
            email_account: $("#email-account").val(),
            email_year: $("#email-year").val(),
            email_month: $("#email-month").val(),
            email_day: $("#email-day").val()
        }, function (data) {
            //the response is in the data variable

            if (data.responseCode == 200) {
                $('#email-response').html(data.message);
            }
            else if (data.responseCode == 400) {//bad request
                $('#email-response').html(data.message);
            }
            else {
                //if we got to this point we know that the controller
                //did not return a json_encoded array. We can assume that           
                //an unexpected PHP error occured
                alert("Yassine : Something weird is going on :( ");

                //if you want to print the error:
                $('#email-response').html(data);
            }
        });

        //we dont what the browser to submit the form
        return false;
    });

    // Signup Form
    $("#signup-form").submit(function () {

        $('#signup-response').html("<p align=\"center\"> Creating your new account ... please wait.<br><br><img src=\"../../../etudiants/yassine/img/loading.gif\"><p>");

        var url = $("#signup-form").attr("action");

        //start send the post request
        $.post(url, {
            signup_firstname: $("#signup-firstname").val(),
            signup_lastname: $("#signup-lastname").val(),
            signup_email: $("#signup-email").val(),
            signup_year: $("#signup-year").val(),
            signup_month: $("#signup-month").val(),
            signup_day: $("#signup-day").val(),
            signup_password1: $("#signup-password1").val()
        }, function (data) {
            //the response is in the data variable

            if (data.responseCode == 200) {
                $('#signup-response').html(data.message);
            }
            else if (data.responseCode == 400) {//bad request
                $('#signup-response').html(data.message);
            }
            else {
                //if we got to this point we know that the controller
                //did not return a json_encoded array. We can assume that           
                //an unexpected PHP error occured
                alert("Yassine : Something weird is going on :( ");

                //if you want to print the error:
                $('#signup-response').html(data);
            }
        });

        //we dont what the browser to submit the form
        return false;
    });


    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });


});
