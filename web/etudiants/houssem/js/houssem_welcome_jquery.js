$(document).ready(function () {

    // Signup Form 
    $("#search-form").submit(function () {
        
        console.log("Requête en cours de préparation");
        $('#offers-ajax-container').html("<div class=\"boxedarea\"> <br> <p align=\"center\"> <b>Searching offers ... please wait ... </b><br><br><img src=\"../../../web/etudiants/houssem/img/loading.gif\" ><p></div>");
        
        var url = $("#search-form").attr("action");

        //start send the post request
        $.post(url, {
            offertype           : $("#offertype").val(),
            dealtype            : $("#dealtype").val(),
            governorate         : $("#governorate").val(),
            available_year : $("#available_year").val(),
            available_month : $("#available_month").val(),
            available_day : $("#available_day").val(),
            only_show_ecological : $("#only_show_ecological").val(),
            max_price_or_rent : $("#prix_max_number").val()
        }, function (data) {
            //the response is in the data variable

            if (data.responseCode == 200) {
                $('#offers-ajax-container').html(data.message);
            }
            else {
                //if we got to this point we know that the controller
                //did not return a json_encoded array. We can assume that           
                //an unexpected PHP error occured
                alert("Houssem : Problème AJAX !");

                //if you want to print the error:
                $('#offers-ajax-container').html(data);
            }
        });

        return false;
    });

});
