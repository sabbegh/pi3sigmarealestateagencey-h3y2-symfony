/*this function is available for Rent Offer */

function search() {
    var searchKeyWord = "";


    /*===============================================================*/
    /*Recupération de la valeur de ISECOLOGICAL  */
    if (document.getElementById("isEcological").checked == true)

    {
        searchKeyWord = searchKeyWord + " AND s.isgreen = true ";
    }
    else
    {
        searchKeyWord = searchKeyWord + " AND s.isgreen = false ";
    }


    /*===============================================================*/
    /*Recupération de la valeur de DATE */
    searchKeyWord = searchKeyWord + " AND s.availablefrom > '" +
            (document.getElementById("year").
                    options[document.getElementById('year').
                            selectedIndex].text) +
            "-" +
            (document.getElementById("month").
                    options[document.getElementById('month').
                            selectedIndex].text) + "-" +
            (document.getElementById("day").
                    options[document.getElementById('day').
                            selectedIndex].text) + "' ";

    /*===============================================================*/
    /*===============================================================*/
    /*Recupération de la valeur de Gouvernorate */
    if ((document.getElementById("selectgovernorate").
            options[document.getElementById('selectgovernorate').
                    selectedIndex].text) != "All Location")
        searchKeyWord = searchKeyWord + " AND s.governorate like '% " +
                (document.getElementById("selectgovernorate").
                        options[document.getElementById('selectgovernorate').
                                selectedIndex].text) + " %'";

    /*===============================================================*/


    if ((document.getElementById("dealtype").
            options[document.getElementById('dealtype').
                    selectedIndex].text) != "All Types")
        searchKeyWord = searchKeyWord + " AND s.dealtype like '% " +
                (document.getElementById("dealtype").
                        options[document.getElementById('dealtype').
                                selectedIndex].text) + " %'";



    if (document.getElementById("prix_max.range").value != 0)
        searchKeyWord = searchKeyWord + " AND s.rentprice BETWEEN 0 AND " + document.getElementById("prix_max.range").value;


    alert(searchKeyWord);
}
/*************************************************************************************************/
/*************************************************************************************************/
/*************************************************************************************************/
/*************************************************************************************************/

/* this function is availabel for sale offer */

function searchSale() {
    var searchKeyWord = "";

    /*===============================================================*/
    /*Recupération de la valeur de ISECOLOGICAL  */
    if (document.getElementById("isEcological").checked == true)

    {
        searchKeyWord = searchKeyWord + " AND s.isgreen = true ";
    }
    else
    {
        searchKeyWord = searchKeyWord + " AND s.isgreen = false ";
    }


    /*===============================================================*/
    /*Recupération de la valeur de DATE */
    searchKeyWord = searchKeyWord + " AND s.availablefrom > '" +
            (document.getElementById("year").
                    options[document.getElementById('year').
                            selectedIndex].text) +
            "-" +
            (document.getElementById("month").
                    options[document.getElementById('month').
                            selectedIndex].text) + "-" +
            (document.getElementById("day").
                    options[document.getElementById('day').
                            selectedIndex].text) + "' ";

    /*===============================================================*/
    /*===============================================================*/
    /*Recupération de la valeur de Gouvernorate */
    if ((document.getElementById("selectgovernorate").
            options[document.getElementById('selectgovernorate').
                    selectedIndex].text) != "All Location")
        searchKeyWord = searchKeyWord + " AND s.governorate like '% " +
                (document.getElementById("selectgovernorate").
                        options[document.getElementById('selectgovernorate').
                                selectedIndex].text) + " %'";

    /*===============================================================*/


    if ((document.getElementById("dealtype").
            options[document.getElementById('dealtype').
                    selectedIndex].text) != "All Types")
        searchKeyWord = searchKeyWord + " AND s.dealtype like '% " +
                (document.getElementById("dealtype").
                        options[document.getElementById('dealtype').
                                selectedIndex].text) + " %'";



    if (document.getElementById("prix_max.range").value != 0)
        searchKeyWord = searchKeyWord + " AND s.saleprice BETWEEN 0 AND " + document.getElementById("prix_max.range").value;


    alert(searchKeyWord);
}



/**
 * 
 
 * @param {type} theNumber
 * @param {type} target
 * @returns {undefined} */

function updateTargetWithNumber(theNumber, target)
{
    document.getElementById(target).value = theNumber;
}