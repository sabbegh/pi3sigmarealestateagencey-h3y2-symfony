/* FICHIER MODIFIE */

$(document).ready(function () {

    // Realtor Request form
    $("#realtor-request-form").submit(function () {

        $('#signup-response').html("<p align=\"center\"> Creating your new account ... please wait.<br><br><img src=\"../../../web/etudiants/houssem/img/loading.gif\"><p>");

        var url = $("#realtor-request-form").attr("action");

        //start send the post request
        $.post(url, {
            signup_firstname: $("#signup-firstname").val(),
            signup_lastname: $("#signup-lastname").val(),
            signup_email: $("#signup-email").val(),
            signup_year: $("#signup-year").val(),
            signup_month: $("#signup-month").val(),
            signup_day: $("#signup-day").val(),
            signup_password1: $("#signup-password1").val(),
            request_text : $("#signup-request-text").val()
        }, function (data) {
            //the response is in the data variable

            if (data.responseCode == 200) {
                $('#signup-response').html(data.message);
            }
            else if (data.responseCode == 400) {//bad request
                $('#signup-response').html(data.message);
            }
            else {
                //if we got to this point we know that the controller
                //did not return a json_encoded array. We can assume that           
                //an unexpected PHP error occured
                alert("Yassine : Something weird is going on :( ");

                //if you want to print the error:
                $('#signup-response').html(data);
            }
        });

        //we dont what the browser to submit the form
        return false;
    });


    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });


});
