<?php

namespace Rsa\HayfaAdministratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Complaintbyclientaboutoffersale
 *
 * @ORM\Table(name="ComplaintByClientAboutOfferSale", indexes={@ORM\Index(name="idOfClient", columns={"idOfClient"}), @ORM\Index(name="idOfOffer", columns={"idOfOffer"})})
 * @ORM\Entity
 */
class Complaintbyclientaboutoffersale
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="textOfComplaint", type="string", length=800, nullable=false)
     */
    private $textofcomplaint;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isResolvedByAdmin", type="boolean", nullable=false)
     */
    private $isresolvedbyadmin;

    /**
     * @var \Saleoffer
     *
     * @ORM\ManyToOne(targetEntity="Saleoffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfOffer", referencedColumnName="id")
     * })
     */
    private $idofoffer;

    /**
     * @var \Registereduser
     *
     * @ORM\ManyToOne(targetEntity="Registereduser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfClient", referencedColumnName="id")
     * })
     */
    private $idofclient;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set textofcomplaint
     *
     * @param string $textofcomplaint
     * @return Complaintbyclientaboutoffersale
     */
    public function setTextofcomplaint($textofcomplaint)
    {
        $this->textofcomplaint = $textofcomplaint;

        return $this;
    }

    /**
     * Get textofcomplaint
     *
     * @return string 
     */
    public function getTextofcomplaint()
    {
        return $this->textofcomplaint;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Complaintbyclientaboutoffersale
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set isresolvedbyadmin
     *
     * @param boolean $isresolvedbyadmin
     * @return Complaintbyclientaboutoffersale
     */
    public function setIsresolvedbyadmin($isresolvedbyadmin)
    {
        $this->isresolvedbyadmin = $isresolvedbyadmin;

        return $this;
    }

    /**
     * Get isresolvedbyadmin
     *
     * @return boolean 
     */
    public function getIsresolvedbyadmin()
    {
        return $this->isresolvedbyadmin;
    }

    /**
     * Set idofoffer
     *
     * @param \Rsa\HayfaAdministratorBundle\Entity\Saleoffer $idofoffer
     * @return Complaintbyclientaboutoffersale
     */
    public function setIdofoffer(\Rsa\HayfaAdministratorBundle\Entity\Saleoffer $idofoffer = null)
    {
        if (is_null($idofoffer)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->idofoffer = $idofoffer;

        return $this;
    }

    /**
     * Get idofoffer
     *
     * @return \Rsa\HayfaAdministratorBundle\Entity\Saleoffer 
     */
    public function getIdofoffer()
    {
        return $this->idofoffer;
    }

    /**
     * Set idofclient
     *
     * @param \Rsa\HayfaAdministratorBundle\Entity\Registereduser $idofclient
     * @return Complaintbyclientaboutoffersale
     */
    public function setIdofclient(\Rsa\HayfaAdministratorBundle\Entity\Registereduser $idofclient = null)
    {
            if (is_null($idofclient)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        
        $this->idofclient = $idofclient;

        return $this;
    }

    /**
     * Get idofclient
     *
     * @return \Rsa\HayfaAdministratorBundle\Entity\Registereduser 
     */
    public function getIdofclient()
    {
        return $this->idofclient;
    }
}
