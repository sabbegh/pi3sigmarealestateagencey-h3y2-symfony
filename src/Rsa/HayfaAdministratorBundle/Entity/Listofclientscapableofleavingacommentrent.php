<?php

namespace Rsa\HayfaAdministratorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Listofclientscapableofleavingacommentrent
 *
 * @ORM\Table(name="ListOfClientsCapableOfLeavingACommentRent", indexes={@ORM\Index(name="registeredUserId", columns={"registeredUserId"}), @ORM\Index(name="offerId", columns={"offerId"})})
 * @ORM\Entity
 */
class Listofclientscapableofleavingacommentrent {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Rentoffer
     *
     * @ORM\ManyToOne(targetEntity="Rentoffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="offerId", referencedColumnName="id")
     * })
     */
    private $offerid;

    /**
     * @var \Registereduser
     *
     * @ORM\ManyToOne(targetEntity="Registereduser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="registeredUserId", referencedColumnName="id")
     * })
     */
    private $registereduserid;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set offerid
     *
     * @param \Rsa\HayfaAdministratorBundle\Entity\Rentoffer $offerid
     * @return Listofclientscapableofleavingacommentrent
     */
    public function setOfferid(\Rsa\HayfaAdministratorBundle\Entity\Rentoffer $offerid = null) {

        if (is_null($offerid)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->offerid = $offerid;

        return $this;
    }

    /**
     * Get offerid
     *
     * @return \Rsa\HayfaAdministratorBundle\Entity\Rentoffer 
     */
    public function getOfferid() {
        return $this->offerid;
    }

    /**
     * Set registereduserid
     *
     * @param \Rsa\HayfaAdministratorBundle\Entity\Registereduser $registereduserid
     * @return Listofclientscapableofleavingacommentrent
     */
    public function setRegistereduserid(\Rsa\HayfaAdministratorBundle\Entity\Registereduser $registereduserid = null) {
        if (is_null($registereduserid)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->registereduserid = $registereduserid;

        return $this;
    }

    /**
     * Get registereduserid
     *
     * @return \Rsa\HayfaAdministratorBundle\Entity\Registereduser 
     */
    public function getRegistereduserid() {
        return $this->registereduserid;
    }

}
