<?php
namespace Rsa\HayfaAdministratorBundle\Entity;
use Doctrine\ORM\EntityRepository;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RentOfferRepository
 *
 * @author azus
 */
class RentOfferRepository extends EntityRepository {
    //put your code here
    public function findrentComplaints()
{
    $query=$this->getEntityManager()
            ->createQuery("select o from Rsa\HayfaAdministratorBundle\Entity\Rentoffer r join Rsa\HayfaAdministratorBundle\Entity\Complaintbyclientaboutofferrent c where r.id = c.idofoffer");
    return $query->getResult();
}
}
