<?php

namespace Rsa\HayfaAdministratorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegistereduserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        
            ->add('email','email')
            ->add('password','password')
            ->add('firstname')
            ->add('lastname')
            ->add('userimageurl')
            ->add('birthday')
    
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rsa\HayfaAdministratorBundle\Entity\Registereduser'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rsa_hayfaadministratorbundle_registereduser';
    }
}
