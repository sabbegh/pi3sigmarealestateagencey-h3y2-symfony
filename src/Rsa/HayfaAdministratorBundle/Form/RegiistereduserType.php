<?php

namespace Rsa\HayfaAdministratorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegiistereduserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            
            ->add('email')
            ->add('password')
            ->add('firstname')
            ->add('lastname')
            ->add('userimageurl')
            ->add('birthday')
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rsa\HayfaAdministratorBundle\Entity\Regiistereduser'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rsa_hayfaadministratorbundle_regiistereduser';
    }
}
