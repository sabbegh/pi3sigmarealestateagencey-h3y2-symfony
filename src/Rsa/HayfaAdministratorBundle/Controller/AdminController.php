<?php

namespace Rsa\HayfaAdministratorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Zend\Json\Expr;
use Rsa\HayfaAdministratorBundle\Entity\Regiistereduser;
use Rsa\HayfaAdministratorBundle\Entity\Registereduser;
use Rsa\HayfaAdministratorBundle\Form\RegistereduserType;
use Rsa\HayfaAdministratorBundle\Form\RegiistereduserType;
use Rsa\HayfaAdministratorBundle\Form\updateAdminForm;
use Rsa\HayfaAdministratorBundle\Form\updateRealForm;

class AdminController extends Controller {

    public function homepageAction() {
        // <<<<<<<=================================================================>>>>>>>
        
        // <<<<<<<=================================================================>>>>>>>

        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:homepage.html.twig', array());
    }
 public function addsuccAction() {
        // <<<<<<<=================================================================>>>>>>>
        
        // <<<<<<<=================================================================>>>>>>>

        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:addedsuccessfully.html.twig', array());
    }
public function updatesuccAction() {
        // <<<<<<<=================================================================>>>>>>>
        
        // <<<<<<<=================================================================>>>>>>>

        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:Updated Sucssefullu.html.twig', array());
    }
    public function addAdminAction() {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $Registereduser = new Registereduser();
        $form = $this->container->get('form.factory')->create(new RegistereduserType(), $Registereduser);
        $Request = $this->getRequest();

        if ($Request->getMethod() == 'POST') {
            $form->bind($Request);
            if ($form->isValid()) {
                $em = $this->container->get('doctrine')->getEntityManager();
                $em->persist($Registereduser);
                $em->flush();
                return $this->redirect($this->generateUrl("rsa_hayfa_administrator_addsucc"));
            }
        }
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:add.html.twig', array('Form' => $form->createView(),));
    }

    public function addRealtorAction() {
        // <<<<<<<=================================================================>>>>>>>
        
        // <<<<<<<=================================================================>>>>>>>
        $Registereduser = new Regiistereduser();
        $form = $this->container->get('form.factory')->create(new RegiistereduserType(), $Registereduser);
        $Request = $this->getRequest();

        if ($Request->getMethod() == 'POST') {
            $form->bind($Request);
            if ($form->isValid()) {
                $em = $this->container->get('doctrine')->getEntityManager();
                $em->persist($Registereduser);
                $em->flush();
                return $this->redirect($this->generateUrl("rsa_hayfa_administrator_addsucc"));
            }
        }
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:addReal.html.twig', array('Form' => $form->createView(),));
    }

    public function manageRealAction() {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $em = $this->container->get('doctrine')->getEntityManager();
        $realtors = $em->getRepository('RsaHayfaAdministratorBundle:Realtoraccountcreationrequest')->findByIstreatedbyadmin(0);
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:list.html.twig', array('realtors' => $realtors));
    }

    public function manageRentCompAction() {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $em = $this->container->get('doctrine')->getEntityManager();
        $complaints = $em->getRepository('RsaHayfaAdministratorBundle:Complaintbyclientaboutofferrent')->findByIsresolvedbyadmin(0);
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:ComplaintRentOffer.html.twig', array('complaints' => $complaints));
    }

    public function manageSaleCompAction() {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $em = $this->container->get('doctrine')->getEntityManager();
        $complaints = $em->getRepository('RsaHayfaAdministratorBundle:Complaintbyclientaboutoffersale')->findByIsresolvedbyadmin(0);
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:ComplaintSaleOffer.html.twig', array('complaints' => $complaints));
    }

    public function disableUserAction($idClient, $id) {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $em = $this->container->get('doctrine')->getEntityManager();
        $user = $em->getRepository('RsaHayfaAdministratorBundle:Registereduser')->find($idClient);
        $user->setIsdeletedbyadmin(1);
        $em->persist($user);
        $em->flush();
        $u = $em->getRepository('RsaHayfaAdministratorBundle:Complaintbyclientaboutofferrent')->find($id);
        $u->setIsresolvedbyadmin(1);
        $em->persist($u);
        $em->flush();
        return $this->redirect($this->generateUrl("rsa_hayfa_administrator_manageRentComplaints"));
    }

    public function disableUserSaleAction($idClient, $id) {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $em = $this->container->get('doctrine')->getEntityManager();
        $user = $em->getRepository('RsaHayfaAdministratorBundle:Registereduser')->find($idClient);
        $user->setIsdeletedbyadmin(1);
        $em->persist($user);
        $em->flush();
        $u = $em->getRepository('RsaHayfaAdministratorBundle:Complaintbyclientaboutoffersale')->find($id);
        $u->setIsresolvedbyadmin(1);
        $em->persist($u);
        $em->flush();
        return $this->redirect($this->generateUrl("rsa_hayfa_administrator_manageSaleComplaints"));
    }

    public function refuseReqAction($id) {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $em = $this->container->get('doctrine')->getEntityManager();
        $realtor = $em->getRepository('RsaHayfaAdministratorBundle:Realtoraccountcreationrequest')->find($id);
        $realtor->setIstreatedbyadmin(1);
        $em->persist($realtor);
        $em->flush();
        return $this->redirect($this->generateUrl("rsa_hayfa_administrator_manageRealtor"));
    }

    public function acceptReqAction($id) {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $Registereduser = new Registereduser();
        $em = $this->container->get('doctrine')->getEntityManager();
        $realtor = $em->getRepository('RsaHayfaAdministratorBundle:Realtoraccountcreationrequest')->find($id);
        $realtor->setIstreatedbyadmin(1);
        $em->persist($realtor);
        $em->flush();
        $Registereduser->setEmail($realtor->getEmail());
        $Registereduser->setPassword($realtor->getPassword());
        $Registereduser->setFirstname($realtor->getFirstname());
        $Registereduser->setLastname($realtor->getLastname());
        $Registereduser->setUserimageurl($realtor->getUserimageurl());
        $Registereduser->setBirthday($realtor->getBirthday());
        $Registereduser->setUsertype("realtor");
        $em->persist($Registereduser);
        $em->flush();
        return $this->redirect($this->generateUrl("rsa_hayfa_administrator_manageRealtor"));
    }

    public function listeAdminAction() {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $em = $this->container->get('doctrine')->getEntityManager();
        $administrators = $em->getRepository('RsaHayfaAdministratorBundle:Registereduser')->findByUsertype('administrator');
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:listeAdmin.html.twig', array('administrators' => $administrators));
    }

    public function listeRealAction() {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $em = $this->container->get('doctrine')->getEntityManager();
        $administrators = $em->getRepository('RsaHayfaAdministratorBundle:Registereduser')->findByUsertype('realtor');
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:listReal.html.twig', array('administrators' => $administrators));
    }

    public function listeClientAction() {
        $em = $this->container->get('doctrine')->getEntityManager();
        $administrators = $em->getRepository('RsaHayfaAdministratorBundle:Registereduser')->findByUsertype('client');
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:listeClient.html.twig', array('administrators' => $administrators));
        $var1 = $administrators . Length;
    }

    public function AdminAction($id) {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $em = $this->container->get('doctrine')->getEntityManager();
        $administrators = $em->getRepository('RsaHayfaAdministratorBundle:Registereduser')->find($id);

        $form = $this->container->get('form.factory')->create(new updateAdminForm(), $administrators);
        $Request = $this->getRequest();

        if ($Request->getMethod() == 'POST') {
            $form->bind($Request);
            if ($form->isValid()) {
                $em = $this->container->get('doctrine')->getEntityManager();
                $em->persist($administrators);
                $em->flush();
            }
        }
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:update.html.twig', array('Form' => $form->createView(),));
    }

    public function updateClientAction($id) {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $em = $this->container->get('doctrine')->getEntityManager();
        $administrators = $em->getRepository('RsaHayfaAdministratorBundle:Registereduser')->find($id);

        $form = $this->container->get('form.factory')->create(new updateAdminForm(), $administrators);
        $Request = $this->getRequest();

        if ($Request->getMethod() == 'POST') {
            $form->bind($Request);
            if ($form->isValid()) {
                $em = $this->container->get('doctrine')->getEntityManager();
                $em->persist($administrators);
                $em->flush();
            }
        }
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:updateClient.html.twig', array('Form' => $form->createView(),));
    }
public function updateAdminActionAction($id) {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $em = $this->container->get('doctrine')->getEntityManager();
        $administrators = $em->getRepository('RsaHayfaAdministratorBundle:Registereduser')->find($id);

        $form = $this->container->get('form.factory')->create(new updateAdminForm(), $administrators);
        $Request = $this->getRequest();

        if ($Request->getMethod() == 'POST') {
            $form->bind($Request);
            if ($form->isValid()) {
                $em = $this->container->get('doctrine')->getEntityManager();
                $em->persist($administrators);
                $em->flush();
            }
        }
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:updateAdministrator.html.twig', array('Form' => $form->createView(),));
    }
    public function updateRealAction($id) {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $em = $this->container->get('doctrine')->getEntityManager();
        $administrators = $em->getRepository('RsaHayfaAdministratorBundle:Registereduser')->find($id);

        $form = $this->container->get('form.factory')->create(new updateRealForm(), $administrators);
        $Request = $this->getRequest();

        if ($Request->getMethod() == 'POST') {
            $form->bind($Request);
            if ($form->isValid()) {
                $em = $this->container->get('doctrine')->getEntityManager();
                $em->persist($administrators);
                $em->flush();
            }
        }
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:updateRealtor.html.twig', array('Form' => $form->createView(),));
    }

    public function chartLineAction() {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        // Chart
        $series = array(
            array("name" => "Nom du graphe", "data" => array(1, 2, 4, 5, 6, 3, 8))
        );
        $ob = new Highchart();
        $ob->chart->renderTo('linechart'); // #id du div où afficher le graphe
        $ob->title->text('Titre du graphique');
        $ob->xAxis->title(array('text' => "Titre axe horizontal"));
        $ob->yAxis->title(array('text' => "Titre axe vertical "));
        $ob->series($series);
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:LineChart.html.twig', array(
                    'chart' => $ob
        ));
    }

    public function chartPieAction() {
       
          // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        // Chart
        $em = $this->container->get('doctrine')->getEntityManager();
        $administrators = $em->getRepository('RsaHayfaAdministratorBundle:Registereduser')->findByUsertype('administrator');
        $em1 = $this->container->get('doctrine')->getEntityManager();
        $realtors = $em1->getRepository('RsaHayfaAdministratorBundle:Registereduser')->findByUsertype('realtor');
        $em2 = $this->container->get('doctrine')->getEntityManager();
        $clientss = $em2->getRepository('RsaHayfaAdministratorBundle:Registereduser')->findByUsertype('client');
        $em3 = $this->container->get('doctrine')->getEntityManager();
        $alls = $em3->getRepository('RsaHayfaAdministratorBundle:Registereduser')->findAll();
        $results = new Registereduser();
        $admins = 0;
        $reals = 0;
        $clients = 0;
        $all=0;
foreach($alls as $results)
{

 
$all++;
}
foreach($administrators as $results)
{

 
$admins++;
}
foreach($realtors as $results)
{

 
$reals++;
}
foreach($clientss as $results)
{

 
$clients++;
}
$admins = ($admins*100)/$all;
        $reals = ($reals*100)/$all;
        $clients = ($clients*100)/$all;
        

        // <<<<<<<=================================================================>>>>>>>
        
        $ob = new Highchart();
        $ob->chart->renderTo('piechart');
        $ob->title->text('the distribution of users. ');
        $ob->plotOptions->pie(array(
            'allowPointSelect' => true,
            'cursor' => 'pointer',
            'dataLabels' => array('enabled' => false), 'showInLegend' => true
        ));
        $data = array(
            array('Realtors', $admins),
            array('Administrators',$reals),
            array('Clients', $clients),
        );
        $ob->series(array(array('type' => 'pie', 'name' => 'amount of this type of users', 'data' => $data)));
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:pie.html.twig', array(
                    'chart' => $ob
        ));
    }

    public function chartHistogrammeAction() {
// <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        $series = array(
            array(
                'name' => 'Rainfall',
                'type' => 'column',
                'color' => '#4572A7',
                'yAxis' => 1,
                'data' => array(49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4),
            ),
            array(
                'name' => 'Temperature',
                'type' => 'spline',
                'color' => '#AA4643',
                'data' => array(7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6),
            ),
        );
        $yData = array(
            array(
                'labels' => array(
                    'formatter' => new Expr('function () { return this.value + " degrees C" }'),
                    'style' => array('color' => '#AA4643')
                ),
                'title' => array(
                    'text' => 'Temperature',
                    'style' => array('color' => '#AA4643')
                ),
                'opposite' => true,
            ),
            array(
                'labels' => array(
                    'formatter' => new Expr('function () { return this.value + " mm" }'),
                    'style' => array('color' => '#4572A7')
                ),
                'gridLineWidth' => 0,
                'title' => array(
                    'text' => 'Rainfall',
                    'style' => array('color' => '#4572A7')
                ),
            ),
        );
        $categories = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
        $ob = new Highchart();
        $ob->chart->renderTo('container'); // The #id of the div where to render the chart
        $ob->chart->type('column');
        $ob->title->text('Average Monthly Weather Data for Tokyo');
        $ob->xAxis->categories($categories);
        $ob->yAxis($yData);
        $ob->legend->enabled(false);
        $formatter = new Expr('function () {
 var unit = {
 "Rainfall": "mm",
 "Temperature": "degrees C"
 }[this.series.name];
 return this.x + ": <b>" + this.y + "</b> " + unit;
 }');
        $ob->tooltip->formatter($formatter);
        $ob->series($series);
        return $this->render('RsaHayfaAdministratorBundle:HayfaAdmin:histogramme.html.twig', array(
                    'chart' => $ob
        ));
    }

}
