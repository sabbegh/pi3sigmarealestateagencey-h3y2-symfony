<?php

namespace Rsa\HayfaAdministratorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('RsaHayfaAdministratorBundle:Default:index.html.twig', array('name' => $name));
    }
}
