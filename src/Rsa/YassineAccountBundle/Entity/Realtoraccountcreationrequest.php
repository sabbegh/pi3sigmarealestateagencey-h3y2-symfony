<?php

namespace Rsa\YassineAccountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Realtoraccountcreationrequest
 *
 * @ORM\Table(name="RealtorAccountCreationRequest")
 * @ORM\Entity
 */
class Realtoraccountcreationrequest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=64, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=128, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=64, nullable=false)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=64, nullable=false)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="userImageUrl", type="string", length=128, nullable=false)
     */
    private $userimageurl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date", nullable=false)
     */
    private $birthday;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="requestDate", type="date", nullable=false)
     */
    private $requestdate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isTreatedByAdmin", type="boolean", nullable=false)
     */
    private $istreatedbyadmin = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="requestTexte", type="string", length=800, nullable=false)
     */
    private $requesttexte;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Realtoraccountcreationrequest
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Realtoraccountcreationrequest
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Realtoraccountcreationrequest
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Realtoraccountcreationrequest
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set userimageurl
     *
     * @param string $userimageurl
     * @return Realtoraccountcreationrequest
     */
    public function setUserimageurl($userimageurl)
    {
        $this->userimageurl = $userimageurl;

        return $this;
    }

    /**
     * Get userimageurl
     *
     * @return string 
     */
    public function getUserimageurl()
    {
        return $this->userimageurl;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return Realtoraccountcreationrequest
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set requestdate
     *
     * @param \DateTime $requestdate
     * @return Realtoraccountcreationrequest
     */
    public function setRequestdate($requestdate)
    {
        $this->requestdate = $requestdate;

        return $this;
    }

    /**
     * Get requestdate
     *
     * @return \DateTime 
     */
    public function getRequestdate()
    {
        return $this->requestdate;
    }

    /**
     * Set istreatedbyadmin
     *
     * @param boolean $istreatedbyadmin
     * @return Realtoraccountcreationrequest
     */
    public function setIstreatedbyadmin($istreatedbyadmin)
    {
        $this->istreatedbyadmin = $istreatedbyadmin;

        return $this;
    }

    /**
     * Get istreatedbyadmin
     *
     * @return boolean 
     */
    public function getIstreatedbyadmin()
    {
        return $this->istreatedbyadmin;
    }

    /**
     * Set requesttexte
     *
     * @param string $requesttexte
     * @return Realtoraccountcreationrequest
     */
    public function setRequesttexte($requesttexte)
    {
        $this->requesttexte = $requesttexte;

        return $this;
    }

    /**
     * Get requesttexte
     *
     * @return string 
     */
    public function getRequesttexte()
    {
        return $this->requesttexte;
    }
}
