<?php

// FICHIER MODIFIE

namespace Rsa\YassineAccountBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Rsa\YassineAccountBundle\Entity\Registereduser;
use Rsa\YassineAccountBundle\Entity\RegistereduserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class AccountsAjaxController extends Controller {

    public function loginFormAction() {

        /* On récupère la requete AJAX */
        $request = $this->get('request');

        /* On récupère les champs de la requête AJAX */
        $email = $request->request->get('login_email');
        $password = $request->request->get('login_password');

        /* Testing if Account Exists */
        $em = $this->getDoctrine()->getManager();
        $theUser = $em->getRepository("RsaYassineAccountBundle:Registereduser")->findOneBy(array('email' => $email));

        if ($theUser == null) {
            // ==== Account Does not Exist <====
            // Answer : Sorry, no such account ! Check your email address
            $responseMessage = 'No accounts associated with : <b>' . $email . ' ! </b>';
            $responseMessage = $responseMessage . '<br> Have you <a href="#forgot-email"c>forgotten your email</a> ?';

            $responseMessage = '<p>' . $responseMessage . '</p>';
            $return = array("responseCode" => 200, "message" => $responseMessage);
            $return = json_encode($return);
            return new Response($return, 200, array('Content-Type' => 'application/json'));
        } else {
            // Account Exists
            /* Testing if password is correct */

            if ($password == $theUser->getPassword()) {
                // Password is correct
                /* Testing if account is not disable */
                if (!$theUser->getIsDeletedByAdmin()) {
                    // ====> Account is active <====
                    {
                        // Setting the session parameters
                        $session = new Session();
                        $session->start();

                        $session->set('connectedUserId', $theUser->getId());
                        // Redirecting the user

//                        $redirectUrl = $this->generateUrl('rsa_yassine_account_accountArea', array('userIdOrEmail' => $theUser->getId()));
                        $redirectUrl = $this->generateUrl('rsa_yassine_account_accountArea', array());                        
                        $return = array("responseCode" => 301, "redirectUrl" => $redirectUrl);
                        $return = json_encode($return);
                        return new Response($return, 200, array('Content-Type' => 'application/json'));

                        // SET ENVIRONMENT VARIABLES ETC
                    }

                    // Redirect user to account area, By rendering 
                } else {
                    // ====> Account is disabled <====
                    // Answer : Sorry Mr. First Name, Last Name. Your Account is disabled ! Contact an administrator
                    $responseMessage = 'Sorry, you cannot login !';
                    $responseMessage = $responseMessage . '<br> Sorry ' . $theUser->getFirstName() . ' ' . $theUser->getLastName() . ' !';
                    $responseMessage = $responseMessage . ' <br> <b>Your account has been disabled</b>';
                    $responseMessage = $responseMessage . '<br>You can <a href="' . $url = $this->generateUrl('rsa_yassine_account_contactAboutUs', array(), true) . '"> contact an administrator</a> to find a solution</b>';

                    $return = array("responseCode" => 200, "message" => $responseMessage);
                    $return = json_encode($return);
                    return new Response($return, 200, array('Content-Type' => 'application/json'));
                }
            } else {
                // ====> Password is wrong <====
                // Answer : Please check your password 
                $responseMessage = 'Sorry, you cannot login ! <br>Please check <b>your password</b> !';
                $responseMessage = $responseMessage . '<br> Have you <a href="#forgot-password">forgotten your password</a> ?';
                $responseMessage = '<p>' . $responseMessage . '</p>';
                $return = array("responseCode" => 200, "message" => $responseMessage);
                $return = json_encode($return);
                return new Response($return, 200, array('Content-Type' => 'application/json'));
            }
        }
    }

    public function passwordFormAction() {

        /* On récupère la requete AJAX */
        $request = $this->get('request');

        /* On récupère les champs de la requête AJAX */
        $email = $request->request->get('password_email');

        /* Testing AJAX parameter passage */
        $responseMessage = 'Hello! This is passwordFormAction !';
        $responseMessage = $responseMessage . '<br> Your email is  : <b> ' . $email . ' </b>';

        $em = $this->getDoctrine()->getManager();
        $theUser = $em->getRepository("RsaYassineAccountBundle:Registereduser")->findOneBy(array('email' => $email));

        if ($theUser == null) {
            $responseMessage = 'Sorry, no account associated with : <b>' . $email . '</b> !';
            $responseMessage = $responseMessage . '<br> Have you <a href="#forgot-email">forgotten your email</a> ?';
        } else {

            $responseMessage = 'Hello ' . $theUser->getFirstname() . ' ' . $theUser->getLastname() . '</b>';
            $responseMessage = $responseMessage . '<br> We have sent a new password to your email !';
//            $responseMessage = $responseMessage . '<br><br> Check your email ! <a href="https://www.gmail.com">Gmail</a> | <a href="https://mail.yahoo.com">Yahoo! Mail</a> | <a href="https://login.live.com/">Outlook</a> | <a href="https://www.zoho.com/mail/login.html">ZoHo</a>';
            $responseMessage = $responseMessage . '<br><br> Back to <a href="#login" onclick="document.getElementById(\'login-email\').value = \'' . $theUser->getEmail() . '\';">login<a>! </b>';

            /* Generating new password */
            $seed = 'seed_aleatoire_pour_generer_mot_de_passe_securise_@~~-\"9rZESG';
            $seed = $seed . $theUser->getFirstname() . $theUser->getLastname() . $theUser->getEmail() . $theUser->getUsertype();
            $seed = \rand() . $seed . \rand();
            $newPassword = \hash('ripemd128', $seed);

            /* Storing new password in database */
            $newPasswordEncrypted = \hash('sha256', $newPassword);
            $theUser->setPassword($newPasswordEncrypted);
            $em->persist($theUser);
            $em->flush();

            /* Generating Email */
            $emailBody = '<h2>Password Reset Email</h2><br>Hello <b>' . $theUser->getFirstname() . ' ' . $theUser->getLastname() . '</b> !';
            $emailBody = $emailBody . '<br><br>You have requested a new password for your ' . $theUser->getUsertype() . ' account at H3Y2 Real Estate Agency !';
            $emailBody = $emailBody . '<br><br>Your new password is :    <b>';
            $emailBody = $emailBody . $newPassword . '</b>';

            /* The code for sending email (and SWIFT integration) is implemented by Houssem Sabbegh - thanks :) ! */
            /* Sending email */
            $message = \Swift_Message::newInstance()
                    ->setSubject('Hello ' . $theUser->getFirstname() . ' ' . $theUser->getLastname() . ' - Password Reset Email - H3Y2 Real Estate Agency')
                    ->setFrom('h3y2devdur@gmail.com')
                    ->setTo($theUser->getEmail())
                    ->setBody($emailBody, 'text/html');
            $this->get('mailer')->send($message);
        }

//        $responseMessage = $responseMessage . '<br><br>DEBUG : the set password is :    <b>' . $theUser->getPassword();
        $responseMessage = '<p>' . $responseMessage . '</p>';
        $return = array("responseCode" => 200, "message" => $responseMessage);
        $return = json_encode($return);
        return new Response($return, 200, array('Content-Type' => 'application/json'));
    }

    public function emailFormAction() {

        /* On récupère la requete AJAX */
        $request = $this->get('request');

        /* On récupère les champs de la requête AJAX */
        $firstname = $request->request->get('email_firstname');
        $lastname = $request->request->get('email_lastname');
        $accounttype = $request->request->get('email_account');
        $year = $request->request->get('email_year');
        $month = $request->request->get('email_month');
        $day = $request->request->get('email_day');

        $responseMessage = '';

        /* Date conversion */
        $birthdayString = $year . '-' . $month . '-' . $day;
        $birthday = \DateTime::createFromFormat('Y-m-d', $birthdayString);


        /* Testing AJAX parameter passage */

        // DEBUT DEBUG <===================
//            $responseMessage = $responseMessage . '<br> Your account type is  : <b> ' . $accounttype . ' </b>';
//            $responseMessage = $responseMessage . '<br> Your first name is  : <b> ' . $firstname . ' </b>';
//            $responseMessage = $responseMessage . '<br> Your last name is  : <b> ' . $lastname . ' </b>';
//            $responseMessage = $responseMessage . '<br> Your b.year is  : <b> ' . $year . ' </b>';
//            $responseMessage = $responseMessage . '<br> Your b.month is  : <b> ' . $month . ' </b>';
//            $responseMessage = $responseMessage . '<br> Your b.day is  : <b> ' . $day . ' </b>';
        // FIN DEBUG <===================

        /* Get the matching users */
        $em = $this->getDoctrine()->getManager();
        $theUser = $em->getRepository("RsaYassineAccountBundle:Registereduser")->findBy(array('usertype' => $accounttype, 'firstname' => $firstname, 'lastname' => $lastname, 'birthday' => $birthday));

        if (sizeof($theUser) == 1) {
            $theUser = $theUser[0];
            $responseMessage = $responseMessage . 'Hello ' . $theUser->getFirstname() . ' ' . $theUser->getLastname() . '</b>';
            $responseMessage = $responseMessage . '<br> Your email is : <b>' . $theUser->getEmail() . '</b>';
            $responseMessage = $responseMessage . '<br> You can <a href="#login" onclick="document.getElementById(\'login-email\').value = \'' . $theUser->getEmail() . '\'; document.getElementById(\'password-email\').value = \'' . $theUser->getEmail() . '\';">login with your email<a> now ! </b>';
        } else {
            if (sizeof($theUser) == 0) {
                $responseMessage = $responseMessage . 'Sorry, <b>no matching accounts</b> !';
                $responseMessage = $responseMessage . '<br>You can <a href="' . $url = $this->generateUrl('rsa_yassine_account_contactAboutUs', array(), true) . '"> contact an administrator</a> to find a solution</b>';
            } else {
                $responseMessage = $responseMessage . 'Sorry, there are <b>more than one match</b>!';
                $responseMessage = $responseMessage . '<br>As a security measure, we cannot show you any email !.</b>';
                $responseMessage = $responseMessage . '<br>You can <a href="' . $url = $this->generateUrl('rsa_yassine_account_contactAboutUs', array(), true) . '"> contact an administrator</a> to find a solution</b>';
            }
        }

        $responseMessage = '<p>' . $responseMessage . '</p>';
        $return = array("responseCode" => 200, "message" => $responseMessage);
        $return = json_encode($return);
        return new Response($return, 200, array('Content-Type' => 'application/json'));
    }

    public function signupFormAction() {

        /* On récupère la requete AJAX */
        $request = $this->get('request');

        /* On récupère les champs de la requête AJAX */
        $firstname = $request->request->get('signup_firstname');
        $lastname = $request->request->get('signup_lastname');
        $email = $request->request->get('signup_email');
        $year = $request->request->get('signup_year');
        $month = $request->request->get('signup_month');
        $day = $request->request->get('signup_day');
        $password = $request->request->get('signup_password1');

        /* Date conversion */
        $birthdayString = $year . '-' . $month . '-' . $day;
        $birthday = \DateTime::createFromFormat('Y-m-d', $birthdayString);


        /* Testing AJAX parameter passage */
//        $responseMessage = 'Hello! This is signupFormAction !';
//        $responseMessage = $responseMessage . '<br> Your email is  : <b> ' . $email . ' </b>';
//        $responseMessage = $responseMessage . '<br> Your first name is  : <b> ' . $firstname . ' </b>';
//        $responseMessage = $responseMessage . '<br> Your last name is  : <b> ' . $lastname . ' </b>';
//        $responseMessage = $responseMessage . '<br> Your b.year is  : <b> ' . $year . ' </b>';
//        $responseMessage = $responseMessage . '<br> Your b.month is  : <b> ' . $month . ' </b>';
//        $responseMessage = $responseMessage . '<br> Your b.day is  : <b> ' . $day . ' </b>';
//        $responseMessage = $responseMessage . '<br> Your password is  : <b> ' . $password . ' </b>';

        $em = $this->getDoctrine()->getManager();
        $existingUser = $em->getRepository("RsaYassineAccountBundle:Registereduser")->findOneBy(array('email' => $email));

        $responseMessage = '';

        if ($existingUser) {

            $responseMessage = $responseMessage . 'The email <b>' . $email . '</b> is alrady taken !';
            $responseMessage = $responseMessage . '<br> Please try again with another email.';
        } else {

            /* Create the user */
            $newUser = new Registereduser();
            $newUser->setFirstname($firstname);
            $newUser->setLastname($lastname);
            $newUser->setEmail($email);
            $newUser->setBirthday($birthday);
            $newUser->setPassword($password);
            // This is the default "no profile picture" image : http://i.imgur.com/Ypqwwg2.png?1
            $newUser->setUserimageurl('http://i.imgur.com/Ypqwwg2.png?1');
            $newUser->setUsertype("client");

            /* Save him to the database */
            $em->persist($newUser);
            $em->flush();

            /* Send an email to the client */
            $emailBody = '<h2>Account Creation Notification</h2><br>Hello <b>' . $newUser->getFirstname() . ' ' . $newUser->getLastname() . '</b> !';
            $emailBody = $emailBody . '<br><br>Your <b>' . $newUser->getUsertype() . '</b> account at H3Y2 Real Estate Agency has been created.';
            $emailBody = $emailBody . '<br><br>We hope to see you soon. Do not forget to set a profile picture !';
            $message = \Swift_Message::newInstance()
                    ->setSubject('Hello ' . $newUser->getFirstname() . ' ' . $newUser->getLastname() . ' - Account Creation Notification - H3Y2 Real Estate Agency')
                    ->setFrom('h3y2devdur@gmail.com')
                    ->setTo($newUser->getEmail())
                    ->setBody($emailBody, 'text/html');
            $this->get('mailer')->send($message);

            /* Give AJAX response to the user */
            $responseMessage = $responseMessage . 'Your <b>client</b> account has been successfully created !';
            $responseMessage = $responseMessage . '<br> You can <a href="#login" onclick="document.getElementById(\'login-email\').value = \'' . $email . '\'; document.getElementById(\'password-email\').value = \'' . $email . '\';">login your new account<a> now ! </b>';
        }

        $responseMessage = '<p>' . $responseMessage . '</p>';
        $return = array("responseCode" => 200, "message" => $responseMessage);
        $return = json_encode($return);
        return new Response($return, 200, array('Content-Type' => 'application/json'));
    }

    public function changePasswordFormAction() {

        /* On récupère la requete AJAX */
        $request = $this->get('request');

        /* On récupère les champs de la requête AJAX */
        $oldPassword = $request->request->get('old_password');
        $newPassword = $request->request->get('new_password');
        $userId = $request->request->get('user_id');
        $responseMessage = '';

        /* Testing AJAX parameter passage */
//        $responseMessage = 'Hello! This is changePasswordActionAction !';
//        $responseMessage = $responseMessage . '<br> user_id  : <b> ' . $userId . ' </b>';
//        $responseMessage = $responseMessage . '<br> old_password  : <b> ' . $oldPassword . ' </b>';
//        $responseMessage = $responseMessage . '<br> new_password  : <b> ' . $newPassword . ' </b>';

        $em = $this->getDoctrine()->getManager();
        $theUser = $em->getRepository("RsaYassineAccountBundle:Registereduser")->findOneById($userId);

        if ($theUser->getPassword() != $oldPassword) {
            $responseMessage = 'You cannot change your password !';
            $responseMessage = $responseMessage . '<br><br><b>Your old password is incorrect. </b>';
        } else if ($theUser->getPassword() == $newPassword) {
            $responseMessage = 'You need to choose another password !';
            $responseMessage = $responseMessage . '<br><br>Your old password <b>matches the new password</b>.';
        } else {
            $responseMessage = $responseMessage . 'Your password has been <b>changed successfully</b> !';

            /* Storing new password in database */
            $theUser->setPassword($newPassword);
            $em->persist($theUser);
            $em->flush();

            /* Generating password change notification email */
            $emailBody = '<h2>Password Change Notification</h2><br>Hello <b>' . $theUser->getFirstname() . ' ' . $theUser->getLastname() . '</b> !';
            $emailBody = $emailBody . '<br><br>You have changed your password at <b>' . date('d/m/Y h:i:s a', time()) . '</b>';
            $emailBody = $emailBody . '<br><br>This email is sent as a security measure. <br><br><b>If you did not initiate this password change, please contact an administrator immediately</b>';

            /* The code for sending email (and SWIFT integration) is implemented by Houssem Sabbegh - thanks :) ! */
            /* Sending email */
            $message = \Swift_Message::newInstance()
                    ->setSubject('Hello ' . $theUser->getFirstname() . ' ' . $theUser->getLastname() . ' - Password Change Notification - H3Y2 Real Estate Agency')
                    ->setFrom('h3y2devdur@gmail.com')
                    ->setTo($theUser->getEmail())
                    ->setBody($emailBody, 'text/html');
            $this->get('mailer')->send($message);
        }

        $responseMessage = '<p>' . $responseMessage . '</p>';
        $return = array("responseCode" => 200, "message" => $responseMessage);
        $return = json_encode($return);
        return new Response($return, 200, array('Content-Type' => 'application/json'));
    }
    
    public function updateInformationFormAction() {
        
        /* On récupère la requete AJAX */
        $request = $this->get('request');
        
        $responseMessage = '';

        /* On récupère les champs de la requête AJAX */
        $userId = $request->request->get('basic_account_user_id');
        $firstname = $request->request->get('basic_account_first_name');
        $lastname = $request->request->get('basic_account_last_name');
        $email = $request->request->get('basic_account_email');
        $birthday_day = $request->request->get('basic_account_day');
        $birthday_month = $request->request->get('basic_account_month');
        $birthday_year = $request->request->get('basic_account_year');
        

        /* Testing */
//        $responseMessage = $responseMessage . $userId . '<br>';
//        $responseMessage = $responseMessage . $firstname . '<br>' ;
//        $responseMessage = $responseMessage . $lastname . '<br>';
//        $responseMessage = $responseMessage . $email . '<br>';
//        $responseMessage = $responseMessage . $birthday_day . '<br>';
//        $responseMessage = $responseMessage . $birthday_month . '<br>';
//        $responseMessage = $responseMessage . $birthday_year . '<br>';

        $em = $this->getDoctrine()->getManager();
        $theUser = $em->getRepository("RsaYassineAccountBundle:Registereduser")->findOneById($userId);
        
        $modified = (bool) 0;

        if ( $email != $theUser->getEmail()  )
        {
            $anotherUser = $em->getRepository("RsaYassineAccountBundle:Registereduser")->findOneBy(array('email' => $email));
            
            if ($anotherUser)
            {
                  $responseMessage = $responseMessage . 'You cannot change your email :<br>';
                  $responseMessage = $responseMessage . 'The email <b>' . $email . '</b> is already taken !<br>';
            }
            
            else
            {
                $modified = (bool) 1;
                $theUser->setEmail($email);
                $responseMessage = $responseMessage . 'Your <b>email</b> has been successfully updated !<br>';
            }
        }
        
        if ($firstname != $theUser->getFirstname() )
        {
            $theUser->setFirstname($firstname);
            $responseMessage = $responseMessage . 'Your </b>first name<b> has been updated !<br>';
            $modified = (bool) 1;
        }
        
        if ($lastname != $theUser->getLastname() )
        {
            $theUser->setLastname($lastname);
            $responseMessage = $responseMessage . 'Your <b>last name</b> has been updated !<br>';
            $modified = (bool) 1;
        }        
        
        // Check birthday
        
        if ( $modified == (bool) 1)
        {
            $em->persist($theUser);
            $em->flush();
        }                    
        
        else
        {
            $responseMessage = $responseMessage . 'Nothing to update !';
        }

        
        $responseMessage = '<p>' . $responseMessage . '</p>';
        $return = array("responseCode" => 200, "message" => $responseMessage, "committed_email" => $theUser->getEmail() );
        $return = json_encode($return);
        return new Response($return, 200, array('Content-Type' => 'application/json'));        
    }

    
}
