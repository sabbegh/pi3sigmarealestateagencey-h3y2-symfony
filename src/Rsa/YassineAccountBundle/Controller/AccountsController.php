<?php

namespace Rsa\YassineAccountBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Rsa\YassineAccountBundle\Entity\Registereduser;
use Rsa\YassineAccountBundle\Entity\RegistereduserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AccountsController extends Controller {

    public function accountAreaPageAction() {

        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>

        $em = $this->getDoctrine()->getManager();

        $theUser = $em->getRepository("RsaYassineAccountBundle:Registereduser")->findOneById($userId);

        if ($theUser == null) {
            // Search FAILED
            $message = '<b> ERROR : No such user ! userId :' . $userId . '</b> .';
            $currentUrl = $this->getRequest()->getUri();
            $currentAction = $this->getRequest()->attributes->get('_controller');
            return new Response($message . '<br/> DEBUGGING : Line' . __LINE__ . ' - File : ' . __FILE__ . ' .');
        } else {
            // Search SUCCEEDED
            return $this->render('RsaYassineAccountBundle:Auth:account.html.twig', array('theUser' => $theUser));
        }
    }

    public function logoutPageAction() {

        $session = $this->get('session');

        if ($session->has('connectedUserId')) {
            $session->remove('connectedUserId');
        }

        return $this->redirectToRoute('rsa_houssem_welcomeRent');
    }

    public function contactPageAction() {

//      $currentUrl = $this->getRequest()->getUri();
//      $currentAction = $this->getRequest()->attributes->get('_controller');
//      return new Response('Action : <b>'.$currentAction.' .</b> <br/> URL :     <b>'.$currentUrl.'</b> .');

        $etudiants = array();

        return $this->render('RsaYassineAccountBundle:Contact:contact.html.twig', array('etudiants' => $etudiants));
    }

}

// return new Response($message.'<br/> DEBUGGING : Line'.__LINE__.' - File : '.__FILE__.' .')