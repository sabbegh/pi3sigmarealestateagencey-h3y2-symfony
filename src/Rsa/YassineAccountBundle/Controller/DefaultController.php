<?php

namespace Rsa\YassineAccountBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('RsaYassineAccountBundle:Default:index.html.twig', array('name' => $name));
    }
}
