<?php

namespace Rsa\HajerRealtorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class addType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                

                ->add('champs','text',array('mapped'=>false))
            ->add('Save','submit')->getForm();
        ;
    }
    
      public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rsa\HajerRealtorBundle\Entity\Rentoffer'
        ));
    }

    public function getName() {
        return 'Rentoffer';
        
    }

//   
//    public function getName()
//    {
//        return 'Rentoffer';
//    }
}
