<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Rsa\HajerRealtorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\True;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;

//use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;

//use Gregwar\CaptchaBundle\GregwarCaptchaBundle;

class MailType extends AbstractType {
    
/**
 * @Recaptcha\True
 */
public $recaptcha;
    


   public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options) {
      $builder
              
    ->add('name','text')
    ->add('to','email')
    ->add('subject','text')
    ->add('text','textarea')
 //  ->add('recaptcha', 'ewz_recaptcha')
    ->add('recaptcha', 'ewz_recaptcha', array(
            'attr'          => array(
                'options' => array(
                    'theme' => 'clean'
                )
            ),
            'mapped' => false,
            'constraints'   => array(
                new True()
            ),
            'error_bubbling' => true
        ))

    ->add('Send','submit')->getForm();
   
   }
   

    
    
    
//Ce si est le nom de formulaire //
    public function getName() {
        return'Mail';
    }

}
