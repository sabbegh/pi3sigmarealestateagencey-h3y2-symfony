<?php


namespace Rsa\HajerRealtorBundle\Entity;
use Doctrine\ORM\EntityRepository;

class SaleofferRepository extends EntityRepository{
   
    

public function findAllSaleOffersDQL()
{
  $query = $this->_em->createQuery('SELECT ro FROM RsaHajerRealtorBundle:Saleoffer ro');
  $results = $query->getResult();

  return $results;
}

public function findDetailsSaleOffersDQL($idOffer)
{
  $query = $this->_em->createQuery('SELECT ro FROM RsaHajerRealtorBundle:Saleoffer ro where ro.id=:idOffer')
    ->setParameter('idOffer',$idOffer);
  $results = $query->getResult();

  return $results;
}

public function findRealtorDQL($idRealtor)
{
     $query=$this->getEntityManager()
   ->createQuery("SELECT c from  RsaHajerRealtorBundle:Registereduser c WHERE c.id= :idOffer")
    ->setParameter('idOffer',$idRealtor);
    return $query->getSingleResult();
    
}


public function findRealtorADQL($idRealtor)
{
  $query = $this->_em->createQuery('SELECT ro FROM RsaHajerRealtorBundle:Registereduser ro where ro.id=:idOffer')
    ->setParameter('idOffer',$idRealtor);
  $results = $query->getResult();

return $results;
  
}
public function findSaleOffersByRealtorDQL($idRealtor)
{
  $query = $this->_em->createQuery('SELECT ro FROM RsaHajerRealtorBundle:Saleoffer ro where ro.idofrealtor=:idRealtor')
    ->setParameter('idRealtor',$idRealtor);
  $results = $query->getResult();

  return $results;
}




public function updateSaleOfferDQL($id,$title,$state,$deal,$gov,$del,$gps,$descrip,$price,$from,$green,$imgUrl)
{
  
 $query = $this->_em->createQuery('UPDATE RsaHajerRealtorBundle:Saleoffer RO'
          . ' SET RO.title = :title,RO.state= :state,RO.dealtype= :deal,'
          . 'RO.governorate= :gov,RO.delegation= :del,RO.coordgps= :gps,'
          . 'RO.saleprice= :price,'
          . 'RO.availablefrom= :from,RO.isgreen= :green,RO.houseimageurl= :img,'
          . 'RO.body = :body where RO.id=:idRo')
    ->setParameter('idRo',$id)
    ->setParameter('title',$title)
    ->setParameter('state',$state)
    ->setParameter('deal',$deal)
    ->setParameter('gov',$gov)
    ->setParameter('del',$del)
    ->setParameter('gps',$gps)
    ->setParameter('price',$price)
    ->setParameter('from',$from)
    ->setParameter('green',$green)
    ->setParameter('img',$imgUrl)
    ->setParameter('body',$descrip);
    
    $query->execute();
}


public function closeSaleOfferDQL($id)
{
  
  $query = $this->_em->createQuery('UPDATE RsaHajerRealtorBundle:Saleoffer RO SET RO.state = :title where RO.id=:idRo')
    ->setParameter('idRo',$id)
    ->setParameter('title','Closed');

    $query->execute();
}

public function deleteSaleOfferDQL($id)
{
  
    
      $query = $this->_em->createQuery('UPDATE RsaHajerRealtorBundle:Saleoffer RO SET RO.state = :title where RO.id=:idRo')
    ->setParameter('idRo',$id)
    ->setParameter('title','Deleted');

    $query->execute();
}

public function searchMySaleOfferByTitleDQL($search,$id)
{
    $search=\strtolower($search);
   //$query = $this->_em->createQuery('SELECT commRO FROM RsaYasmineClientBundle:Commentaboutofferrent commRO WHERE LOWER(commRO.title) LIKE :LOWER(titleComm) and commRO.idofoffer=:idOffer ORDER BY commRO.date DESC' )
       $query = $this->_em->createQuery('SELECT commRO FROM RsaHajerRealtorBundle:Saleoffer commRO WHERE lower(commRO.title) LIKE :titleComm and commRO.idofrealtor=:idOffer ORDER BY commRO.id ASC' )
       ->setParameter('titleComm','%'.$search.'%') 
       ->setParameter('idOffer',$id);
   
    $results = $query->getResult();

  return $results;   
}



public function searchMySaleOfferByGovDQL($search,$id)
{
    $search=\strtolower($search);
       $query = $this->_em->createQuery('SELECT commRO FROM RsaHajerRealtorBundle:Saleoffer commRO WHERE lower(commRO.governorate) LIKE :titleComm and commRO.idofrealtor=:idOffer ORDER BY commRO.id ASC' )
       ->setParameter('titleComm','%'.$search.'%') 
       ->setParameter('idOffer',$id);
   
    $results = $query->getResult();

  return $results;   
}


public function searchMySaleOfferByDelDQL($search,$id)
{
    $search=\strtolower($search);
   //$query = $this->_em->createQuery('SELECT commRO FROM RsaYasmineClientBundle:Commentaboutofferrent commRO WHERE LOWER(commRO.title) LIKE :LOWER(titleComm) and commRO.idofoffer=:idOffer ORDER BY commRO.date DESC' )
       $query = $this->_em->createQuery('SELECT commRO FROM RsaHajerRealtorBundle:Saleoffer commRO WHERE lower(commRO.delegation) LIKE :titleComm and commRO.idofrealtor=:idOffer ORDER BY commRO.id ASC' )
       ->setParameter('titleComm','%'.$search.'%') 
       ->setParameter('idOffer',$id);
   
    $results = $query->getResult();

  return $results;   
}

}
