<?php

namespace Rsa\HajerRealtorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Secondaryimagesrentoffer
 *
 * @ORM\Table(name="SecondaryImagesRentOffer", indexes={@ORM\Index(name="idOfOffer", columns={"idOfOffer"})})
 * @ORM\Entity
 */
class Secondaryimagesrentoffer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="secondaryHouseImageUrl", type="string", length=128, nullable=false)
     */
    private $secondaryhouseimageurl;

    /**
     * @var \Rentoffer
     *
     * @ORM\ManyToOne(targetEntity="Rentoffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfOffer", referencedColumnName="id")
     * })
     */
    private $idofoffer;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set secondaryhouseimageurl
     *
     * @param string $secondaryhouseimageurl
     * @return Secondaryimagesrentoffer
     */
    public function setSecondaryhouseimageurl($secondaryhouseimageurl)
    {
        $this->secondaryhouseimageurl = $secondaryhouseimageurl;

        return $this;
    }

    /**
     * Get secondaryhouseimageurl
     *
     * @return string 
     */
    public function getSecondaryhouseimageurl()
    {
        return $this->secondaryhouseimageurl;
    }

    /**
     * Set idofoffer
     *
     * @param \Rsa\HajerRealtorBundle\Entity\Rentoffer $idofoffer
     * @return Secondaryimagesrentoffer
     */
    public function setIdofoffer(\Rsa\HajerRealtorBundle\Entity\Rentoffer $idofoffer = null)
    {
           if (is_null($idofoffer)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }

        $this->idofoffer = $idofoffer;

        return $this;
    }

    /**
     * Get idofoffer
     *
     * @return \Rsa\HajerRealtorBundle\Entity\Rentoffer 
     */
    public function getIdofoffer()
    {
        return $this->idofoffer;
    }
}
