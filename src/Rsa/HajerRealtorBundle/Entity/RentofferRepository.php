<?php


namespace Rsa\HajerRealtorBundle\Entity;
use Doctrine\ORM\EntityRepository;

class RentofferRepository extends EntityRepository{
   
    

public function findAllRentOffersDQL()
{
  $query = $this->_em->createQuery('SELECT ro FROM RsaHajerRealtorBundle:Rentoffer ro');
  $results = $query->getResult();

  return $results;
}

public function findDetailsRentOffersDQL($idOffer)
{
  $query = $this->_em->createQuery('SELECT ro FROM RsaHajerRealtorBundle:Rentoffer ro where ro.id=:idOffer')
    ->setParameter('idOffer',$idOffer);
  $results = $query->getResult();

  return $results;
}

public function findRealtorDQL($idRealtor)
{
    $query=$this->getEntityManager()
   ->createQuery("SELECT c from  RsaHajerRealtorBundle:Registereduser c WHERE c.id= :idOffer")
    ->setParameter('idOffer',$idRealtor);
    return $query->getSingleResult();

}


public function findRealtorADQL($idRealtor)
{
  $query = $this->_em->createQuery('SELECT ro FROM RsaHajerRealtorBundle:Registereduser ro where ro.id=:idOffer')
    ->setParameter('idOffer',$idRealtor);
  $results = $query->getResult();

return $results;
  
}

public function findRentOffersByRealtorDQL($idRealtor)
{
  $query = $this->_em->createQuery('SELECT ro FROM RsaHajerRealtorBundle:Rentoffer ro where ro.idofrealtor=:idRealtor')
    ->setParameter('idRealtor',$idRealtor);
  $results = $query->getResult();

  return $results;
}

public function updateRentOfferDQL($id,$title,$state,$deal,$gov,$del,$gps,$descrip,$price,$deposit,$status,$green,$imgUrl)
{
  
  $query = $this->_em->createQuery('UPDATE RsaHajerRealtorBundle:Rentoffer RO'
          . ' SET RO.title = :title,RO.state= :state,RO.dealtype= :deal,'
          . 'RO.governorate= :gov,RO.delegation= :del,RO.coordgps= :gps,'
          . 'RO.rentprice= :price,RO.depositvalue= :deposit,RO.status= :status,RO.isgreen= :green,RO.houseimageurl= :img,'
          . 'RO.body = :body where RO.id=:idRo')
    ->setParameter('idRo',$id)
    ->setParameter('title',$title)
    ->setParameter('state',$state)
    ->setParameter('deal',$deal)
    ->setParameter('gov',$gov)
    ->setParameter('del',$del)
    ->setParameter('gps',$gps)
    ->setParameter('deposit',$deposit)
    ->setParameter('price',$price)
    ->setParameter('status',$status)
    ->setParameter('green',$green)
    ->setParameter('img',$imgUrl)
    ->setParameter('body',$descrip);
                       
  
    $query->execute();
}


public function closeRentOfferDQL($id)
{
  
  $query = $this->_em->createQuery('UPDATE RsaHajerRealtorBundle:Rentoffer RO SET RO.state = :title where RO.id=:idRo')
    ->setParameter('idRo',$id)
    ->setParameter('title','Closed');

    $query->execute();
}

public function deleteRentOfferDQL($id)
{
  $query = $this->_em->createQuery('UPDATE RsaHajerRealtorBundle:Rentoffer RO SET RO.state = :title where RO.id=:idRo')
    ->setParameter('idRo',$id)
    ->setParameter('title','Deleted');

    $query->execute();
  
}

public function searchMyRentOfferByTitleDQL($search,$id)
{
    $search=\strtolower($search);
   //$query = $this->_em->createQuery('SELECT commRO FROM RsaYasmineClientBundle:Commentaboutofferrent commRO WHERE LOWER(commRO.title) LIKE :LOWER(titleComm) and commRO.idofoffer=:idOffer ORDER BY commRO.date DESC' )
       $query = $this->_em->createQuery('SELECT commRO FROM RsaHajerRealtorBundle:Rentoffer commRO WHERE lower(commRO.title) LIKE :titleComm and commRO.idofrealtor=:idOffer ORDER BY commRO.id ASC' )
       ->setParameter('titleComm','%'.$search.'%') 
       ->setParameter('idOffer',$id);
   
    $results = $query->getResult();

  return $results;   
}

public function searchMyRentOfferByGovDQL($search,$id)
{
    $search=\strtolower($search);
   //$query = $this->_em->createQuery('SELECT commRO FROM RsaYasmineClientBundle:Commentaboutofferrent commRO WHERE LOWER(commRO.title) LIKE :LOWER(titleComm) and commRO.idofoffer=:idOffer ORDER BY commRO.date DESC' )
       $query = $this->_em->createQuery('SELECT commRO FROM RsaHajerRealtorBundle:Rentoffer commRO WHERE lower(commRO.governorate) LIKE :titleComm and commRO.idofrealtor=:idOffer ORDER BY commRO.id ASC' )
       ->setParameter('titleComm','%'.$search.'%') 
       ->setParameter('idOffer',$id);
   
    $results = $query->getResult();

  return $results;   
}


public function searchMyRentOfferByDelDQL($search,$id)
{
    $search=\strtolower($search);
   //$query = $this->_em->createQuery('SELECT commRO FROM RsaYasmineClientBundle:Commentaboutofferrent commRO WHERE LOWER(commRO.title) LIKE :LOWER(titleComm) and commRO.idofoffer=:idOffer ORDER BY commRO.date DESC' )
       $query = $this->_em->createQuery('SELECT commRO FROM RsaHajerRealtorBundle:Rentoffer commRO WHERE lower(commRO.delegation) LIKE :titleComm and commRO.idofrealtor=:idOffer ORDER BY commRO.id ASC' )
       ->setParameter('titleComm','%'.$search.'%') 
       ->setParameter('idOffer',$id);
   
    $results = $query->getResult();

  return $results;   
}

}
