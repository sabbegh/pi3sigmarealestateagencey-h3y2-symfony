<?php

namespace Rsa\HajerRealtorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historyofviewedsaleoffers
 *
 * @ORM\Table(name="HistoryOfViewedSaleOffers", indexes={@ORM\Index(name="idOfClient", columns={"idOfClient"}), @ORM\Index(name="idOfOffer", columns={"idOfOffer"})})
 * @ORM\Entity
 */
class Historyofviewedsaleoffers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeOfView", type="date", nullable=false)
     */
    private $timeofview;

    /**
     * @var \Saleoffer
     *
     * @ORM\ManyToOne(targetEntity="Saleoffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfOffer", referencedColumnName="id")
     * })
     */
    private $idofoffer;

    /**
     * @var \Registereduser
     *
     * @ORM\ManyToOne(targetEntity="Registereduser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfClient", referencedColumnName="id")
     * })
     */
    private $idofclient;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timeofview
     *
     * @param \DateTime $timeofview
     * @return Historyofviewedsaleoffers
     */
    public function setTimeofview($timeofview)
    {
        $this->timeofview = $timeofview;

        return $this;
    }

    /**
     * Get timeofview
     *
     * @return \DateTime 
     */
    public function getTimeofview()
    {
        return $this->timeofview;
    }

    /**
     * Set idofoffer
     *
     * @param \Rsa\HajerRealtorBundle\Entity\Saleoffer $idofoffer
     * @return Historyofviewedsaleoffers
     */
    public function setIdofoffer(\Rsa\HajerRealtorBundle\Entity\Saleoffer $idofoffer = null)
    {
        if (is_null($idofoffer)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        
        $this->idofoffer = $idofoffer;

        return $this;
    }

    /**
     * Get idofoffer
     *
     * @return \Rsa\HajerRealtorBundle\Entity\Saleoffer 
     */
    public function getIdofoffer()
    {
        return $this->idofoffer;
    }

    /**
     * Set idofclient
     *
     * @param \Rsa\HajerRealtorBundle\Entity\Registereduser $idofclient
     * @return Historyofviewedsaleoffers
     */
    public function setIdofclient(\Rsa\HajerRealtorBundle\Entity\Registereduser $idofclient = null)
    {
        
            if (is_null($idofclient)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        
        $this->idofclient = $idofclient;

        return $this;
    }

    /**
     * Get idofclient
     *
     * @return \Rsa\HajerRealtorBundle\Entity\Registereduser 
     */
    public function getIdofclient()
    {
        return $this->idofclient;
    }
}
