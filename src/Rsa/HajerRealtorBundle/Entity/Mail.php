<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Rsa\HajerRealtorBundle\Entity;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;

/**
 * Description of Mail
 *
 * @author admin
 */
class Mail {
    
    private $name;
    private $subject;
    private $to;
    private $text;
    
 /**
 * @Recaptcha\True
 */
public $recaptcha;
    
    function getName() {
        return $this->name;
    }

    function getSubject() {
        return $this->subject;
    }

    function getTo() {
        return $this->to;
    }

    function getText() {
        return $this->text;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setSubject($subject) {
        $this->subject = $subject;
    }

    function setTo($to) {
        $this->to = $to;
    }

    function setText($text) {
        $this->text = $text;
    }


}
