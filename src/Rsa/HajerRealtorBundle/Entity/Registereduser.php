<?php

namespace Rsa\HajerRealtorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Registereduser
 *
 * @ORM\Table(name="RegisteredUser", uniqueConstraints={@ORM\UniqueConstraint(name="email", columns={"email"})})
 * @ORM\Entity(repositoryClass="Rsa\HajerRealtorBundle\Entity\RegistereduserRepository")
 */
class Registereduser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="userType", type="string", nullable=true)
     */
    private $usertype;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=64, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=128, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=64, nullable=false)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=64, nullable=false)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="userImageUrl", type="string", length=128, nullable=false)
     */
    private $userimageurl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date", nullable=false)
     */
    private $birthday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDeletedByAdmin", type="boolean", nullable=false)
     */
    private $isdeletedbyadmin = '0';



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usertype
     *
     * @param string $usertype
     * @return Registereduser
     */
    public function setUsertype($usertype)
    {
if ($usertype === "client" || $usertype === "realtor"|| $usertype === "administrator"){
    throw new \Psr\Log\InvalidArgumentException("setter for Registred "
            . "User.usertype : ==> $usertype <== is invalid ");
}
        
    $this->usertype = $usertype;
        return $this;
    }

    /**
     * Get usertype
     *
     * @return string 
     */
    public function getUsertype()
    {
        return $this->usertype;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Registereduser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Registereduser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Registereduser
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Registereduser
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set userimageurl
     *
     * @param string $userimageurl
     * @return Registereduser
     */
    public function setUserimageurl($userimageurl)
    {
        $this->userimageurl = $userimageurl;

        return $this;
    }

    /**
     * Get userimageurl
     *
     * @return string 
     */
    public function getUserimageurl()
    {
        return $this->userimageurl;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return Registereduser
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set isdeletedbyadmin
     *
     * @param boolean $isdeletedbyadmin
     * @return Registereduser
     */
    public function setIsdeletedbyadmin($isdeletedbyadmin)
    {
        $this->isdeletedbyadmin = $isdeletedbyadmin;

        return $this;
    }

    /**
     * Get isdeletedbyadmin
     *
     * @return boolean 
     */
    public function getIsdeletedbyadmin()
    {
        return $this->isdeletedbyadmin;
    }
}
