<?php


namespace Rsa\HajerRealtorBundle\Entity;
use Doctrine\ORM\EntityRepository;

class RegistereduserRepository extends EntityRepository{
   
public function findRealtorDQL($idRealtor)
{
  $query = $this->_em->createQuery('SELECT ro FROM RsaHajerRealtorBundle:Registereduser ro where ro.id=:idOffer')
    ->setParameter('idOffer',$idRealtor);
  $results = $query->getResult();

  return $results;
}

}
