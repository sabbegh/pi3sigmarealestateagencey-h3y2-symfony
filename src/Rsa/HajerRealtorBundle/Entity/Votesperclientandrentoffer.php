<?php

namespace Rsa\HajerRealtorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Votesperclientandrentoffer
 *
 * @ORM\Table(name="VotesPerClientAndRentOffer", indexes={@ORM\Index(name="registeredUserId", columns={"registeredUserId"}), @ORM\Index(name="offerId", columns={"offerId"})})
 * @ORM\Entity
 */
class Votesperclientandrentoffer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="voteIsPlus", type="boolean", nullable=false)
     */
    private $voteisplus;

    /**
     * @var \Rentoffer
     *
     * @ORM\ManyToOne(targetEntity="Rentoffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="offerId", referencedColumnName="id")
     * })
     */
    private $offerid;

    /**
     * @var \Registereduser
     *
     * @ORM\ManyToOne(targetEntity="Registereduser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="registeredUserId", referencedColumnName="id")
     * })
     */
    private $registereduserid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set voteisplus
     *
     * @param boolean $voteisplus
     * @return Votesperclientandrentoffer
     */
    public function setVoteisplus($voteisplus)
    {
        $this->voteisplus = $voteisplus;

        return $this;
    }

    /**
     * Get voteisplus
     *
     * @return boolean 
     */
    public function getVoteisplus()
    {
        return $this->voteisplus;
    }

    /**
     * Set offerid
     *
     * @param \Rsa\HajerRealtorBundle\Entity\Rentoffer $offerid
     * @return Votesperclientandrentoffer
     */
    public function setOfferid(\Rsa\HajerRealtorBundle\Entity\Rentoffer $offerid = null)
    {
        
           if (is_null($offerid)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->offerid = $offerid;

        return $this;
    }

    /**
     * Get offerid
     *
     * @return \Rsa\HajerRealtorBundle\Entity\Rentoffer 
     */
    public function getOfferid()
    {
        return $this->offerid;
    }

    /**
     * Set registereduserid
     *
     * @param \Rsa\HajerRealtorBundle\Entity\Registereduser $registereduserid
     * @return Votesperclientandrentoffer
     */
    public function setRegistereduserid(\Rsa\HajerRealtorBundle\Entity\Registereduser $registereduserid = null)
    {
          if (is_null($registereduserid)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->registereduserid = $registereduserid;

        return $this;
    }

    /**
     * Get registereduserid
     *
     * @return \Rsa\HajerRealtorBundle\Entity\Registereduser 
     */
    public function getRegistereduserid()
    {
        return $this->registereduserid;
    }
}
