<?php

namespace Rsa\HajerRealtorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Rsa\HajerRealtorBundle\Entity\Mail;
use Symfony\Component\Form\FormView;
use Rsa\HajerRealtorBundle\Form\MailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;


class MailController extends Controller {

    public function indexAction() {
        return $this->render('RsaHajerRealtorBundle:Default:mail.html.twig', array());
    }

    public function redirectionAction() {
        return $this->redirect($this->generateUrl('profile', array('id' => 2)));
    }

    public function sendMailAction($idC, $idR) {

        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');


        $request = $this->getRequest();
//        $session = $request->getSession();
//        $idRealtor=$session->get('idRealtor');

        $em = $this->getDoctrine()->getManager();
        $realtor = $em->getRepository("RsaHajerRealtorBundle:Registereduser")
                ->findRealtorDQL($idR);


        $km = $this->getDoctrine()->getManager();
        $client = $km->getRepository("RsaHajerRealtorBundle:Registereduser")
                ->findRealtorDQL($idC);



        $mail = new Mail();
        $form = $this->createForm(new MailType(), $mail);

        if ($request->getMethod() == 'POST') {

            $subject = $request->get("subject");
            $to = $request->get("email");
            $from = $request->get("emailf");
            $text = " Vous avez reçu un message de " . $request->get("name") . " ( " . $from . " ) : \n  ";
            $text = $text . $request->get("message");


            $message = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom($from) // 'bobo75055@gmail.com'
                    ->setTo($to) //'bibi75055@gmail.com' 
                    ->setBody($text); //'Hello world'
            //envoi du message
            $this->get('mailer')->send($message);
            
            $this->get('session')->getFlashBag()->add(
                    'notice', 'Email sent!'
            );
         //   return $this->redirect($this->generateUrl('hajer_realtorprofile', array('idC' => $idC, 'idR' => $idR)));
       
              return $this->render('RsaHajerRealtorBundle:Realtor:profile.html.twig', array('realtor' => $realtor, 'client' => $client));

            
        }

        return $this->render('RsaHajerRealtorBundle:Mail:formMail.html.twig', array('realtor' => $realtor, 'client' => $client, 'form' => $form->createView()));
    }

}
