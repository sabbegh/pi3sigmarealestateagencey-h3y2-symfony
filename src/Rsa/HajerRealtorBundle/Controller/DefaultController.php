<?php

namespace Rsa\HajerRealtorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('RsaHajerRealtorBundle:Default:index.html.twig', array('name' => $name));
    }
}
