<?php

namespace Rsa\HajerRealtorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Rsa\HajerRealtorBundle\Entity\Saleoffer;
use Rsa\HajerRealtorBundle\Entity\SaleofferRepository;
use Symfony\Component\HttpFoundation\Session\Session;

class SaleOfferController extends Controller {

    public function indexAction($id) {
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $id = $session->get('connectedUserId');
//        $session = new Session();
//        $session->start();
//        $session->set('idRealtor',$id);
        $em = $this->getDoctrine()->getManager();
        $ro = $em->getRepository("RsaHajerRealtorBundle:Registereduser")
                ->findRealtorDQL($idRealtor);


        return $this->render('RsaHajerRealtorBundle:Default:index.html.twig', array('realtor' => $ro));
    }

    public function addAction() {
        
           $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');


        $Request = $this->getRequest();
//        $session = new Session();
//        $idRealtor = $session->get('idRealtor');
//        echo "Bonjour " . $idRealtor;
//        $Request = $this->getRequest();
        
        
        
        if ($Request->getMethod() == "POST") {
            //instanciation de Modele
            $Modele = new Saleoffer();
            //recuperation des données
            $state = $Request->get('state');
            $title = $Request->get('title');
            $deal = $Request->get('deal');
            $gov = $Request->get('gov');
            $del = $Request->get('del');
            $gps = $Request->get('gps');
            $descrip = $Request->get('descrip');
            $price = $Request->get('price');
            $from = $Request->get('date');
            $green = $Request->get('green');
            $imgUrl = $Request->get('imgUrl');
#libelle en orangé, c'est le name dans la page ajout..
//            $course = $this->getDoctrine()->getManager()->getRepository('RsaHajerRealtorBundle:Registereduser')->findRealtorDQL($idRealtor);
//
//            $Modele->setIdofrealtor($course);
            $course = $this->getDoctrine()->getManager()->getRepository('RsaHajerRealtorBundle:Rentoffer')->findRealtorDQL($idRealtor);

            $Modele->setIdofrealtor($course);
            $Modele->setSaleprice($price);



            if ($green == "yes") {
                $Modele->setIsgreen(true);
            } else {
                $Modele->setIsgreen(false);
            }
//$from=date("Y/m/d", strtotime($from));
//$dateFormated = split("/",$from);
            //$from = $dateFormated[2]."-".$dateFormated[0]."-".$dateFormated[1];  
            //$from=date('Y-m-d', strtotime(str_replace('-','/', $from)));
//str_replace('-','/', $from);
            $date11 = new \DateTime($from);
            $date11->format('Y-m-d');
//echo $date11->format('Y-m-d');
//$date=date("Y-m-d",strtotime($date));
            $Modele->setAvailablefrom($date11);
//            $originalDate = date("d-m-Y");
//$newDate = date("d-m-Y", strtotime($originalDate));
//date("Y-m-d H:i:s", $unixTimestamp);
            // $Modele->setAvailablefrom(new \DateTime());



            $Modele->setState($state);
            $Modele->setTitle($title);
            $Modele->setBody($descrip);
            $Modele->setGovernorate($gov);
            $Modele->setDelegation($del);
            $Modele->setCoordgps($gps);

            $Modele->setNbvotesplus(0);
            $Modele->setNbvotesmoins(0);

            $Modele->setDealtype($deal);
            $Modele->setHouseimageurl($imgUrl);
            //  echo "Bonjour ".$imgUrl;
            //instanciation de l'em

            $em = $this->getDoctrine()->getManager();
            $em->persist($Modele);
            $em->flush(); // permet enregistrement dans la base de données
        
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Offer added successfully!'
            );
            }

        return $this->render('RsaHajerRealtorBundle:SaleOffer:add.html.twig', array()); # pour faire le lien entre controller et page.
    }

    public function listSaleOffersAction() {

        
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');
        
        $em = $this->getDoctrine()->getManager();
        $ro = $em->getRepository("RsaHajerRealtorBundle:Saleoffer")
                ->findAllSaleOffersDQL();
        return ($this->render("RsaHajerRealtorBundle:SaleOffer:listSaleOffers.html.twig"
                        , array('modeles' => $ro)));
    }

    public function detailedSaleOfferAction($id) {

        
        
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');


//       
//        $session = new Session();
//        $idRealtor = $session->get('idRealtor');
        $em = $this->getDoctrine()->getManager();
        $roDetails = $em->getRepository("RsaHajerRealtorBundle:Saleoffer")
                ->findDetailsSaleOffersDQL($id);

        return ($this->render("RsaHajerRealtorBundle:SaleOffer:detailedSaleOffer.html.twig"
                        , array('modeles' => $roDetails)));
    }

    public function listSaleOffersByRealtorAction() {

        
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor= $session->get('connectedUserId');


        
//        $session = new Session();
//        $idRealtor = $session->get('idRealtor');

        $em = $this->getDoctrine()->getManager();
        $ro = $em->getRepository("RsaHajerRealtorBundle:Saleoffer")
                ->findSaleOffersByRealtorDQL($idRealtor);
        return ($this->render("RsaHajerRealtorBundle:SaleOffer:listSaleOffersByRealtor.html.twig"
                        , array('modeles' => $ro)));
    }

    public function modifySaleOfferAction($id) {

//        $session = new Session();
//        $idRealtor = $session->get('idRealtor');
//        echo "Bonjour realtor : " . $idRealtor;
        
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');


        $Request = $this->getRequest();
        
        $em = $this->getDoctrine()->getManager();

        $ro = $em->getRepository("RsaHajerRealtorBundle:Saleoffer")
                ->findDetailsSaleOffersDQL($id);



        //exécution de la méthode get qui est implémentée dans la classe mere Controller
        //appel au service request( objet) qui nous permet de récupérer la requête
        $Request = $this->get('request');
        //instantiation de CommentRentOffer
        $Modele = new Saleoffer();
        //$date->setDays(new \DateTime());
        //on teste si la requête est porteuse de données ou non 
        if ($Request->getMethod() == "POST") {
            //Récupération des données
            $state = $Request->get('state');
            $title = $Request->get('title');
            $deal = $Request->get('deal');
            $gov = $Request->get('gov');
            $del = $Request->get('del');
            $gps = $Request->get('gps');
            $descrip = $Request->get('descrip');
            $price = $Request->get('price');
            $from = $Request->get('date');
            $green = $Request->get('green');
            $imgUrl = $Request->get('imgUrl');


            //Remplissage de l'objet
            $Modele->setSaleprice($price);


            if ($green == "yes") {
                $Modele->setIsgreen(true);
            } else {
                $Modele->setIsgreen(false);
            }

            $date11 = new \DateTime($from);
            $date11->format('Y-m-d');
            $Modele->setAvailablefrom($date11);
            $Modele->setState($state);
            $Modele->setTitle($title);
            $Modele->setBody($descrip);
            $Modele->setGovernorate($gov);
            $Modele->setDelegation($del);
            $Modele->setCoordgps($gps);
            $Modele->setDealtype($deal);
            $Modele->setHouseimageurl("http://i.imgur.com/" . $imgUrl);

            $em = $this->getDoctrine()->getManager();
            $em->getRepository("RsaHajerRealtorBundle:Saleoffer")
                    ->updateSaleOfferDQL($id, $title, $state, $deal, $gov, $del, $gps, $descrip, $price, $from, $green, $imgUrl);

            //Redirection :
            /*  $commRORedirect=$em->getRepository("RsaYasmineClientBundle:Commentaboutofferrent")
              ->findAllCommentsRoDQL($id); */

//            return $this->redirectToRoute("hajer_listSaleOffers");
            /*
              $commRORedirect=$em->getRepository("RsaYasmineClientBundle:Commentaboutofferrent")
              ->findAllCommentsRoDQL($id);
              return ($this->render("RsaYasmineClientBundle:RentOffer:ListCommentRent.html.twig"
              ,array('CommentsRO'=>$commRORedirect)));
             */
            
             $this->get('session')->getFlashBag()->add(
                    'notice', 'Offer modified successfully!'
            );
        }
        return ($this->render("RsaHajerRealtorBundle:SaleOffer:modifySaleOffer.html.twig"
                        , array('modeles' => $ro)));
    }

    public function deleteSaleOfferAction($id) {
        
        
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');

        
        $em = $this->getDoctrine()->getManager();
        $em->getRepository("RsaHajerRealtorBundle:Saleoffer")
                ->deleteSaleOfferDQL($id);
  $this->get('session')->getFlashBag()->add(
                'notice', 'Offer deleted successfully!'
        );

        return $this->redirectToRoute("hajer_listSaleOffersByRealtor");
    }

    public function closeSaleOfferAction($id) {
        
        
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');

        $em = $this->getDoctrine()->getManager();
        $em->getRepository("RsaHajerRealtorBundle:Saleoffer")
                ->closeSaleOfferDQL($id);
 $this->get('session')->getFlashBag()->add(
                'notice', 'Offer closed successfully!'
        );
        return $this->redirectToRoute("hajer_listSaleOffersByRealtor");
    }
    

    
    public function deleteSaleOfferSAction($id) {
        
        
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');

        
        $em = $this->getDoctrine()->getManager();
        $em->getRepository("RsaHajerRealtorBundle:Saleoffer")
                ->deleteSaleOfferDQL($id);
  $this->get('session')->getFlashBag()->add(
                'notice', 'Offer deleted successfully!'
        );

        return $this->redirectToRoute("hajer_searchMySaleOfferByTitle");
    }

    public function closeSaleOfferSAction($id) {
        
        
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');

        $em = $this->getDoctrine()->getManager();
        $em->getRepository("RsaHajerRealtorBundle:Saleoffer")
                ->closeSaleOfferDQL($id);
 $this->get('session')->getFlashBag()->add(
                'notice', 'Offer closed successfully!'
        );
        return $this->redirectToRoute("hajer_searchMySaleOfferByTitle");
    }
    
    


    function searchMySaleOfferByTitleAction() {
         $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $id = $session->get('connectedUserId');


        $request = $this->getRequest();
        
//        $session = new Session();
//        $id = $session->get('idRealtor');
//        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $modeles = $em->getRepository('RsaHajerRealtorBundle:Saleoffer')->findSaleOffersByRealtorDQL($id);
        if ($request->getMethod() == "POST") {
            $search = $request->get('search');
            $choix = $request->get('choix');
            if ($choix=="Title") {
                   $modeles = $em->getRepository('RsaHajerRealtorBundle:Saleoffer')
                    ->searchMySaleOfferByTitleDQL($search, $id);
            } elseif ($choix=="Gov") {


                  $modeles = $em->getRepository('RsaHajerRealtorBundle:Saleoffer')
                    ->searchMySaleOfferByGovDQL($search, $id);
            } else {
                   $modeles = $em->getRepository('RsaHajerRealtorBundle:Saleoffer')
                    ->searchMySaleOfferByDelDQL($search, $id);
            }      
        }

        return $this->render('RsaHajerRealtorBundle:SaleOffer:searchMySaleOfferByTitle.html.twig', array('modeles' => $modeles));
    }

}
