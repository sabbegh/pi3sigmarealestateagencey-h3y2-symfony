<?php

namespace Rsa\HajerRealtorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormView;
use Rsa\HajerRealtorBundle\Form\addType;
use Rsa\HajerRealtorBundle\Entity\Rentoffer;
use Rsa\HajerRealtorBundle\Entity\RentofferRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use Rsa\HajerRealtorBundle\Entity\Registereduser;
use Rsa\HajerRealtorBundle\Entity\RegistereduserRepository;

class RentOfferController extends Controller {

    public function accAction() {
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $id = $session->get('connectedUserId');

//        $session = new Session();
//        $session->start();
//        $session->set('idRealtor',$id);

        $em = $this->getDoctrine()->getManager();
        $ro = $em->getRepository("RsaHajerRealtorBundle:Rentoffer")
                ->findRealtorADQL($id);


        return $this->render('RsaHajerRealtorBundle:RentOffer:indexe.html.twig', array('realtor' => $ro));
    }

    public function newAction() {
        // create a task and give it some dummy data for this example
        $request = $this->getRequest();
        $session = $request->getSession();
        //  $session = new Session();
        $idRealtor = $session->get('idRealtor');
        $task = new Rentoffer();
        $task->setNbvotesplus(0);
        $task->setNbvotesmoins(0);
        // $request=$this->getRequest();
        //  $task->setTitle('Write a blog post');

        $course = $this->getDoctrine()->getManager()->getRepository('RsaHajerRealtorBundle:Rentoffer')->findRealtorDQL($idRealtor);

        $task->setIdofrealtor($course);

        $task->setAvailablefrom(new \DateTime('tomorrow'));

        $form = $this->createFormBuilder($task)
                ->add('state', 'choice', array(
                    'choices' => array('Available' => 'Available', 'Closed' => 'Closed', 'Sold' => 'Sold', 'Deleted' => 'Deleted'),
                    'required' => true,))
                ->add('availablefrom', 'date', array('label' => 'Date',))
                ->add('title', 'text', array('label' => 'Title',))
                ->add('dealtype', 'choice', array(
                    'choices' => array('Apartment' => 'Apartment', 'Warehouse' => 'Warehouse', 'Empty Lot' => 'Empty Lot', 'Villa' => 'Villa'),
                    'required' => true,))
                ->add('governorate', 'text', array('required' => true,))
                ->add('delegation', 'text', array('required' => true,))
                ->add('coordgps', 'textarea', array('required' => true, 'label' => 'Address',))
                ->add('body', 'textarea', array('label' => 'Description',))
                ->add('rentprice', 'integer', array('required' => true, 'label' => 'Rent price',))
                ->add('depositvalue', 'integer', array('required' => true, 'label' => 'Deposit value',))
                ->add('status', 'text', array('required' => true,))
//                ->add('nbvotesplus', 'hidden')
//                ->add('nbvotesmoins', 'hidden')
                // ->add('isgreen', 'textarea', array(    'required'  => true,))
                ->add('isgreen', 'choice', array(
                    'choices' => array('false' => 'No', 'true' => 'Yes'),
                    'expanded' => true,
                    'data' => 'false',
                    'label' => 'Is green',
                ))
                ->add('houseimageurl', 'file', array('attr' => array(
                    //  'placeholder' => 'http://i.imgur.com/575pfPX.jpg',
                    ), 'required' => true, 'label' => ' Image ',))

//            ->add('houseimageurl', 'text', array( 'attr' => array(
//             'placeholder' => 'http://i.imgur.com/575pfPX.jpg',
//        ),    'required'  => true,))
                ->add('save', 'submit', array('label' => 'Add '))
                ->getForm();



        if ($form->handleRequest($request)->isValid()) {
            $objetToPersist = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($objetToPersist);
            $em->flush();
        }

        return $this->render('RsaHajerRealtorBundle:RentOffer:new.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function addAction() {

        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');


        $Request = $this->getRequest();
//       $session = $Request->getSession();
//        $idRealtor=$session->get('idRealtor');

        $Modele = new Rentoffer();



        if ($Request->getMethod() == "POST") {

            //instanciation du Modele
            //recuperation des données
            $state = $Request->get('state');
            $title = $Request->get('title');
            $deal = $Request->get('deal');
            $gov = $Request->get('gov');
            $del = $Request->get('del');
            $gps = $Request->get('gps');
            $descrip = $Request->get('descrip');
            $price = $Request->get('price');
            $deposit = $Request->get('deposit');
            $status = $Request->get('status');
            $from = $Request->get('date');
            $green = $Request->get('green');
            $imgUrl = $Request->get('imgUrl');
#libelle en orangé, c'est le name dans la page ajout..
            $course = $this->getDoctrine()->getManager()->getRepository('RsaHajerRealtorBundle:Rentoffer')->findRealtorDQL($idRealtor);

            $Modele->setIdofrealtor($course);
            $Modele->setRentprice($price);
            $Modele->setDepositvalue($deposit);


            if ($green == "yes") {
                $Modele->setIsgreen(true);
            } else {
                $Modele->setIsgreen(false);
            }
//$from=date("Y/m/d", strtotime($from));
//$dateFormated = split("/",$from);
            //$from = $dateFormated[2]."-".$dateFormated[0]."-".$dateFormated[1];  
            //$from=date('Y-m-d', strtotime(str_replace('-','/', $from)));
//str_replace('-','/', $from);
            $date11 = new \DateTime($from);
            $date11->format('Y-m-d');
//echo $date11->format('Y-m-d');
//$date=date("Y-m-d",strtotime($date));
            $Modele->setAvailablefrom($date11);
//            $originalDate = date("d-m-Y");
//$newDate = date("d-m-Y", strtotime($originalDate));
//date("Y-m-d H:i:s", $unixTimestamp);
            // $Modele->setAvailablefrom(new \DateTime());

            $Modele->setStatus($status);

            $Modele->setState($state);
            $Modele->setTitle($title);
            $Modele->setBody($descrip);
            $Modele->setGovernorate($gov);
            $Modele->setDelegation($del);
            $Modele->setCoordgps($gps);

            $Modele->setNbvotesplus(0);
            $Modele->setNbvotesmoins(0);

            $Modele->setDealtype($deal);
            $Modele->setHouseimageurl($imgUrl);
            //  echo "Bonjour ".$imgUrl;
            //instanciation de l'em

            $em = $this->getDoctrine()->getManager();
            $em->persist($Modele);
            $em->flush(); // permet enregistrement dans la base de données

            $this->get('session')->getFlashBag()->add(
                    'notice', 'Offer added successfully!'
            );
        }

        // $request = $this->container->get('request');


        return $this->render('RsaHajerRealtorBundle:RentOffer:add.html.twig', array()); # pour faire le lien entre controller et page.
    }

    public function listRentOffersAction() {


        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');




        $em = $this->getDoctrine()->getManager();
        $ro = $em->getRepository("RsaHajerRealtorBundle:Rentoffer")
                ->findAllRentOffersDQL();
        return ($this->render("RsaHajerRealtorBundle:RentOffer:listRentOffers.html.twig"
                        , array('modeles' => $ro)));
    }

    public function detailedRentOfferAction($id) {


        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');



//    $session = new Session();
//        $idRealtor=$session->get('idRealtor');

        $em = $this->getDoctrine()->getManager();
        $roDetails = $em->getRepository("RsaHajerRealtorBundle:Rentoffer")
                ->findDetailsRentOffersDQL($id);

        return ($this->render("RsaHajerRealtorBundle:RentOffer:detailedRentOffer.html.twig"
                        , array('modeles' => $roDetails)));
    }

    public function listRentOffersByRealtorAction() {


        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');




        $em = $this->getDoctrine()->getManager();
        $ro = $em->getRepository("RsaHajerRealtorBundle:Rentoffer")
                ->findRentOffersByRealtorDQL($idRealtor);
        return ($this->render("RsaHajerRealtorBundle:RentOffer:listRentOffersByRealtor.html.twig"
                        , array('modeles' => $ro)));
    }

    public function modifyRentOfferAction($id) {


        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');


        $Request = $this->getRequest();
//         $session = new Session();
//        $idRealtor=$session->get('idRealtor');

        $em = $this->getDoctrine()->getManager();
        $ro = $em->getRepository("RsaHajerRealtorBundle:Rentoffer")
                ->findDetailsRentOffersDQL($id);



        //exécution de la méthode get qui est implémentée dans la classe mere Controller
        //appel au service request( objet) qui nous permet de récupérer la requête
        $Request = $this->get('request');
        //instantiation de CommentRentOffer
        $Modele = new Rentoffer();
        //$date->setDays(new \DateTime());
        //on teste si la requête est porteuse de données ou non 
        if ($Request->getMethod() == "POST") {
            //Récupération des données

            $state = $Request->get('state');
            $title = $Request->get('title');
            $deal = $Request->get('deal');
            $gov = $Request->get('gov');
            $del = $Request->get('del');
            $gps = $Request->get('gps');
            $descrip = $Request->get('descrip');
            $price = $Request->get('price');
            $deposit = $Request->get('deposit');
            $status = $Request->get('status');

            $green = $Request->get('green');
            $imgUrl = $Request->get('imgUrl');

            //Remplissage de l'objet
            $Modele->setRentprice($price);
            $Modele->setDepositvalue($deposit);

            if ($green == "yes") {
                $Modele->setIsgreen(true);
            } else {
                $Modele->setIsgreen(false);
            }


            $Modele->setStatus($status);
            $Modele->setState($state);
            $Modele->setTitle($title);
            $Modele->setBody($descrip);
            $Modele->setGovernorate($gov);
            $Modele->setDelegation($del);
            $Modele->setCoordgps($gps);
            $Modele->setDealtype($deal);
            $Modele->setHouseimageurl($imgUrl);


            $em = $this->getDoctrine()->getManager();
            $em->getRepository("RsaHajerRealtorBundle:Rentoffer")
                    ->updateRentOfferDQL($id, $title, $state, $deal, $gov, $del, $gps, $descrip, $price, $deposit, $status, $green, $imgUrl);

            //Redirection :
            /*  $commRORedirect=$em->getRepository("RsaYasmineClientBundle:Commentaboutofferrent")
              ->findAllCommentsRoDQL($id); */

            //return $this->redirectToRoute("hajer_listRentOffersByRealtor");
            /*
              $commRORedirect=$em->getRepository("RsaYasmineClientBundle:Commentaboutofferrent")
              ->findAllCommentsRoDQL($id);
              return ($this->render("RsaYasmineClientBundle:RentOffer:ListCommentRent.html.twig"
              ,array('CommentsRO'=>$commRORedirect)));
             */
            $this->get('session')->getFlashBag()->add(
                    'notice', 'Offer modified successfully!'
            );
        }
        return ($this->render("RsaHajerRealtorBundle:RentOffer:modifyRentOffer.html.twig"
                        , array('modeles' => $ro)));
    }

    public function deleteRentOfferAction($id) {


        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');


        $em = $this->getDoctrine()->getManager();
        $em->getRepository("RsaHajerRealtorBundle:Rentoffer")
                ->deleteRentOfferDQL($id);
        $this->get('session')->getFlashBag()->add(
                'notice', 'Offer deleted successfully!'
        );

        return $this->redirectToRoute("hajer_listRentOffersByRealtor");
    }

    public function closeRentOfferAction($id) {


        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');

        $em = $this->getDoctrine()->getManager();
        $em->getRepository("RsaHajerRealtorBundle:Rentoffer")
                ->closeRentOfferDQL($id);
        $this->get('session')->getFlashBag()->add(
                'notice', 'Offer closed successfully!'
        );
        return $this->redirectToRoute("hajer_listRentOffersByRealtor");
    }

    public function deleteRentOfferSAction($id) {

        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');



        $em = $this->getDoctrine()->getManager();
        $em->getRepository("RsaHajerRealtorBundle:Rentoffer")
                ->deleteRentOfferDQL($id);
        $this->get('session')->getFlashBag()->add(
                'notice', 'Offer deleted successfully!'
        );

        return $this->redirectToRoute("hajer_searchMyRentOfferByTitle");
    }

    public function closeRentOfferSAction($id) {


        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');


        $em = $this->getDoctrine()->getManager();
        $em->getRepository("RsaHajerRealtorBundle:Rentoffer")
                ->closeRentOfferDQL($id);
        $this->get('session')->getFlashBag()->add(
                'notice', 'Offer closed successfully!'
        );
        return $this->redirectToRoute("hajer_searchMyRentOfferByTitle");
    }

    function searchMyRentOfferByTitleAction() {


        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $id = $session->get('connectedUserId');


        $request = $this->getRequest();
//        $session = new Session();
//$id=$session->get('idRealtor');        
//                $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $modeles = $em->getRepository('RsaHajerRealtorBundle:Rentoffer')->findRentOffersByRealtorDQL($id);
        if ($request->getMethod() == "POST") {
            $search = $request->get('search');
            $choix = $request->get('choix');
            if ($choix=="Title") {
                $modeles = $em->getRepository('RsaHajerRealtorBundle:Rentoffer')
                        ->searchMyRentOfferByTitleDQL($search, $id);
            } elseif ($choix=="Gov") {


                $modeles = $em->getRepository('RsaHajerRealtorBundle:Rentoffer')
                        ->searchMyRentOfferByGovDQL($search, $id);
            } else {
                $modeles = $em->getRepository('RsaHajerRealtorBundle:Rentoffer')
                        ->searchMyRentOfferByDelDQL($search, $id);
            }
        }

        return $this->render('RsaHajerRealtorBundle:RentOffer:searchMyRentOfferByTitle.html.twig', array('modeles' => $modeles));
    }

}
