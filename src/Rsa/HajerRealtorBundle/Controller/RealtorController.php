<?php

namespace Rsa\HajerRealtorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Rsa\HajerRealtorBundle\Entity\Rentoffer;
use Rsa\HajerRealtorBundle\Entity\RentofferRepository;
use Symfony\Component\HttpFoundation\Session\Session;

class RealtorController extends Controller {

    public function profileAction($idC, $idR) {

        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $idRealtor = $session->get('connectedUserId');

//    $session = new Session();
//    $idRealtor=$session->get('idRealtor');

        $em = $this->getDoctrine()->getManager();
        $ro = $em->getRepository("RsaHajerRealtorBundle:Registereduser")
                ->findRealtorDQL($idR);

        $km = $this->getDoctrine()->getManager();
        $client = $km->getRepository("RsaHajerRealtorBundle:Registereduser")
                ->findRealtorDQL($idC);

        return ($this->render("RsaHajerRealtorBundle:Realtor:profile.html.twig"
                        , array('realtor' => $ro, 'client' => $client)));
    }

}
