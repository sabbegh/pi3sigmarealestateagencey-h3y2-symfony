<?php

namespace Rsa\HoussemBundle\Entity;

use Doctrine\ORM\EntityRepository;

class RentofferRepository extends EntityRepository
{
    
    public function getAll()
    {
       //quesry builder permet de créé des requette 
        $qb=$this->createQueryBuilder("s");
        $query=$qb;
        $result=$query->getQuery()->execute();
        return $result;
       
    }
     public function getbyid($id)
    {
       //quesry builder permet de créé des requette 
        $qb=$this->createQueryBuilder("s");
        $query=$qb
                    ->where("s.id = :id")
                    ->setParameter("id", "$id")  
                
              
                ;
        $result=$query->getQuery()->execute();
        return $result;
       
    }
    
    public function getbystring($searchKeyWord) {
       //quesry builder permet de créé des requette 
        $qb=$this->createQueryBuilder("s");
       
       $searchKeyWord="s.id != 0 ".$searchKeyWord;
        $query=$qb
                    ->where($searchKeyWord)
                    
                
              
                ;
        $result=$query->getQuery()->execute();
        return $result;
        
    }
}
