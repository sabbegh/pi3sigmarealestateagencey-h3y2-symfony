<?php

namespace Rsa\HoussemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Complaintbyclientaboutofferrent
 *
 * @ORM\Table(name="ComplaintByClientAboutOfferRent", indexes={@ORM\Index(name="idOfClient", columns={"idOfClient"}), @ORM\Index(name="idOfOffer", columns={"idOfOffer"})})
 * @ORM\Entity
 */
class Complaintbyclientaboutofferrent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="textOfComplaint", type="string", length=800, nullable=false)
     */
    private $textofcomplaint;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isResolvedByAdmin", type="boolean", nullable=false)
     */
    private $isresolvedbyadmin;

    /**
     * @var \Rentoffer
     *
     * @ORM\ManyToOne(targetEntity="Rentoffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfOffer", referencedColumnName="id")
     * })
     */
    private $idofoffer;

    /**
     * @var \Registereduser
     *
     * @ORM\ManyToOne(targetEntity="Registereduser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfClient", referencedColumnName="id")
     * })
     */
    private $idofclient;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set textofcomplaint
     *
     * @param string $textofcomplaint
     * @return Complaintbyclientaboutofferrent
     */
    public function setTextofcomplaint($textofcomplaint)
    {
        $this->textofcomplaint = $textofcomplaint;

        return $this;
    }

    /**
     * Get textofcomplaint
     *
     * @return string 
     */
    public function getTextofcomplaint()
    {
        return $this->textofcomplaint;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Complaintbyclientaboutofferrent
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set isresolvedbyadmin
     *
     * @param boolean $isresolvedbyadmin
     * @return Complaintbyclientaboutofferrent
     */
    public function setIsresolvedbyadmin($isresolvedbyadmin)
    {
        $this->isresolvedbyadmin = $isresolvedbyadmin;

        return $this;
    }

    /**
     * Get isresolvedbyadmin
     *
     * @return boolean 
     */
    public function getIsresolvedbyadmin()
    {
        return $this->isresolvedbyadmin;
    }

    /**
     * Set idofoffer
     *
     * @param \Rsa\HoussemBundle\Entity\Rentoffer $idofoffer
     * @return Complaintbyclientaboutofferrent
     */
    public function setIdofoffer(\Rsa\HoussemBundle\Entity\Rentoffer $idofoffer = null)
    {
        
         if (is_null($idofoffer)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->idofoffer = $idofoffer;

        return $this;
    }

    /**
     * Get idofoffer
     *
     * @return \Rsa\HoussemBundle\Entity\Rentoffer 
     */
    public function getIdofoffer()
    {
        return $this->idofoffer;
    }

    /**
     * Set idofclient
     *
     * @param \Rsa\HoussemBundle\Entity\Registereduser $idofclient
     * @return Complaintbyclientaboutofferrent
     */
    public function setIdofclient(\Rsa\HoussemBundle\Entity\Registereduser $idofclient = null)
    {
        if (is_null($idofclient)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->idofclient = $idofclient;

        return $this;
    }

    /**
     * Get idofclient
     *
     * @return \Rsa\HoussemBundle\Entity\Registereduser 
     */
    public function getIdofclient()
    {
        return $this->idofclient;
    }
}
