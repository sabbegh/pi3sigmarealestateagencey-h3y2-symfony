<?php

namespace Rsa\HoussemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historyofsearches
 *
 * @ORM\Table(name="HistoryOfSearches", indexes={@ORM\Index(name="idOfClient", columns={"idOfClient"})})
 * @ORM\Entity
 */
class Historyofsearches
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="searchText", type="string", length=64, nullable=false)
     */
    private $searchtext;

    /**
     * @var \Registereduser
     *
     * @ORM\ManyToOne(targetEntity="Registereduser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfClient", referencedColumnName="id")
     * })
     */
    private $idofclient;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set searchtext
     *
     * @param string $searchtext
     * @return Historyofsearches
     */
    public function setSearchtext($searchtext)
    {
        $this->searchtext = $searchtext;

        return $this;
    }

    /**
     * Get searchtext
     *
     * @return string 
     */
    public function getSearchtext()
    {
        return $this->searchtext;
    }

    /**
     * Set idofclient
     *
     * @param \Rsa\HoussemBundle\Entity\Registereduser $idofclient
     * @return Historyofsearches
     */
    public function setIdofclient(\Rsa\HoussemBundle\Entity\Registereduser $idofclient = null)
    {
        if (is_null($idofclient)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->idofclient = $idofclient;

        return $this;
    }

    /**
     * Get idofclient
     *
     * @return \Rsa\HoussemBundle\Entity\Registereduser 
     */
    public function getIdofclient()
    {
        return $this->idofclient;
    }
}
