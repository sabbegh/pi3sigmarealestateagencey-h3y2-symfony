<?php

namespace Rsa\HoussemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Votesperclientandsaleoffer
 *
 * @ORM\Table(name="VotesPerClientAndSaleOffer", indexes={@ORM\Index(name="registeredUserId", columns={"registeredUserId"}), @ORM\Index(name="offerId", columns={"offerId"})})
 * @ORM\Entity
 */
class Votesperclientandsaleoffer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="voteIsPlus", type="boolean", nullable=false)
     */
    private $voteisplus;

    /**
     * @var \Saleoffer
     *
     * @ORM\ManyToOne(targetEntity="Saleoffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="offerId", referencedColumnName="id")
     * })
     */
    private $offerid;

    /**
     * @var \Registereduser
     *
     * @ORM\ManyToOne(targetEntity="Registereduser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="registeredUserId", referencedColumnName="id")
     * })
     */
    private $registereduserid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set voteisplus
     *
     * @param boolean $voteisplus
     * @return Votesperclientandsaleoffer
     */
    public function setVoteisplus($voteisplus)
    {
        $this->voteisplus = $voteisplus;

        return $this;
    }

    /**
     * Get voteisplus
     *
     * @return boolean 
     */
    public function getVoteisplus()
    {
        return $this->voteisplus;
    }

    /**
     * Set offerid
     *
     * @param \Rsa\HoussemBundle\Entity\Saleoffer $offerid
     * @return Votesperclientandsaleoffer
     */
    public function setOfferid(\Rsa\HoussemBundle\Entity\Saleoffer $offerid = null)
    {
        
           if (is_null($offerid)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }

        $this->offerid = $offerid;

        return $this;
    }

    /**
     * Get offerid
     *
     * @return \Rsa\HoussemBundle\Entity\Saleoffer 
     */
    public function getOfferid()
    {
        return $this->offerid;
    }

    /**
     * Set registereduserid
     *
     * @param \Rsa\HoussemBundle\Entity\Registereduser $registereduserid
     * @return Votesperclientandsaleoffer
     */
    public function setRegistereduserid(\Rsa\HoussemBundle\Entity\Registereduser $registereduserid = null)
    {
        if (is_null($registereduserid)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->registereduserid = $registereduserid;

        return $this;
    }

    /**
     * Get registereduserid
     *
     * @return \Rsa\HoussemBundle\Entity\Registereduser 
     */
    public function getRegistereduserid()
    {
        return $this->registereduserid;
    }
}
