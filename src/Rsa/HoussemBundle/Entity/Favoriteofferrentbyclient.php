<?php

namespace Rsa\HoussemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Favoriteofferrentbyclient
 *
 * @ORM\Table(name="FavoriteOfferRentByClient", indexes={@ORM\Index(name="idOfClient", columns={"idOfClient"}), @ORM\Index(name="idOfOffer", columns={"idOfOffer"})})
 * @ORM\Entity
 */
class Favoriteofferrentbyclient
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="bookmarkTitle", type="string", length=64, nullable=false)
     */
    private $bookmarktitle;

    /**
     * @var \Rentoffer
     *
     * @ORM\ManyToOne(targetEntity="Rentoffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfOffer", referencedColumnName="id")
     * })
     */
    private $idofoffer;

    /**
     * @var \Registereduser
     *
     * @ORM\ManyToOne(targetEntity="Registereduser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfClient", referencedColumnName="id")
     * })
     */
    private $idofclient;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookmarktitle
     *
     * @param string $bookmarktitle
     * @return Favoriteofferrentbyclient
     */
    public function setBookmarktitle($bookmarktitle)
    {
        $this->bookmarktitle = $bookmarktitle;

        return $this;
    }

    /**
     * Get bookmarktitle
     *
     * @return string 
     */
    public function getBookmarktitle()
    {
        return $this->bookmarktitle;
    }

    /**
     * Set idofoffer
     *
     * @param \Rsa\HoussemBundle\Entity\Rentoffer $idofoffer
     * @return Favoriteofferrentbyclient
     */
    public function setIdofoffer(\Rsa\HoussemBundle\Entity\Rentoffer $idofoffer = null)
    {
        if (is_null($idofoffer)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->idofoffer = $idofoffer;

        return $this;
    }

    /**
     * Get idofoffer
     *
     * @return \Rsa\HoussemBundle\Entity\Rentoffer 
     */
    public function getIdofoffer()
    {
        return $this->idofoffer;
    }

    /**
     * Set idofclient
     *
     * @param \Rsa\HoussemBundle\Entity\Registereduser $idofclient
     * @return Favoriteofferrentbyclient
     */
    public function setIdofclient(\Rsa\HoussemBundle\Entity\Registereduser $idofclient = null)
    {
            if (is_null($idofclient)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->idofclient = $idofclient;

        return $this;
    }

    /**
     * Get idofclient
     *
     * @return \Rsa\HoussemBundle\Entity\Registereduser 
     */
    public function getIdofclient()
    {
        return $this->idofclient;
    }
}
