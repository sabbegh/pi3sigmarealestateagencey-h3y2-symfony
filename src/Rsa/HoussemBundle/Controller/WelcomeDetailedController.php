<?php

namespace Rsa\HoussemBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Requeste;
use Symfony\Component\HttpFoundation\Response;

class WelcomeDetailedController extends Controller {

    public function getOffersAjaxAction($RentOffer, $SaleOffer) {
        return $this->render('RsaHoussemBundle::WelcomeAjaxContent.html.twig', array('RentOffer' => $RentOffer, 'SaleOffer' => $SaleOffer));
    }

    public function searchFormAjaxAction() {
        /* On récupère la requete AJAX */
        $request = $this->get('request');


        /* On récupère les champs de la requête AJAX */
        $offertype = $request->request->get('offertype');
        $dealtype = $request->request->get('dealtype');
        $governorate = $request->request->get('governorate');
        $available_year = $request->request->get('available_year');
        $available_month = $request->request->get('available_month');
        $available_day = $request->request->get('available_day');
        $only_show_ecological = $request->request->get('only_show_ecological');
        $max_price_or_rent = $request->request->get('max_price_or_rent');

        /* Testing AJAX parameter passage */

        /* BEGING DEBUG */
//        $responseMessage = "<b><h1>Debugging !</h1></b>";
//        $responseMessage = $responseMessage . '<br> offertype : <b>' . $offertype . '</b> !';
//        $responseMessage = $responseMessage . '<br> dealtype : <b>' . $dealtype . '</b> !';
//        $responseMessage = $responseMessage . '<br> governorate : <b>' . $governorate . '</b> !';
//        $responseMessage = $responseMessage . '<br> available_year : <b>' . $available_year . '</b> !';
//        $responseMessage = $responseMessage . '<br> available_month : <b>' . $available_month . '</b> !';
//        $responseMessage = $responseMessage . '<br> available_day : <b>' . $available_day . '</b> !';
//        $responseMessage = $responseMessage . '<br> only_show_ecological : <b>' . $only_show_ecological . '</b> !';
//        $responseMessage = $responseMessage . '<br> max_price_or_rent : <b>' . $max_price_or_rent . '</b> !';
//        $return = array("responseCode" => 200, "message" => $responseMessage);
//        $return = json_encode($return);
//        return new Response($return, 200, array('Content-Type' => 'application/json'));
        /* END DEBUG */


        $em = $this->getDoctrine()->getManager();

        // DEBUT - AJOUTER LES TRAITEMENTS DQL

        $requete_DQL = "";
        //  *****************************************
        //    PARTIE COMMUNE AUX DEUX TYPES D'OFFRES
        //  *****************************************

        if ($only_show_ecological == 'true') {
            $requete_DQL = $requete_DQL . " AND s.isgreen = true ";
        }

        $requete_DQL = $requete_DQL . " AND s.availablefrom > '"
                . $available_year . "-"
                . $available_month . "-"
                . $available_day . "' ";

        if ($governorate != 'Any') {
            $requete_DQL = $requete_DQL . " AND s.governorate like '% " . $governorate . " %'";
        }

        if ($dealtype != 'Any') {
            $requete_DQL = $requete_DQL . " AND s.dealtype like '% " . $dealtype . " %'";
        }



        //  *****************************************


        if ($offertype == 'RentOffer') {

            if (intval($max_price_or_rent) == 0)
            {
                $requete_DQL = $requete_DQL . " AND s.rentprice BETWEEN 0 AND 1 " ;
            }
            
            else
            {
                $requete_DQL = $requete_DQL . " AND s.rentprice BETWEEN 0 AND " . $max_price_or_rent ;
            }

            $SaleOffer = null;
            $RentOffer = $em->getRepository("RsaHoussemBundle:Rentoffer")->getbystring($requete_DQL);
        } else {

            if (intval($max_price_or_rent) == 0)
            {
                $requete_DQL = $requete_DQL . " AND s.saleprice BETWEEN 0 AND 1 " ;
            }
            
            else
            {
                $requete_DQL = $requete_DQL . " AND s.saleprice BETWEEN 0 AND " . $max_price_or_rent ;
            }

            $RentOffer = null;
            $SaleOffer = $em->getRepository("RsaHoussemBundle:Saleoffer")->getbystring($requete_DQL);
        }

        // FIN - AJOUTER LES TRAITEMENTS DQL

        $template = $this->forward('RsaHoussemBundle:WelcomeDetailed:getOffersAjax', array('RentOffer' => $RentOffer, 'SaleOffer' => $SaleOffer))->getContent();
        $return = array("responseCode" => 200, "message" => $template);
        $return = json_encode($return);
        return new Response($return, 200, array('Content-Type' => 'application/json'));
    }

    public function showDetailedSaleAction($offerId) {
        if (intval($offerId) == 0) {
            $message = '<b> ERROR : Illegal offerId : ' . $offerId . '</b> .';
            $currentUrl = $this->getRequest()->getUri();
            $currentAction = $this->getRequest()->attributes->get('_controller');
            return new Response($message . '<br/> DEBUGGING : Line' . __LINE__ . ' - File : ' . __FILE__ . ' .');
        }

        $em = $this->getDoctrine()->getManager();

        $RentOffer = null;
        $SaleOffer = null;

        $SaleOffer = $em->getRepository("RsaHoussemBundle:Saleoffer")->findById($offerId);

        if ($SaleOffer == null) {
            $message = '<b> ERROR : No such offer ! offerId :' . $offerId . '</b> .';
            $currentUrl = $this->getRequest()->getUri();
            $currentAction = $this->getRequest()->attributes->get('_controller');
            return new Response($message . '<br/> DEBUGGING : Line' . __LINE__ . ' - File : ' . __FILE__ . ' .');
        } else {

            $SecondaryImages = $em->getRepository("RsaHoussemBundle:Secondaryimagessaleoffer")->findBy(array('idofoffer' => '$offerId'));
            return $this->render('RsaHoussemBundle::DetailedOffer.html.twig', array('RentOffer' => $RentOffer, 'SaleOffer' => $SaleOffer, 'SecondaryImages' => $SecondaryImages));
        }
    }

    public function showDetailedRentAction($offerId) {

        if (intval($offerId) == 0) {
            $message = '<b> ERROR : Illegal offerId : ' . $offerId . '</b> .';
            $currentUrl = $this->getRequest()->getUri();
            $currentAction = $this->getRequest()->attributes->get('_controller');
            return new Response($message . '<br/> DEBUGGING : Line' . __LINE__ . ' - File : ' . __FILE__ . ' .');
        }

        $em = $this->getDoctrine()->getManager();

        $RentOffer = null;
        $SaleOffer = null;

        $RentOffer = $em->getRepository("RsaHoussemBundle:Rentoffer")->findById($offerId);

        if ($RentOffer == null) {
            $message = '<b> ERROR : No such offer ! offerId :' . $offerId . '</b> .';
            $currentUrl = $this->getRequest()->getUri();
            $currentAction = $this->getRequest()->attributes->get('_controller');
            return new Response($message . '<br/> DEBUGGING : Line' . __LINE__ . ' - File : ' . __FILE__ . ' .');
        } else {

            $SecondaryImages = $em->getRepository("RsaHoussemBundle:Secondaryimagesrentoffer")->findBy(array('idofoffer' => '$offerId'));
            return $this->render('RsaHoussemBundle::DetailedOffer.html.twig', array('RentOffer' => $RentOffer, 'SaleOffer' => $SaleOffer, 'SecondaryImages' => $SecondaryImages));
        }
    }

        public function requestRealtorAjaxAction() {

        /* On récupère la requete AJAX */
        $request = $this->get('request');

        /* On récupère les champs de la requête AJAX */
        $firstname = $request->request->get('signup_firstname');
        $lastname = $request->request->get('signup_lastname');
        $email = $request->request->get('signup_email');
        $year = $request->request->get('signup_year');
        $month = $request->request->get('signup_month');
        $day = $request->request->get('signup_day');
        $password = $request->request->get('signup_password1');
        $request_text = $request->request->get('request_text');

        /* Date conversion */
        $birthdayString = $year . '-' . $month . '-' . $day;
        $birthday = \DateTime::createFromFormat('Y-m-d', $birthdayString);

        $em = $this->getDoctrine()->getManager();
        $existingUser = $em->getRepository("RsaYassineAccountBundle:Registereduser")->findOneBy(array('email' => $email));

        $responseMessage = '';

        if ($existingUser) {

            $responseMessage = $responseMessage . 'The email <b>' . $email . '</b> is alrady taken !';
            $responseMessage = $responseMessage . '<br> Please try again with another email.';
        } else {

            /* Create the user */
            $realtorRequest = new \Rsa\HoussemBundle\Entity\Realtoraccountcreationrequest();
            $realtorRequest->setFirstname($firstname);
            $realtorRequest->setLastname($lastname);
            $realtorRequest->setEmail($email);
            $realtorRequest->setBirthday($birthday);
            $realtorRequest->setPassword($password);
            // This is the default "no profile picture" image : http://i.imgur.com/Ypqwwg2.png?1
            $realtorRequest->setUserimageurl('http://i.imgur.com/Ypqwwg2.png?1');
            $realtorRequest->setRequesttexte($request_text);
            $realtorRequest->setRequestdate(new \DateTime); // The request has just been submitted

            /* Save him to the database */
            $em->persist($realtorRequest);
            $em->flush();

            /* Give AJAX response to the user */
            $responseMessage = $responseMessage . 'Your request for a <b>realtor</b> account has been successfully sent !<br> An admin will review your application and get back to you soon.';
        }

        $responseMessage = '<p>' . $responseMessage . '</p>';
        $return = array("responseCode" => 200, "message" => $responseMessage);
        $return = json_encode($return);
        return new Response($return, 200, array('Content-Type' => 'application/json'));
    }

    
}
