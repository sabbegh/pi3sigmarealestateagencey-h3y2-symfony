<?php

namespace Rsa\HoussemBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Requeste;
use Symfony\Component\HttpFoundation\Response;

class WelcomeController extends Controller {

    public function RentOfferListeAction() {

        $em = $this->getDoctrine()->getManager();
        $Rentrepository = $em->getRepository("RsaHoussemBundle:Rentoffer");
        
        $RentOffer = $Rentrepository->getAll();
        $SaleOffer = null;
        
        return $this->render('RsaHoussemBundle::Welcome.html.twig', array('RentOffer' => $RentOffer, 'SaleOffer' => $SaleOffer));
    }

    public function SaleOfferListeAction() {

        $RentOffer = null;
        $em = $this->getDoctrine()->getManager();
        $Salerepository = $em->getRepository("RsaHoussemBundle:Saleoffer");        
        $SaleOffer = $Salerepository->getAll();
        

        return $this->render('RsaHoussemBundle::Welcome.html.twig', array('RentOffer' => $RentOffer,
                    'SaleOffer' => $SaleOffer));
    }

   
    
//    public function RentOfferListeSearchAction() {
//       
//       // return new \Symfony\Component\HttpFoundation\Response("le routing fonctionne ");
//       
//        $SaleOffer = null;
//        $em = $this->getDoctrine()->getManager();
//        $Rentrepository = $em->getRepository("RsaHoussemBundle:Rentoffer");
//        /////////////////////////////////////////////////////
//        $searchKeyWord=" AND s.isgreen = false  AND s.availablefrom > '2015-1-1'  AND s.rentprice BETWEEN 0 AND 2200";
//        /////////////////////////////////////////////////////
//        $RentOffer = $Rentrepository->getbystring($searchKeyWord);
//        
//        // $sheets=$repository->getbytype();
//        // var_dump($sheets);die;
//
//        return $this->render('RsaHoussemBundle::Welcome.html.twig', array('RentOffer' => $RentOffer, 'SaleOffer' => $SaleOffer));
//        
//    }
//
//    public function SaleOfferListeSearchAction() {
//       
//       // return new \Symfony\Component\HttpFoundation\Response("le routing fonctionne ");
//       
//        $RentOffer = null;
//        $em = $this->getDoctrine()->getManager();
//        $Salerepository = $em->getRepository("RsaHoussemBundle:Saleoffer");
//        /////////////////////////////////////////////////////
//        $searchKeyWord=" AND s.isgreen = false  AND s.availablefrom > '2015-1-1'  AND s.saleprice BETWEEN 0 AND 2200";
//        /////////////////////////////////////////////////////
//        $SaleOffer = $Salerepository->getbystring($searchKeyWord);
//        
//        // $sheets=$repository->getbytype();
//        // var_dump($sheets);die;
//
//        return $this->render('RsaHoussemBundle::Welcome.html.twig', array('RentOffer' => $RentOffer, 'SaleOffer' => $SaleOffer));
//        
//    }
      
}


