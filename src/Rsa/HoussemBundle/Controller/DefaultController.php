<?php

namespace Rsa\HoussemBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        $name="36.823146,10.9516904";
        return $this->render('RsaHoussemBundle::index.html.twig', array('name' => $name));
    }
}
