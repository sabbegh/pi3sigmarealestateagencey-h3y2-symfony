<?php
// c'est un controleur :)

namespace Rsa\HoussemBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Rsa\HoussemBundle\Form\Handler\RealtorSubscriptionHandler;
use Rsa\HoussemBundle\Form\Type\RealtorSubscriptionType;
use Rsa\HoussemBundle\Entity\Realtoraccountcreationrequest;

class FormController extends Controller {

    function RealtorSubscriptionRequestTypeAction(Request $request) {
        // return new \Symfony\Component\HttpFoundation\ResponseHeaderBag("le routage fonctionne"); 
        
        $formHandler = new RealtorSubscriptionHandler(
                //le fonction si desosus prend comme argument un, formulaire
                //et une request :)
        $this->createForm(
         new RealtorSubscriptionType(), new Realtoraccountcreationrequest()), $request);
        if ($formHandler->process()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($formHandler->getForm()->getData());
            $em->flush();
        }
        return $this->render('RsaHoussemBundle::RealtorSubscriptionRequest.html.twig', array('form' => $formHandler->getForm()->createView()));
    }

}
