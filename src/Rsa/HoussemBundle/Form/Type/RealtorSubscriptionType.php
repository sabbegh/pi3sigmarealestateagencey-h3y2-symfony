<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Rsa\HoussemBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

class RealtorSubscriptionType extends AbstractType {

   public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options) {
      $builder
              
    ->add('firstname',null,array("label"=>"Your First Name :"))
    ->add('lastname',null,array("label"=>"Your Last Name :"))
    ->add('birthday',null,array("label"=>"Your Birthday :"))
    // PASSWORD needs to be encrypte via sha256 hash ? 
    ->add('password',null,array("label"=>"Your Password :"))
              //password verification is needed
    ->add('email',null,array("label"=>"Your E-mail :"))
    ->add('requesttexte',null,array("label"=>"Please Enter Your Request Text : "))
    ->add('save', 'submit', array('label' => 'Send Request'));
    //not mapped attribut
    //->add('userimageurl',null,array("mapped"=>"false"))          
    //->add('requestdate',null,array("mapped"=>"false"))  
    //->add('istreatedbyadmin'),array("label"=>"nomduchamp")
   // ->add('id',null,array("mapped"=>"false"));
      
   }
   
   //bonne pratique il faut faire une recherche//
   public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver) {
     $resolver->setDefaults(array (
         "data_class"=>"Rsa\HoussemBundle\Entity\Realtoraccountcreationrequest"
        )
     );  
   }
    
    
    
//Ce si est le nom de formulaire //
    public function getName() {
        return("RealtorSubscriptionType");
    }

}
