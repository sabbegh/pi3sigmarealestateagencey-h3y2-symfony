<?php

/*
 * l'objective de ce fichier et de soeparer le traitement du formulaire 
 * du formulaire et de controleur
 * l'ideé est f'avoir un controleur non charger de traitement , c'est comme
 * on va créé pour chaque formulaire un vue (Type) et son controleur (handler)
 */

namespace Rsa\HoussemBundle\Form\Handler;

use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class RealtorSubscriptionHandler {

    protected $form;
    protected $request; //pour de la verification des parametres

    public function __construct(Form $form, Request $request) {

        $this->form = $form;
        $this->request = $request;
        
    }
    
    public function process(){
        //c'est une methode de managerment 
        //var_dump($this->request);die;
        $this->form->handleRequest($this->request);
        //=====================================================
        //Inspects the given request and calls
        //{@link submit()} if the form was submitted.
        //Internally, the request is forwarded to the 
        //configured {@link RequestHandlerInterface} instance,
        //which determines whether to submit the form or not.
        //====================================================
        
        if ($this->request->isMethod("post") && $this->form->isValid()) {
            return true;
        }
        return false;
    }
    
    public function getForm() {
        return $this->form;       
    }
    public function onSuccess(){
        
    }
}
