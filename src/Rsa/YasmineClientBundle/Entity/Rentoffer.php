<?php

namespace Rsa\YasmineClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rentoffer
 *
 * @ORM\Table(name="RentOffer", indexes={@ORM\Index(name="idOfRealtor", columns={"idOfRealtor"})})
 * @ORM\Entity(repositoryClass="Rsa\YasmineClientBundle\Entity\RentOfferRepository")
 */
 
class Rentoffer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="rentPrice", type="float", precision=10, scale=0, nullable=false)
     */
    private $rentprice;

    /**
     * @var float
     *
     * @ORM\Column(name="depositValue", type="float", precision=10, scale=0, nullable=false)
     */
    private $depositvalue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="availableFrom", type="date", nullable=false)
     */
    private $availablefrom;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=64, nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=64, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string", length=800, nullable=false)
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="governorate", type="string", nullable=true)
     */
    private $governorate;

    /**
     * @var string
     *
     * @ORM\Column(name="delegation", type="string", length=64, nullable=false)
     */
    private $delegation;

    /**
     * @var string
     *
     * @ORM\Column(name="coordgps", type="string", length=64, nullable=false)
     */
    private $coordgps;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbVotesPlus", type="integer", nullable=false)
     */
    private $nbvotesplus;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbVotesMoins", type="integer", nullable=false)
     */
    private $nbvotesmoins;

    /**
     * @var string
     *
     * @ORM\Column(name="dealtype", type="string", nullable=true)
     */
    private $dealtype;

    /**
     * @var string
     *
     * @ORM\Column(name="houseImageUrl", type="string", length=128, nullable=false)
     */
    private $houseimageurl;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isGreen", type="boolean", nullable=false)
     */
    private $isgreen;

    /**
     * @var \Registereduser
     *
     * @ORM\ManyToOne(targetEntity="Registereduser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfRealtor", referencedColumnName="id")
     * })
     */
    private $idofrealtor;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rentprice
     *
     * @param float $rentprice
     * @return Rentoffer
     */
    public function setRentprice($rentprice)
    {
        $this->rentprice = $rentprice;

        return $this;
    }

    /**
     * Get rentprice
     *
     * @return float 
     */
    public function getRentprice()
    {
        return $this->rentprice;
    }

    /**
     * Set depositvalue
     *
     * @param float $depositvalue
     * @return Rentoffer
     */
    public function setDepositvalue($depositvalue)
    {
        $this->depositvalue = $depositvalue;

        return $this;
    }

    /**
     * Get depositvalue
     *
     * @return float 
     */
    public function getDepositvalue()
    {
        return $this->depositvalue;
    }

    /**
     * Set availablefrom
     *
     * @param \DateTime $availablefrom
     * @return Rentoffer
     */
    public function setAvailablefrom($availablefrom)
    {
        $this->availablefrom = $availablefrom;

        return $this;
    }

    /**
     * Get availablefrom
     *
     * @return \DateTime 
     */
    public function getAvailablefrom()
    {
        return $this->availablefrom;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Rentoffer
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Rentoffer
     */
    public function setState($state)
    {
if ($state === "Ariana" || $state === "Beja"|| $state === "Ben Arous" ||
        $state ==="Bizerte" || $state ==="Gabes" || $state ==="Gafsa" ||
        $state ==="Jendouba" || $state ==="Kairouan" || $state ==="Kasserine" ||
        $state ==="Kebili" || $state ==="Kef" || $state ==="Mahdia"||
        $state ==="Manouba" || $state ==="Medenine" || $state ==="Monastir" ||
        $state ==="Nabeul" || $state ==="Sfax" || $state ==="Sidi Bouzid" || 
        $state ==="Siliana" || $state ==="Sousse" || $state ==="Tataouine" || 
        $state ==="Tozeur" || $state ==="Tunis" || $state ==="Zaghouan"){
    throw new \Psr\Log\InvalidArgumentException("setter for Rentoffer "
            . ".state : ==> $state <== is invalid ");
}       
        
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Rentoffer
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Rentoffer
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set governorate
     *
     * @param string $governorate
     * @return Rentoffer
     */
    public function setGovernorate($governorate)
    {
        $this->governorate = $governorate;

        return $this;
    }

    /**
     * Get governorate
     *
     * @return string 
     */
    public function getGovernorate()
    {
        return $this->governorate;
    }

    /**
     * Set delegation
     *
     * @param string $delegation
     * @return Rentoffer
     */
    public function setDelegation($delegation)
    {
        $this->delegation = $delegation;

        return $this;
    }

    /**
     * Get delegation
     *
     * @return string 
     */
    public function getDelegation()
    {
        return $this->delegation;
    }

    /**
     * Set coordgps
     *
     * @param string $coordgps
     * @return Rentoffer
     */
    public function setCoordgps($coordgps)
    {
        $this->coordgps = $coordgps;

        return $this;
    }

    /**
     * Get coordgps
     *
     * @return string 
     */
    public function getCoordgps()
    {
        return $this->coordgps;
    }

    /**
     * Set nbvotesplus
     *
     * @param integer $nbvotesplus
     * @return Rentoffer
     */
    public function setNbvotesplus($nbvotesplus)
    {
        $this->nbvotesplus = $nbvotesplus;

        return $this;
    }

    /**
     * Get nbvotesplus
     *
     * @return integer 
     */
    public function getNbvotesplus()
    {
        return $this->nbvotesplus;
    }

    /**
     * Set nbvotesmoins
     *
     * @param integer $nbvotesmoins
     * @return Rentoffer
     */
    public function setNbvotesmoins($nbvotesmoins)
    {
        $this->nbvotesmoins = $nbvotesmoins;

        return $this;
    }

    /**
     * Get nbvotesmoins
     *
     * @return integer 
     */
    public function getNbvotesmoins()
    {
        return $this->nbvotesmoins;
    }

    /**
     * Set dealtype
     *
     * @param string $dealtype
     * @return Rentoffer
     */
    public function setDealtype($dealtype)
    {
        if ($state === "Apartement" || $state === "Warehouse"||
            $state === "Empty Lot"  || $state ==="Villa"){
    throw new \Psr\Log\InvalidArgumentException("setter for Rentoffer "
            . ".state : ==> $state <== is invalid ");
}    




        
        $this->dealtype = $dealtype;

        return $this;
    }

    /**
     * Get dealtype
     *
     * @return string 
     */
    public function getDealtype()
    {
        return $this->dealtype;
    }

    /**
     * Set houseimageurl
     *
     * @param string $houseimageurl
     * @return Rentoffer
     */
    public function setHouseimageurl($houseimageurl)
    {
        $this->houseimageurl = $houseimageurl;

        return $this;
    }

    /**
     * Get houseimageurl
     *
     * @return string 
     */
    public function getHouseimageurl()
    {
        return $this->houseimageurl;
    }

    /**
     * Set isgreen
     *
     * @param boolean $isgreen
     * @return Rentoffer
     */
    public function setIsgreen($isgreen)
    {
        $this->isgreen = $isgreen;

        return $this;
    }

    /**
     * Get isgreen
     *
     * @return boolean 
     */
    public function getIsgreen()
    {
        return $this->isgreen;
    }

    /**
     * Set idofrealtor
     *
     * @param \Rsa\YasmineClientBundle\Entity\Registereduser $idofrealtor
     * @return Rentoffer
     */
    public function setIdofrealtor(\Rsa\YasmineClientBundle\Entity\Registereduser $idofrealtor = null)
    {
         if (is_null($idofrealtor)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }

        $this->idofrealtor = $idofrealtor;

        return $this;
    }

    /**
     * Get idofrealtor
     *
     * @return \Rsa\YasmineClientBundle\Entity\Registereduser 
     */
    public function getIdofrealtor()
    {
        return $this->idofrealtor;
    }
}
