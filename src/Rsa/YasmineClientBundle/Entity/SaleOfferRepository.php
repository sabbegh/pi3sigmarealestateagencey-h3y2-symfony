<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Rsa\YasmineClientBundle\Entity;
use Doctrine\ORM\EntityRepository;

class SaleOfferRepository extends EntityRepository{
   

public function findAllSaleOffersDQL()
{
  $query = $this->getEntityManager()->createQuery('SELECT ro FROM RsaYasmineClientBundle:Saleoffer ro');
  $results = $query->getResult();

  return $results;
}

public function findDetailsSaleOffersDQL($idOffer)
{
  $query = $this->getEntityManager()->createQuery('SELECT ro FROM RsaYasmineClientBundle:Saleoffer ro where ro.id=:idOffer')
    ->setParameter('idOffer',$idOffer);
  $results = $query->getResult();

  return $results;
}

public function findOfferDQL($id)
{
    $query=$this->getEntityManager()
  ->createQuery("SELECT ro from  RsaYasmineClientBundle:Saleoffer ro WHERE ro.id= :idOffer")
    ->setParameter('idOffer',$id);
    return $query->getSingleResult();
}

public function findOffersNbVotesDQL($idOffer)
{
  $query = $this->getEntityManager()->createQuery('SELECT ro FROM RsaYasmineClientBundle:Saleoffer ro where ro.id=:idOffer')
    ->setParameter('idOffer',$idOffer);
  $results = $query->getSingleResult();

  return $results;
}


public function updateDQLPlus($idOffer,$nbVotesPlusApres)
{
 
  $query = $this->getEntityManager()->createQuery('UPDATE RsaYasmineClientBundle:Saleoffer ro SET ro.nbvotesplus = :nvNb where ro.id=:idOffer')
    ->setParameter('idOffer',$idOffer)
    ->setParameter('nvNb',$nbVotesPlusApres);

    $query->execute();

}

public function updateDQLMoins($idOffer,$nbVotesMoinsApres)
{
 
  $query = $this->getEntityManager()->createQuery('UPDATE RsaYasmineClientBundle:Saleoffer ro SET ro.nbvotesmoins = :nvNb where ro.id=:idOffer')
    ->setParameter('idOffer',$idOffer)
    ->setParameter('nvNb',$nbVotesMoinsApres);

    $query->execute();

}



}