<?php

namespace Rsa\YasmineClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Votesperclientandrentoffer
 *
 * @ORM\Table(name="VotesPerClientAndRentOffer", indexes={@ORM\Index(name="registeredUserId", columns={"registeredUserId"}), @ORM\Index(name="offerId", columns={"offerId"})})
 * @ORM\Entity(repositoryClass="Rsa\YasmineClientBundle\Entity\VotesperclientandrentofferRepository")
 */
class Votesperclientandrentoffer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="voteIsPlus", type="integer", nullable=false)
     */
    private $voteisplus;

    /**
     * @var \Rentoffer
     *
     * @ORM\ManyToOne(targetEntity="Rentoffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="offerId", referencedColumnName="id")
     * })
     */
    private $offerid;

    /**
     * @var \Registereduser
     *
     * @ORM\ManyToOne(targetEntity="Registereduser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="registeredUserId", referencedColumnName="id")
     * })
     */
    private $registereduserid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set voteisplus
     *
     * @param boolean $voteisplus
     * @return Votesperclientandrentoffer
     */
    public function setVoteisplus($voteisplus)
    {
        $this->voteisplus = $voteisplus;

        return $this;
    }

    /**
     * Get voteisplus
     *
     * @return boolean 
     */
    public function getVoteisplus()
    {
        return $this->voteisplus;
    }

    /**
     * Set offerid
     *
     * @param \Rsa\YasmineClientBundle\Entity\Rentoffer $offerid
     * @return Votesperclientandrentoffer
     */
    
    /*public function setOfferid(\Rsa\YasmineClientBundle\Entity\Rentoffer $offerid = null)
    {
        
           if (is_null($offerid)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->offerid = $offerid;

        return $this;
    }*/
    public function setOfferid($offerid = null)
    {
        
           if (is_null($offerid)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->offerid = $offerid;

        return $this;
    }

    /**
     * Get offerid
     *
     * @return \Rsa\YasmineClientBundle\Entity\Rentoffer 
     */
    public function getOfferid()
    {
        return $this->offerid;
    }

    /**
     * Set registereduserid
     *
     * @param \Rsa\YasmineClientBundle\Entity\Registereduser $registereduserid
     * @return Votesperclientandrentoffer
     */
    
    /*public function setRegistereduserid(\Rsa\YasmineClientBundle\Entity\Registereduser $registereduserid = null)
    {
          if (is_null($registereduserid)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->registereduserid = $registereduserid;

        return $this;
    }*/
    public function setRegistereduserid($registereduserid = null)
    {
          if (is_null($registereduserid)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->registereduserid = $registereduserid;

        return $this;
    }
    
    

    /**
     * Get registereduserid
     *
     * @return \Rsa\YasmineClientBundle\Entity\Registereduser 
     */
    public function getRegistereduserid()
    {
        return $this->registereduserid;
    }
}
