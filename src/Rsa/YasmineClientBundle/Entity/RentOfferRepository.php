<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Rsa\YasmineClientBundle\Entity;
use Doctrine\ORM\EntityRepository;

class RentOfferRepository extends EntityRepository{
   

public function findAllRentOffersDQL()
{
  $query = $this->getEntityManager()->createQuery('SELECT ro FROM RsaYasmineClientBundle:Rentoffer ro');
  $results = $query->getResult();

  return $results;
}

public function findDetailsRentOffersDQL($idOffer)
{
  $query = $this->getEntityManager()->createQuery('SELECT ro FROM RsaYasmineClientBundle:Rentoffer ro where ro.id=:idOffer')
    ->setParameter('idOffer',$idOffer);
  $results = $query->getResult();

  return $results;
}

public function findOfferDQL($id)
{
    $query=$this->getEntityManager()
  ->createQuery("SELECT ro from  RsaYasmineClientBundle:Rentoffer ro WHERE ro.id= :idOffer")
    ->setParameter('idOffer',$id);
    return $query->getSingleResult();
}

public function findOffersNbVotesDQL($idOffer)
{
  $query = $this->getEntityManager()->createQuery('SELECT ro FROM RsaYasmineClientBundle:Rentoffer ro where ro.id=:idOffer')
    ->setParameter('idOffer',$idOffer);
  $results = $query->getSingleResult();

  return $results;
}


public function updateDQLPlus($idOffer,$nbVotesPlusApres)
{
 
  $query = $this->getEntityManager()->createQuery('UPDATE RsaYasmineClientBundle:Rentoffer ro SET ro.nbvotesplus = :nvNb where ro.id=:idOffer')
    ->setParameter('idOffer',$idOffer)
    ->setParameter('nvNb',$nbVotesPlusApres);

    $query->execute();

}

public function updateDQLMoins($idOffer,$nbVotesMoinsApres)
{
 
  $query = $this->getEntityManager()->createQuery('UPDATE RsaYasmineClientBundle:Rentoffer ro SET ro.nbvotesmoins = :nvNb where ro.id=:idOffer')
    ->setParameter('idOffer',$idOffer)
    ->setParameter('nvNb',$nbVotesMoinsApres);

    $query->execute();

}


}