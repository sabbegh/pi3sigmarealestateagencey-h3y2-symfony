<?php

namespace Rsa\YasmineClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Favoriteofferrentbyclient
 *
 * @ORM\Table(name="FavoriteOfferRentByClient", indexes={@ORM\Index(name="idOfClient", columns={"idOfClient"}), @ORM\Index(name="idOfOffer", columns={"idOfOffer"})})
 * @ORM\Entity(repositoryClass="Rsa\YasmineClientBundle\Entity\FavoriteofferrentbyclientRepository")
 */
class Favoriteofferrentbyclient
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="bookmarkTitle", type="string", length=64, nullable=false)
     */
    private $bookmarktitle;

    /**
     * @var \Rentoffer
     *
     * @ORM\ManyToOne(targetEntity="Rentoffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfOffer", referencedColumnName="id")
     * })
     */
    private $idofoffer;

    /**
     * @var \Registereduser
     *
     * @ORM\ManyToOne(targetEntity="Registereduser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfClient", referencedColumnName="id")
     * })
     */
    private $idofclient;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookmarktitle
     *
     * @param string $bookmarktitle
     * @return Favoriteofferrentbyclient
     */
    public function setBookmarktitle($bookmarktitle)
    {
        $this->bookmarktitle = $bookmarktitle;

        return $this;
    }

    /**
     * Get bookmarktitle
     *
     * @return string 
     */
    public function getBookmarktitle()
    {
        return $this->bookmarktitle;
    }

    /**
     * Set idofoffer
     *
     * @param \Rsa\YasmineClientBundle\Entity\Rentoffer $idofoffer
     * @return Favoriteofferrentbyclient
     */
    
    /*public function setIdofoffer(\Rsa\YasmineClientBundle\Entity\Rentoffer $idofoffer = null)
    {
        if (is_null($idofoffer)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->idofoffer = $idofoffer;

        return $this;
    }*/
    public function setIdofoffer($idofoffer)
    {
        if (is_null($idofoffer)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->idofoffer = $idofoffer;

        return $this;
    }

    /**
     * Get idofoffer
     *
     * @return \Rsa\YasmineClientBundle\Entity\Rentoffer 
     */
    public function getIdofoffer()
    {
        return $this->idofoffer;
    }

    /**
     * Set idofclient
     *
     * @param \Rsa\YasmineClientBundle\Entity\Registereduser $idofclient
     * @return Favoriteofferrentbyclient
     */
    
    /*public function setIdofclient(\Rsa\YasmineClientBundle\Entity\Registereduser $idofclient = null)
    {
            if (is_null($idofclient)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->idofclient = $idofclient;

        return $this;
    }*/
    public function setIdofclient($idofclient)
    {
            if (is_null($idofclient)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->idofclient = $idofclient;

        return $this;
    }

    /**
     * Get idofclient
     *
     * @return \Rsa\YasmineClientBundle\Entity\Registereduser 
     */
    public function getIdofclient()
    {
        return $this->idofclient;
    }
}
