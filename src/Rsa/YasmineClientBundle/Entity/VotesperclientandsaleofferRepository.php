<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Rsa\YasmineClientBundle\Entity;
use Doctrine\ORM\EntityRepository;

class VotesperclientandsaleofferRepository extends EntityRepository{
   
public function checkOneVotePerClientDQL($idOffer,$idC)
{
  $query = $this->getEntityManager()->createQuery('SELECT pers FROM RsaYasmineClientBundle:Votesperclientandsaleoffer pers where pers.offerid = :idOffer and pers.registereduserid = :idClient')
    ->setParameter('idOffer',$idOffer)
    ->setParameter('idClient',$idC);
  $results = $query->getResult();

  if($results==null) {return true;}
  
  else {return false;}
}

}