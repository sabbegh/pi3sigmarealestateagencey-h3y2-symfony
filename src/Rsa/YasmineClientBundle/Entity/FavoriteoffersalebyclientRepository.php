<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Rsa\YasmineClientBundle\Entity;
use Doctrine\ORM\EntityRepository;

class FavoriteoffersalebyclientRepository extends EntityRepository{
   

public function findAllMYFavoriteSoDQL($id,$idC)
{
  $query = $this->getEntityManager()->createQuery('SELECT favSO FROM RsaYasmineClientBundle:Favoriteoffersalebyclient favSO where favSO.idofoffer=:idOffer and favSO.idofclient=:idClient')
    ->setParameter('idOffer',$id)
    ->setParameter('idClient',$idC);
  $results = $query->getResult();

  return $results;
}

public function findFavClientDQL($idC)
{
  $query = $this->getEntityManager()->createQuery('SELECT favSO FROM RsaYasmineClientBundle:Favoriteoffersalebyclient favSO where favSO.idofclient=:idC')
    ->setParameter('idC',$idC);
  $results = $query->getResult();

  return $results;
}

public function findMyFavDQL($id)
{
  $query = $this->getEntityManager()->createQuery('SELECT favSO FROM RsaYasmineClientBundle:Favoriteoffersalebyclient favSO where favSO.id=:idFav')
    ->setParameter('idFav',$id);
  $results = $query->getResult();

  return $results;
}

public function updatelMYFavSoDQL($id,$Bkmtitle)
{
  
  $query = $this->getEntityManager()->createQuery('UPDATE RsaYasmineClientBundle:Favoriteoffersalebyclient favSO SET favSO.bookmarktitle = :title where favSO.id=:idfav')
    ->setParameter('idfav',$id)
    ->setParameter('title',$Bkmtitle);
    $query->execute();
}

public function deletelMYFavSoDQL($id)
{
  
  $query = $this->getEntityManager()->createQuery('DELETE RsaYasmineClientBundle:Favoriteoffersalebyclient favSO where favSO.id=:idFav')
    ->setParameter('idFav',$id);
  
    $query->execute();
}

public function findAllMYFavSoDQL($idC)
{
  $query = $this->getEntityManager()->createQuery('SELECT favSO FROM RsaYasmineClientBundle:Favoriteoffersalebyclient favSO where favSO.idofclient=:idClient')
    ->setParameter('idClient',$idC);
  $results = $query->getResult();

  return $results;
}

public function searchMYFavSoByTitleCommDQL($searchT,$idC)
{
   //$query = $this->_em->createQuery('SELECT commRO FROM RsaYasmineClientBundle:Commentaboutofferrent commRO WHERE LOWER(commRO.title) LIKE :LOWER(titleComm) and commRO.idofoffer=:idOffer ORDER BY commRO.date DESC' )
       $query = $this->getEntityManager()->createQuery('SELECT favSO FROM RsaYasmineClientBundle:Favoriteoffersalebyclient favSO WHERE favSO.bookmarktitle LIKE :titleFav and favSO.idofclient=:idC')
       ->setParameter('titleFav','%'.$searchT.'%') 
       ->setParameter('idC',$idC);
   
    $results = $query->getResult();

  return $results;   
}

}