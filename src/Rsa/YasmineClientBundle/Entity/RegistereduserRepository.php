<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Rsa\YasmineClientBundle\Entity;
use Doctrine\ORM\EntityRepository;

class RegistereduserRepository extends EntityRepository{
    
    public function findAllCommentsRoDQL($id)
    {

     $query = $this->getEntityManager()->createQuery('SELECT a FROM RsaYasmineClientBundle:Commentaboutofferrent a JOIN a.idofclient i WHERE i.id =:idO')
       ->setParameter('idO',$id);

   $results = $query->getResult();  
   
  return $results;
  //SELECT a, u FROM Article a JOIN a.utilisateur u WHERE u.age = 25
  
}

public function findClientDQL($idC)
{
    $query=$this->getEntityManager()
  ->createQuery("SELECT r from  RsaYasmineClientBundle:Registereduser r WHERE r.id= :idClient")
    ->setParameter('idClient',$idC);
    return $query->getSingleResult();
}


}
