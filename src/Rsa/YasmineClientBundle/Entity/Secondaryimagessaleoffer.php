<?php

namespace Rsa\YasmineClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Secondaryimagessaleoffer
 *
 * @ORM\Table(name="SecondaryImagesSaleOffer", indexes={@ORM\Index(name="idOfOffer", columns={"idOfOffer"})})
 * @ORM\Entity
 */
class Secondaryimagessaleoffer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="secondaryHouseImageUrl", type="string", length=128, nullable=false)
     */
    private $secondaryhouseimageurl;

    /**
     * @var \Saleoffer
     *
     * @ORM\ManyToOne(targetEntity="Saleoffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfOffer", referencedColumnName="id")
     * })
     */
    private $idofoffer;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set secondaryhouseimageurl
     *
     * @param string $secondaryhouseimageurl
     * @return Secondaryimagessaleoffer
     */
    public function setSecondaryhouseimageurl($secondaryhouseimageurl)
    {
        $this->secondaryhouseimageurl = $secondaryhouseimageurl;

        return $this;
    }

    /**
     * Get secondaryhouseimageurl
     *
     * @return string 
     */
    public function getSecondaryhouseimageurl()
    {
        return $this->secondaryhouseimageurl;
    }

    /**
     * Set idofoffer
     *
     * @param \Rsa\YasmineClientBundle\Entity\Saleoffer $idofoffer
     * @return Secondaryimagessaleoffer
     */
    public function setIdofoffer(\Rsa\YasmineClientBundle\Entity\Saleoffer $idofoffer = null)
    {
        
           if (is_null($idofoffer)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }

        $this->idofoffer = $idofoffer;

        return $this;
    }

    /**
     * Get idofoffer
     *
     * @return \Rsa\YasmineClientBundle\Entity\Saleoffer 
     */
    public function getIdofoffer()
    {
        return $this->idofoffer;
    }
}
