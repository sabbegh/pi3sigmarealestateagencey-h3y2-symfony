<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Rsa\YasmineClientBundle\Entity;
use Doctrine\ORM\EntityRepository;

class CommentaboutofferrentRepository extends EntityRepository{
   

public function findAllCommentsRoDQL($id)
{
  $query = $this->getEntityManager()->createQuery('SELECT commRO FROM RsaYasmineClientBundle:Commentaboutofferrent commRO where commRO.idofoffer=:idOffer ORDER BY commRO.date DESC')
 // $query = $this->_em->createQuery("SELECT commRO FROM RsaYasmineClientBundle:Commentaboutofferrent commRO join commRO.idofclient regist where regist.id='8'");
     ->setParameter('idOffer',$id);
  $results = $query->getResult();

  return $results;
}


public function findAllMYCommentsRoDQL($id,$idC)
{
  $query = $this->getEntityManager()->createQuery('SELECT commRO FROM RsaYasmineClientBundle:Commentaboutofferrent commRO where commRO.idofoffer=:idOffer and commRO.idofclient=:idClient ORDER BY commRO.date DESC')
    ->setParameter('idOffer',$id)
    ->setParameter('idClient',$idC);
  $results = $query->getResult();

  return $results;
}


public function findMyCommentDQL($id)
{
  $query = $this->getEntityManager()->createQuery('SELECT commRO FROM RsaYasmineClientBundle:Commentaboutofferrent commRO where commRO.id=:idComm')
    ->setParameter('idComm',$id);
  $results = $query->getResult();

  return $results;
}

public function updatelMYCommentRoDQL($id,$title,$body,$dateModif)
{
  
  $query = $this->getEntityManager()->createQuery('UPDATE RsaYasmineClientBundle:Commentaboutofferrent commRO SET commRO.title = :title,commRO.body = :body,commRO.modifcationdate = :dateModif where commRO.id=:idComm')
    ->setParameter('idComm',$id)
    ->setParameter('title',$title)
    ->setParameter('body',$body)
    ->setParameter('dateModif',$dateModif);
    $query->execute();
}

public function deletelMYCommentRoDQL($id)
{
  
  $query = $this->getEntityManager()->createQuery('DELETE RsaYasmineClientBundle:Commentaboutofferrent commRO where commRO.id=:idComm')
    ->setParameter('idComm',$id);
  
    $query->execute();
}

public function searchMYCommentsRoByTitleCommDQL($searchT,$idOffer)
{
   //$query = $this->_em->createQuery('SELECT commRO FROM RsaYasmineClientBundle:Commentaboutofferrent commRO WHERE LOWER(commRO.title) LIKE :LOWER(titleComm) and commRO.idofoffer=:idOffer ORDER BY commRO.date DESC' )
       $query = $this->getEntityManager()->createQuery('SELECT commRO FROM RsaYasmineClientBundle:Commentaboutofferrent commRO WHERE commRO.title LIKE :titleComm and commRO.idofoffer=:idOffer ORDER BY commRO.date DESC' )
       ->setParameter('titleComm','%'.$searchT.'%') 
       ->setParameter('idOffer',$idOffer);
   
    $results = $query->getResult();

  return $results;   
}

}
//->setParameter('titleComm','%'.$searchT.'%') 