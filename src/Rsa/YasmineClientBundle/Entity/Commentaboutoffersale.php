<?php

namespace Rsa\YasmineClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commentaboutoffersale
 *
 * @ORM\Table(name="CommentAboutOfferSale", indexes={@ORM\Index(name="idOfClient", columns={"idOfClient"}), @ORM\Index(name="idOfOffer", columns={"idOfOffer"})})
 * @ORM\Entity(repositoryClass="Rsa\YasmineClientBundle\Entity\CommentaboutoffersaleRepository")

 */
class Commentaboutoffersale
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=64, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string", length=800, nullable=false)
     */
    private $body;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifcationDate", type="date", nullable=false)
     */
    private $modifcationdate;

    /**
     * @var \Saleoffer
     *
     * @ORM\ManyToOne(targetEntity="Saleoffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfOffer", referencedColumnName="id")
     * })
     */
    private $idofoffer;

    /**
     * @var \Registereduser
     *
     * @ORM\ManyToOne(targetEntity="Registereduser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfClient", referencedColumnName="id")
     * })
     */
    private $idofclient;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Commentaboutoffersale
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Commentaboutoffersale
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Commentaboutoffersale
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set modifcationdate
     *
     * @param \DateTime $modifcationdate
     * @return Commentaboutoffersale
     */
    public function setModifcationdate($modifcationdate)
    {
        $this->modifcationdate = $modifcationdate;

        return $this;
    }

    /**
     * Get modifcationdate
     *
     * @return \DateTime 
     */
    public function getModifcationdate()
    {
        return $this->modifcationdate;
    }

    /**
     * Set idofoffer
     *
     * @param \Rsa\YasmineClientBundle\Entity\Saleoffer $idofoffer
     * @return Commentaboutoffersale
     */
    public function setIdofoffer(\Rsa\YasmineClientBundle\Entity\Saleoffer $idofoffer = null)
    {
        if (is_null($idofoffer)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        $this->idofoffer = $idofoffer;

        return $this;
    }

    /**
     * Get idofoffer
     *
     * @return \Rsa\YasmineClientBundle\Entity\Saleoffer 
     */
    public function getIdofoffer()
    {
        return $this->idofoffer;
    }

    /**
     * Set idofclient
     *
     * @param \Rsa\YasmineClientBundle\Entity\Registereduser $idofclient
     * @return Commentaboutoffersale
     */
    public function setIdofclient(\Rsa\YasmineClientBundle\Entity\Registereduser $idofclient = null)
    {
            if (is_null($idofclient)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        
        $this->idofclient = $idofclient;

        return $this;
    }

    /**
     * Get idofclient
     *
     * @return \Rsa\YasmineClientBundle\Entity\Registereduser 
     */
    public function getIdofclient()
    {
        return $this->idofclient;
    }
}
