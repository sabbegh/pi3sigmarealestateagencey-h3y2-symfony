<?php

namespace Rsa\YasmineClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historyofviewedrentoffers
 *
 * @ORM\Table(name="HistoryOfViewedRentOffers", indexes={@ORM\Index(name="idOfOffer", columns={"idOfOffer"}), @ORM\Index(name="idOfClient", columns={"idOfClient"})})
 * @ORM\Entity
 */
class Historyofviewedrentoffers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeOfView", type="date", nullable=false)
     */
    private $timeofview;

    /**
     * @var \Rentoffer
     *
     * @ORM\ManyToOne(targetEntity="Rentoffer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfOffer", referencedColumnName="id")
     * })
     */
    private $idofoffer;

    /**
     * @var \Registereduser
     *
     * @ORM\ManyToOne(targetEntity="Registereduser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idOfClient", referencedColumnName="id")
     * })
     */
    private $idofclient;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timeofview
     *
     * @param \DateTime $timeofview
     * @return Historyofviewedrentoffers
     */
    public function setTimeofview($timeofview)
    {
        $this->timeofview = $timeofview;

        return $this;
    }

    /**
     * Get timeofview
     *
     * @return \DateTime 
     */
    public function getTimeofview()
    {
        return $this->timeofview;
    }

    /**
     * Set idofoffer
     *
     * @param \Rsa\YasmineClientBundle\Entity\Rentoffer $idofoffer
     * @return Historyofviewedrentoffers
     */
    public function setIdofoffer(\Rsa\YasmineClientBundle\Entity\Rentoffer $idofoffer = null)
    {
        if (is_null($idofoffer)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        
        $this->idofoffer = $idofoffer;

        return $this;
    }

    /**
     * Get idofoffer
     *
     * @return \Rsa\YasmineClientBundle\Entity\Rentoffer 
     */
    public function getIdofoffer()
    {
        return $this->idofoffer;
    }

    /**
     * Set idofclient
     *
     * @param \Rsa\YasmineClientBundle\Entity\Registereduser $idofclient
     * @return Historyofviewedrentoffers
     */
    public function setIdofclient(\Rsa\YasmineClientBundle\Entity\Registereduser $idofclient = null)
    {
            if (is_null($idofclient)) {
            throw new InvalidArgumentException("The value NULL is unauthorized ");
        }
        
        $this->idofclient = $idofclient;

        return $this;
    }

    /**
     * Get idofclient
     *
     * @return \Rsa\YasmineClientBundle\Entity\Registereduser 
     */
    public function getIdofclient()
    {
        return $this->idofclient;
    }
}
