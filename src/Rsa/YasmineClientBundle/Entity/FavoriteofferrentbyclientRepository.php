<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Rsa\YasmineClientBundle\Entity;
use Doctrine\ORM\EntityRepository;

class FavoriteofferrentbyclientRepository extends EntityRepository{
   

public function findAllMYFavoriteRoDQL($id,$idC)
{
  $query = $this->getEntityManager()->createQuery('SELECT favRO FROM RsaYasmineClientBundle:Favoriteofferrentbyclient favRO where favRO.idofoffer=:idOffer and favRO.idofclient=:idClient')
    ->setParameter('idOffer',$id)
    ->setParameter('idClient',$idC);
  $results = $query->getResult();

  return $results;
}

public function findFavClientDQL($idC)
{
  $query = $this->getEntityManager()->createQuery('SELECT favRO FROM RsaYasmineClientBundle:Favoriteofferrentbyclient favRO where favRO.idofclient=:idC')
    ->setParameter('idC',$idC);
  $results = $query->getResult();

  return $results;
}


public function findMyFavDQL($id)
{
  $query = $this->getEntityManager()->createQuery('SELECT favRO FROM RsaYasmineClientBundle:Favoriteofferrentbyclient favRO where favRO.id=:idComm')
    ->setParameter('idComm',$id);
  $results = $query->getResult();

  return $results;
}

public function updatelMYFavRoDQL($id,$Bkmtitle)
{
  
  $query = $this->getEntityManager()->createQuery('UPDATE RsaYasmineClientBundle:Favoriteofferrentbyclient favRO SET favRO.bookmarktitle = :title where favRO.id=:idfav')
    ->setParameter('idfav',$id)
    ->setParameter('title',$Bkmtitle);
    $query->execute();
}

public function deletelMYFavRoDQL($id)
{
  
  $query = $this->getEntityManager()->createQuery('DELETE RsaYasmineClientBundle:Favoriteofferrentbyclient favRO where favRO.id=:idFav')
    ->setParameter('idFav',$id);
  
    $query->execute();
}

public function findAllMYFavRoDQL($idC)
{
  $query = $this->getEntityManager()->createQuery('SELECT favRO FROM RsaYasmineClientBundle:Favoriteofferrentbyclient favRO where favRO.idofclient=:idClient')
    ->setParameter('idClient',$idC);
  $results = $query->getResult();

  return $results;
}

public function searchMYFavRoByTitleCommDQL($searchT,$idC)
{
   //$query = $this->_em->createQuery('SELECT commRO FROM RsaYasmineClientBundle:Commentaboutofferrent commRO WHERE LOWER(commRO.title) LIKE :LOWER(titleComm) and commRO.idofoffer=:idOffer ORDER BY commRO.date DESC' )
       $query = $this->getEntityManager()->createQuery('SELECT favRO FROM RsaYasmineClientBundle:Favoriteofferrentbyclient favRO WHERE favRO.bookmarktitle LIKE :titleFav and favRO.idofclient=:idC')
       ->setParameter('titleFav','%'.$searchT.'%') 
       ->setParameter('idC',$idC);
   
    $results = $query->getResult();

  return $results;   
}


}