<?php

namespace Rsa\YasmineClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('RsaYasmineClientBundle:Default:index.html.twig', array('name' => $name));
    }
}
