<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Rsa\YasmineClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Rsa\YasmineClientBundle\Entity\Commentaboutofferrent;
use Rsa\YasmineClientBundle\Entity\Rentoffer;
use Rsa\YasmineClientBundle\Entity\RentOfferRepository;
use Rsa\YasmineClientBundle\Entity\Registereduser;
use Rsa\YasmineClientBundle\Entity\RegistereduserRepository;
use Rsa\YasmineClientBundle\Entity\Votesperclientandrentoffer;
use Rsa\YasmineClientBundle\Entity\VotesperclientandrentofferRepository;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\HttpFoundation\Session\Session;

class CommentRentOfferController extends Controller {

    public function chooseTypeAction() { //public function chooseTypeAction($id)

        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
        
        return $this->render('RsaYasmineClientBundle:RentOffer:TypeOfOfferRentSale.html.twig');
    }

    public function listRentOffersAction(Request $request) {
        
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>

        $em = $this->getDoctrine()->getManager();
        $ro = $em->getRepository("RsaYasmineClientBundle:Rentoffer")
                ->findAllRentOffersDQL();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $ro, $request->query->get('page', 1)/* page number */, 5/* limit per page */
        );

        return ($this->render("RsaYasmineClientBundle:RentOffer:ListRentOffers.html.twig"
                        , array('pagination' => $pagination, 'idClient'=> $userId)));
    }

    public function detailedRentOfferAction($id) {

        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>

        $em = $this->getDoctrine()->getManager();
        $roDetails = $em->getRepository("RsaYasmineClientBundle:Rentoffer")
                ->findDetailsRentOffersDQL($id);

        return ($this->render("RsaYasmineClientBundle:RentOffer:DetailedRentOffer.html.twig"
                        , array('roDetails' => $roDetails, 'idClient'=> $userId)));
    }

    public function addCommentAction($id) {
        
        
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>


        //exécution de la méthode get qui est implémentée dans la classe mere Controller
        //appel au service request( objet) qui nous permet de récupérer la requête
        $request = $this->get('request');
        
        //instantiation de CommentRentOffer
        //$date->setDays(new \DateTime());
        //on teste si la requête est porteuse de données ou non 
        if ($request->getMethod() == "POST") {
            $commentRentOffer = new Commentaboutofferrent();
            //Récupération des données
            $title = $request->get('title');
            $body = $request->get('body');

            //Remplissage de l'objet

            $cl = $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Registereduser')->findClientDQL($userId);
            $commentRentOffer->setIdofclient($cl);

            $off = $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Rentoffer')->findOfferDQL($id);
            $commentRentOffer->setIdofoffer($off);

            $commentRentOffer->setDate(new \DateTime());
            $commentRentOffer->setTitle($title);
            $commentRentOffer->setBody($body);
            $commentRentOffer->setModifcationdate(new \DateTime());

          
            //instantiation de l'em
            $em = $this->getDoctrine()->getManager();
            $em->persist($commentRentOffer);
            $em->flush();

             $this->get('session')->getFlashBag()->add(
              'notice',
              'Comment added successfully'); 
        }
        return $this->render('RsaYasmineClientBundle:RentOffer:addComment.html.twig', array(
            'idOffer'=>$id
        ));
    }

    public function listCommentsRentOfferAction($id) {
            
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>


        $em = $this->getDoctrine()->getManager();
        $commRO = $em->getRepository("RsaYasmineClientBundle:Commentaboutofferrent")
                ->findAllCommentsRoDQL($id);

        return ($this->render("RsaYasmineClientBundle:RentOffer:ListCommentRent.html.twig"
                        , array('CommentsRO' => $commRO)));

        //print_r($commRO);exit();

        /*
          $em=$this->getDoctrine()->getManager();
          $commRO=$em->getRepository("RsaYasmineClientBundle:Registereduser")
          ->findAllCommentsRoDQL($id);
          return ($this->render("RsaYasmineClientBundle:RentOffer:ListCommentRent.html.twig"
          ,array('CommentsRO'=>$commRO))); */
    }

    public function listMYCommentsRentOfferAction($id) {
        
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>


        $em = $this->getDoctrine()->getManager();
        $commRO = $em->getRepository("RsaYasmineClientBundle:Commentaboutofferrent")
                ->findAllMYCommentsRoDQL($id, $userId);
        return ($this->render("RsaYasmineClientBundle:RentOffer:ListMYCommentRent.html.twig"
                        , array('CommentsRO' => $commRO)));
    }

    public function modifyCommentsRentOfferAction($id) {
        
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>


        $em = $this->getDoctrine()->getManager();

        $myComm = $em->getRepository("RsaYasmineClientBundle:Commentaboutofferrent")
                ->findMyCommentDQL($id);

        //exécution de la méthode get qui est implémentée dans la classe mere Controller
        //appel au service request( objet) qui nous permet de récupérer la requête
        $request = $this->get('request');
        //instantiation de CommentRentOffer
        $commentRentOffer = new Commentaboutofferrent();
        //$date->setDays(new \DateTime());
        //on teste si la requête est porteuse de données ou non 
        if ($request->getMethod() == "POST") {
            //Récupération des données
            // $date = $request->get('date');
            $title = $request->get('title');
            $body = $request->get('body');


            //Remplissage de l'objet
            $commentRentOffer->setTitle($title);
            $commentRentOffer->setBody($body);
            $commentRentOffer->setModifcationdate(new \DateTime());

            $dateModif = $commentRentOffer->getModifcationdate();

            $em = $this->getDoctrine()->getManager();
            $em->getRepository("RsaYasmineClientBundle:Commentaboutofferrent")
                    ->updatelMYCommentRoDQL($id, $title, $body, $dateModif);

            //Redirection :

            //return $this->redirectToRoute("rsa_yasmine_listRentOffers");

             $this->get('session')->getFlashBag()->add(
              'notice',
              'Comment modified successfully');
        }
        return ($this->render("RsaYasmineClientBundle:RentOffer:ModifyMyCommentRent.html.twig"
                        , array('myCommRO' => $myComm)));
    }

 
    public function deleteCommentsRentOfferAction($id) {
        
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
 
        $em = $this->getDoctrine()->getManager();
        $em->getRepository("RsaYasmineClientBundle:Commentaboutofferrent")
                ->deletelMYCommentRoDQL($id);

        return $this->redirectToRoute("rsa_yasmine_listRentOffers");
    }

    public function searchMYCommentRentOfferAction($idOffer) {

        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>

        //RECHERCHE NORMALE      

        $em = $this->getDoctrine()->getManager();

        $commRO = $em->getRepository('RsaYasmineClientBundle:Commentaboutofferrent')->findAllCommentsRoDQL($idOffer);
        $request = $this->getRequest();

        if ($request->getMethod() == 'POST') {
            $searchT = $request->get('searchTitle');
            $em = $this->getDoctrine()->getManager();
            $commRO = $em->getRepository("RsaYasmineClientBundle:Commentaboutofferrent")
                    ->searchMYCommentsRoByTitleCommDQL($searchT, $idOffer);
        }

        return ($this->render("RsaYasmineClientBundle:RentOffer:SearchMYTitleComm.html.twig"
                        , array('CommentsRO' => $commRO)));

        //RECHERCHE AJAX 1 - NO

        /*      $request = $this->container->get('request');

          if($request->isXmlHttpRequest())
          {
          $searchT = '';
          $searchT = $request->request->get('searchTitle');

          $em = $this->container->get('doctrine')->getEntityManager();

          if($searchT != '')
          {
          $em=$this->getDoctrine()->getManager();
          $commRO=$em->getRepository("RsaYasmineClientBundle:Commentaboutofferrent")
          ->searchMYCommentsRoByTitleCommDQL($searchT,$idOffer);
          }
          else {
          $commRO=$em->getRepository('RsaYasmineClientBundle:Commentaboutofferrent')->findAllCommentsRoDQL($idOffer);
          }

          return $this->container->get('templating')->renderResponse("RsaYasmineClientBundle:RentOffer:SearchMYTitleComm.html.twig"
          ,array('CommentsRO'=>$commRO));
          }

          else {
          return $this->listCommentsRentOfferAction($idOffer);
          } */
    }

    public function votePlusAction($idOffer, Request $request) {

        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>

        $em = $this->getDoctrine()->getManager();

        $droitVoteOuPas = $em->getRepository("RsaYasmineClientBundle:Votesperclientandrentoffer")
                ->checkOneVotePerClientDQL($idOffer, $userId);



        if ($droitVoteOuPas == true) {

            //Ajout dans la table VotesPerClientAndRentOffer pr garantir l'unicité par la suite

            $votePerClient = new Votesperclientandrentoffer();

            $cl = $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Registereduser')->findClientDQL($userId);
            $votePerClient->setRegistereduserid($cl);

            // $votePerClient->setRegistereduserid($idC);

            $off = $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Rentoffer')->findOfferDQL($idOffer);
            $votePerClient->setOfferid($off);

            //$votePerClient->setOfferid($idOffer);
            $votePerClient->setVoteisplus(1);

            //instantiation de l'em
            $em = $this->getDoctrine()->getManager();
            $em->persist($votePerClient);
            $em->flush();



            //Mise à jour de la table RentOffer
            $findRO = $em->getRepository("RsaYasmineClientBundle:Rentoffer")
                    ->findOffersNbVotesDQL($idOffer);

            $nbVotesPlusAvant = $findRO->getNbVotesPlus();
            $nbVotesPlusApres = $nbVotesPlusAvant + 1;

            $updateRO = $em->getRepository("RsaYasmineClientBundle:Rentoffer")
                    ->updateDQLPlus($idOffer, $nbVotesPlusApres);
        } else {
            $roDetails = $em->getRepository("RsaYasmineClientBundle:Rentoffer")
                    ->findDetailsRentOffersDQL($idOffer);

            return ($this->render("RsaYasmineClientBundle:RentOffer:DetailedRentOfferUniciteVoteError.html.twig"
                            , array('roDetails' => $roDetails, 'idClient'=> $userId)));
        }

        $ro = $em->getRepository("RsaYasmineClientBundle:Rentoffer")
                ->findAllRentOffersDQL();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $ro, $request->query->get('page', 1)/* page number */, 5/* limit per page */
        );

        return ($this->render("RsaYasmineClientBundle:RentOffer:ListRentOffersThanks.html.twig"
                        , array('pagination' => $pagination, 'idClient'=> $userId)));
    }

    public function voteMoinsAction($idOffer, Request $request) {
        
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>

        $em = $this->getDoctrine()->getManager();

        $droitVoteOuPas = $em->getRepository("RsaYasmineClientBundle:Votesperclientandrentoffer")
                ->checkOneVotePerClientDQL($idOffer, $userId);



        if ($droitVoteOuPas == true) {

            //Ajout dans la table VotesPerClientAndRentOffer pr garantir l'unicité par la suite

            $votePerClient = new Votesperclientandrentoffer();

            $cl = $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Registereduser')->findClientDQL($userId);
            $votePerClient->setRegistereduserid($cl);

            // $votePerClient->setRegistereduserid($userId);

            $off = $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Rentoffer')->findOfferDQL($idOffer);
            $votePerClient->setOfferid($off);

            //$votePerClient->setOfferid($idOffer);
            $votePerClient->setVoteisplus(0);

            //instantiation de l'em
            $em = $this->getDoctrine()->getManager();
            $em->persist($votePerClient);
            $em->flush();



            //Mise à jour de la table RentOffer
            $findRO = $em->getRepository("RsaYasmineClientBundle:Rentoffer")
                    ->findOffersNbVotesDQL($idOffer);

            $nbVotesMoinsAvant = $findRO->getNbVotesMoins();
            $nbVotesMoinsApres = $nbVotesMoinsAvant + 1;

            $updateRO = $em->getRepository("RsaYasmineClientBundle:Rentoffer")
                    ->updateDQLMoins($idOffer, $nbVotesMoinsApres);

            $roDetails = $em->getRepository("RsaYasmineClientBundle:Rentoffer")
                    ->findDetailsRentOffersDQL($idOffer);
        } else {//refresh ce composant avec ajax pr dire qu'on a déjà voté!
            $roDetails = $em->getRepository("RsaYasmineClientBundle:Rentoffer")
                    ->findDetailsRentOffersDQL($idOffer);

            return ($this->render("RsaYasmineClientBundle:RentOffer:DetailedRentOfferUniciteVoteError.html.twig"
                            , array('roDetails' => $roDetails, 'idClient'=> $userId)));
        }

        $ro = $em->getRepository("RsaYasmineClientBundle:Rentoffer")
                ->findAllRentOffersDQL();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $ro, $request->query->get('page', 1)/* page number */, 5/* limit per page */
        );

        return ($this->render("RsaYasmineClientBundle:RentOffer:ListRentOffersThanks.html.twig"
                        , array('pagination' => $pagination, 'idClient'=> $userId)));
    }

    public function generatePDFAction($id) {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>

        $file = 'C:\wamp\www\Pidev\web\etudiants\yasmine\PDF_files\\';

        $em = $this->getDoctrine()->getManager();
        $roDetails = $em->getRepository("RsaYasmineClientBundle:Rentoffer")
                ->findDetailsRentOffersDQL($id);
//        if(file_exists ( $file.$id.'.pdf' ))
//    {  
//    /*     $this->get('knp_snappy.pdf')->generateFromHtml(
//           $this->renderView(
//              'RsaYasmineClientBundle:SaleOffer:GeneratePDFSale.html.twig',
//                 array('roDetails'=>$roDetails)
//    ),
//    $file.$id.'-new'.'pdf'
//                   
//);*/
//       return ($this->render("RsaYasmineClientBundle:RentOffer:DetailedRentOfferApresGeneration.html.twig"
//               ,array('roDetails'=>$roDetails)));
//    }
//       
//       else{
        $file_location = 'RentOffer_' . $id . '_' . \rand(0, 99999) . \rand(0, 99999) . '.pdf';

        $this->get('knp_snappy.pdf')->generateFromHtml(
                $this->renderView(
                        'RsaYasmineClientBundle:RentOffer:GeneratePDF.html.twig', array('roDetails' => $roDetails)
                ), $file . $file_location);
//       }    

        $theLink = '../../../etudiants/yasmine/PDF_files/' . $file_location;
        return ($this->render("RsaYasmineClientBundle:RentOffer:DetailedRentOfferApresGeneration.html.twig"
                        , array('roDetails' => $roDetails, 'pdf_link' => $theLink, 'idClient'=> $userId)));
    }

    
    public function generatePieChartVotesAction($id) {
        
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>

        
        $em = $this->getDoctrine()->getManager();
        $findRO = $em->getRepository("RsaYasmineClientBundle:Rentoffer")
                ->findOffersNbVotesDQL($id);

        $nbVotesPlus = $findRO->getNbVotesPlus();

        $nbVotesMoins = $findRO->getNbVotesMoins();


        $ob = new Highchart();
        $ob->chart->renderTo('piechart');
        $ob->title->text('The evaluation of ' . $findRO->getTitle());
        $ob->plotOptions->pie(array(
            'allowPointSelect' => true,
            'cursor' => 'pointer',
            'dataLabels' => array('enabled' => false),
            'showInLegend' => true
        ));
        $data = array(
            array('Number of positive votes', $nbVotesPlus),
            array('Number of negative votes', $nbVotesMoins),
        );
        $ob->series(array(array('type' => 'pie', 'name' => 'Browser share', 'data' => $data)));
        return $this->render('RsaYasmineClientBundle:RentOffer:pieRentOffer.html.twig', array(
                    'chart' => $ob
        ));
    }

}
