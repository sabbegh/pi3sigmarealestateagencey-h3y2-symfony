<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Rsa\YasmineClientBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request; 
use Symfony\Component\HttpFoundation\Response;

use Rsa\YasmineClientBundle\Entity\Rentoffer;
use Rsa\YasmineClientBundle\Entity\RentOfferRepository;

use Rsa\YasmineClientBundle\Entity\Favoriteofferrentbyclient;
use Rsa\YasmineClientBundle\Entity\FavoriteofferrentbyclientRepository;

use Rsa\YasmineClientBundle\Entity\Registereduser;
use Rsa\YasmineClientBundle\Entity\RegistereduserRepository;


class FavoriteRentOfferController extends Controller{
    
    public function addFavoriteRentAction($id)
    {
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
     
         $favoriteRentOffer = new Favoriteofferrentbyclient(); 

        //Remplissage de l'objet
            
            $cl = $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Registereduser')->findClientDQL($userId);
            $favoriteRentOffer->setIdofclient($cl);
            
            $off= $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Rentoffer')->findOfferDQL($id);
            $favoriteRentOffer->setIdofoffer($off);

            $offerTitle= $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Rentoffer')->findOfferDQL($id);
            $title=$offerTitle->getTitle();
            $favoriteRentOffer->setBookmarktitle($title);
 
            //instantiation de l'em
            $em = $this->getDoctrine()->getManager();
            $em->persist($favoriteRentOffer);
            $em->flush();
        
         
       $favRO=$em->getRepository("RsaYasmineClientBundle:Favoriteofferrentbyclient")
       ->findFavClientDQL($userId);
       return ($this->render("RsaYasmineClientBundle:RentOffer:ListMYFavoriteRent.html.twig"
               ,array('FavRO'=>$favRO, 'idClient'=> $userId)));
          }
          
          public function listMYFavoriteRentOfferAction()
          {
              
          // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
            
              
       $em=$this->getDoctrine()->getManager();
       $favRO=$em->getRepository("RsaYasmineClientBundle:Favoriteofferrentbyclient")
              // ->findAllMYFavoriteRoDQL($id,$idC);
               ->findFavClientDQL($userId);
       return ($this->render("RsaYasmineClientBundle:RentOffer:ListMYFavoriteRent.html.twig"
               ,array('FavRO'=>$favRO, 'idClient'=> $userId)));
          }
    
     public function modifyFavoritesRentOfferAction($id){
      
         
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
     
         
        $em=$this->getDoctrine()->getManager();

        $favRO=$em->getRepository("RsaYasmineClientBundle:Favoriteofferrentbyclient")
               ->findMyFavDQL($id);
        
        //exécution de la méthode get qui est implémentée dans la classe mere Controller
        //appel au service request( objet) qui nous permet de récupérer la requête
        $request = $this->get('request');
        //instantiation de CommentRentOffer
        $favoriteRentOffer = new Favoriteofferrentbyclient();      
        //$date->setDays(new \DateTime());
        
        //on teste si la requête est porteuse de données ou non 
        if($request->getMethod()=="POST")
        {          

            $Bkmtitle = $request->get('title');
            
            //Remplissage de l'objet
            $favoriteRentOffer->setBookmarktitle($Bkmtitle);
   
            $em=$this->getDoctrine()->getManager();
            $em->getRepository("RsaYasmineClientBundle:Favoriteofferrentbyclient")
               ->updatelMYFavRoDQL($id,$Bkmtitle);


        return $this->redirectToRoute("rsa_yasmine_listRentOffers");

        }
            return ($this->render("RsaYasmineClientBundle:RentOffer:ModifyMyFavoriteRent.html.twig"
               ,array('FavRO'=>$favRO)));  
 
     }
     
     
     public function deleteFavoritesRentOfferAction($id){
         
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
     
         
        $em=$this->getDoctrine()->getManager();
        $em->getRepository("RsaYasmineClientBundle:Favoriteofferrentbyclient")
               ->deletelMYFavRoDQL($id);

       return $this->redirectToRoute("rsa_yasmine_listRentOffers");
     }
     
    public function searchMYFavoriteRentOfferAction(){

        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
    
    //RECHERCHE NORMALE      
    
      $em =$this->getDoctrine()->getManager();
 
     $favRO=$em->getRepository("RsaYasmineClientBundle:Favoriteofferrentbyclient")
        ->findAllMYFavRoDQL($userId);
      $request=$this->getRequest();
           
             
     if($request->getMethod()=='POST'){
           $searchT = $request->get('searchTitle');
           // $searchT = $request->query->get('searchTitle');
      $em=$this->getDoctrine()->getManager();
       $favRO=$em->getRepository("RsaYasmineClientBundle:Favoriteofferrentbyclient")
               ->searchMYFavRoByTitleCommDQL($searchT,$userId);        
     
     }   

    return ($this->render("RsaYasmineClientBundle:RentOffer:SearchMYTitleFav.html.twig"
      ,array('FavRO'=>$favRO))); 
          
     
     }   
  
}