<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Rsa\YasmineClientBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request; 
use Symfony\Component\HttpFoundation\Response;

use Rsa\YasmineClientBundle\Entity\Commentaboutoffersale;
use Rsa\YasmineClientBundle\Entity\CommentaboutoffersaleRepository;

use Rsa\YasmineClientBundle\Entity\Saleoffer;
use Rsa\YasmineClientBundle\Entity\SaleOfferRepository;

use Rsa\YasmineClientBundle\Entity\Registereduser;
use Rsa\YasmineClientBundle\Entity\RegistereduserRepository;

use Rsa\YasmineClientBundle\Entity\Votesperclientandsaleoffer;
use Rsa\YasmineClientBundle\Entity\VotesperclientandsaleofferRepository;

use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\HttpFoundation\Session\Session;

class CommentSaleOfferController extends Controller{
        
   public function listSaleOffersAction(Request $request){
       
         // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
    
       
       $em=$this->getDoctrine()->getManager();
       $ro=$em->getRepository("RsaYasmineClientBundle:Saleoffer")
               ->findAllSaleOffersDQL();
       
       $paginator=$this->get('knp_paginator');
       $pagination = $paginator->paginate(
        $ro,
        $request->query->get('page', 1)/*page number*/,
        5/*limit per page*/
        );
       
       return ($this->render("RsaYasmineClientBundle:SaleOffer:ListSaleOffers.html.twig"
              ,array('pagination' => $pagination, 'idClient'=> $userId)));
    }
    
    public function detailedSaleOfferAction($id)
    {

         // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>

       $em=$this->getDoctrine()->getManager();
       $roDetails=$em->getRepository("RsaYasmineClientBundle:Saleoffer")
               ->findDetailsSaleOffersDQL($id);
       
       return ($this->render("RsaYasmineClientBundle:Saleoffer:DetailedSaletOffer.html.twig"
               ,array('roDetails'=>$roDetails, 'idClient'=> $userId)));
    
    }
    
    public function addCommentSaleAction($id)
    {
            
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>

        //exécution de la méthode get qui est implémentée dans la classe mere Controller
        //appel au service request( objet) qui nous permet de récupérer la requête
        $request = $this->get('request');
        //instantiation de CommentSaleOffer
         
        //$date->setDays(new \DateTime());
        
        //on teste si la requête est porteuse de données ou non 
        if($request->getMethod()=="POST")
        {        
            $commentSaleOffer = new Commentaboutoffersale();   
            //Récupération des données
            $title = $request->get('title');
            $body = $request->get('body');

           
       /*     
            $idOffer=$id;
            
       $em = $this->getDoctrine()->getManager();

       $clientId = $em->getRepository("RsaYasmineClientBundle:Registereduser")
               ->findClientDQL($idC);
              
       $offerId = $em->getRepository("RsaYasmineClientBundle:Rentoffer")
               ->findDetailsRentOffersDQL($idOffer);
       
       $client[] = $clientId;
       $offer[] = $offerId;
       */
            
        //Remplissage de l'objet
            
            $cl = $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Registereduser')->findClientDQL($userId);
            $commentSaleOffer->setIdofclient($cl);
                       
            $off= $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Saleoffer')->findOfferDQL($id);
            $commentSaleOffer->setIdofoffer($off);
        
            $commentSaleOffer->setDate(new \DateTime());
            $commentSaleOffer->setTitle($title);
            $commentSaleOffer->setBody($body);
            $commentSaleOffer->setModifcationdate(new \DateTime());
            
            //EN VRAI : $commentRentOffer->setModifcationdate(null);
            
            //instantiation de l'em
            $em = $this->getDoctrine()->getManager();
            $em->persist($commentSaleOffer);
            $em->flush();
            
             $this->get('session')->getFlashBag()->add(
              'notice',
              'Comment added successfully'); 
        }
        return $this->render('RsaYasmineClientBundle:SaleOffer:addCommentSale.html.twig', array(
            'idOffer'=>$id
        ));
    }
    
     public function listCommentsSaleOfferAction($id){
         // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
      
       $em=$this->getDoctrine()->getManager();
       $commRO=$em->getRepository("RsaYasmineClientBundle:Commentaboutoffersale")
               ->findAllCommentsRoDQL($id);
       
       return ($this->render("RsaYasmineClientBundle:SaleOffer:ListCommentSale.html.twig"
               ,array('CommentsRO'=>$commRO))); 
           
     //print_r($commRO);exit();
      
    }
    
    public function listMYCommentsSaleOfferAction($id){
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
     
       $em=$this->getDoctrine()->getManager();
       $commRO=$em->getRepository("RsaYasmineClientBundle:Commentaboutoffersale")
               ->findAllMYCommentsRoDQL($id,$userId);
       return ($this->render("RsaYasmineClientBundle:SaleOffer:ListMYCommentSale.html.twig"
               ,array('CommentsRO'=>$commRO)));
    }
    
     public function modifyCommentsSaleOfferAction($id){
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
      
        $em=$this->getDoctrine()->getManager();

        $myComm=$em->getRepository("RsaYasmineClientBundle:Commentaboutoffersale")
               ->findMyCommentDQL($id);
        
        //exécution de la méthode get qui est implémentée dans la classe mere Controller
        //appel au service request( objet) qui nous permet de récupérer la requête
        $request = $this->get('request');
        //instantiation de CommentRentOffer
        $commentSaleOffer = new Commentaboutoffersale();      
        //$date->setDays(new \DateTime());
        
        //on teste si la requête est porteuse de données ou non 
        if($request->getMethod()=="POST")
        {          
            //Récupération des données
           // $date = $request->get('date');
            $title = $request->get('title');
            $body = $request->get('body');
          
            
            //Remplissage de l'objet
            $commentSaleOffer->setTitle($title);
            $commentSaleOffer->setBody($body);
            $commentSaleOffer->setModifcationdate(new \DateTime());

            $dateModif=$commentSaleOffer->getModifcationdate();
            
            $em=$this->getDoctrine()->getManager();
            $em->getRepository("RsaYasmineClientBundle:Commentaboutoffersale")
               ->updatelMYCommentRoDQL($id,$title,$body,$dateModif);

            //Redirection :
            /*  $commRORedirect=$em->getRepository("RsaYasmineClientBundle:Commentaboutofferrent")
               ->findAllCommentsRoDQL($id);*/

          //  return $this->redirectToRoute("rsa_yasmine_listSaleOffers");
/*
       $commRORedirect=$em->getRepository("RsaYasmineClientBundle:Commentaboutofferrent")
               ->findAllCommentsRoDQL($id);
       return ($this->render("RsaYasmineClientBundle:RentOffer:ListCommentRent.html.twig"
               ,array('CommentsRO'=>$commRORedirect)));
*/
              $this->get('session')->getFlashBag()->add(
              'notice',
              'Comment modified successfully');
        }
            return ($this->render("RsaYasmineClientBundle:SaleOffer:ModifyMyCommentSale.html.twig"
               ,array('myCommRO'=>$myComm)));  
 
     }
    
  public function deleteCommentsSaleOfferAction($id){
      
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
 
        $em=$this->getDoctrine()->getManager();
        $em->getRepository("RsaYasmineClientBundle:Commentaboutoffersale")
               ->deletelMYCommentRoDQL($id);

            return $this->redirectToRoute("rsa_yasmine_listSaleOffers");
     }    

     
  public function searchMYCommentSaleOfferAction($idOffer){
      
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
 
   
        $em =$this->getDoctrine()->getManager();
 
      $commRO=$em->getRepository('RsaYasmineClientBundle:Commentaboutoffersale')->findAllCommentsRoDQL($idOffer);
             $request=$this->getRequest();
             
     if($request->getMethod()=='POST'){
           $searchT = $request->get('searchTitle');
           // $searchT = $request->query->get('searchTitle');
      $em=$this->getDoctrine()->getManager();
       $commRO=$em->getRepository("RsaYasmineClientBundle:Commentaboutoffersale")
               ->searchMYCommentsRoByTitleCommDQL($searchT,$idOffer);        
     
     }   

    return ($this->render("RsaYasmineClientBundle:SaleOffer:SearchMYTitleCommSale.html.twig"
      ,array('CommentsRO'=>$commRO))); 
      }
      
       public function votePlusSaleAction($idOffer, Request $request){
       
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
          
            $em=$this->getDoctrine()->getManager();
             
            $droitVoteOuPas=$em->getRepository("RsaYasmineClientBundle:Votesperclientandsaleoffer")
               ->checkOneVotePerClientDQL($idOffer,$userId);
            
            
            
            if($droitVoteOuPas==true){

                //Ajout dans la table VotesPerClientAndRentOffer pr garantir l'unicité par la suite
               
         $votePerClient = new Votesperclientandsaleoffer();
            
          $cl = $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Registereduser')->findClientDQL($userId);
          $votePerClient->setRegistereduserid($cl);
            
       // $votePerClient->setRegistereduserid($idC);
         
          $off= $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Saleoffer')->findOfferDQL($idOffer);
          $votePerClient->setOfferid($off);
            
       //$votePerClient->setOfferid($idOffer);
         $votePerClient->setVoteisplus(1);
            
            //instantiation de l'em
            $em = $this->getDoctrine()->getManager();
            $em->persist($votePerClient);
            $em->flush();
               
               
            
               //Mise à jour de la table RentOffer
                $findRO=$em->getRepository("RsaYasmineClientBundle:Saleoffer")
               ->findOffersNbVotesDQL($idOffer);
                
                $nbVotesPlusAvant=$findRO->getNbVotesPlus();
                $nbVotesPlusApres=$nbVotesPlusAvant+1;
                
                $updateRO=$em->getRepository("RsaYasmineClientBundle:Saleoffer")
               ->updateDQLPlus($idOffer,$nbVotesPlusApres);
            }
                
            
            else {//refresh ce composant avec ajax pr dire qu'on a déjà voté!
                $roDetails=$em->getRepository("RsaYasmineClientBundle:Saleoffer")
               ->findDetailsSaleOffersDQL($idOffer);
                
                return ($this->render("RsaYasmineClientBundle:SaleOffer:DetailedSaleOfferUniciteVoteError.html.twig"
               ,array('roDetails'=>$roDetails, 'idClient'=> $userId)));
                 }
                 
               $ro=$em->getRepository("RsaYasmineClientBundle:Saleoffer")
               ->findAllSaleOffersDQL();
               
       $paginator=$this->get('knp_paginator');
       $pagination = $paginator->paginate(
        $ro,
        $request->query->get('page', 1)/*page number*/,
        5/*limit per page*/
    );
               
       return ($this->render("RsaYasmineClientBundle:SaleOffer:ListSaleOffersThanks.html.twig"
               ,array('pagination'=>$pagination, 'idClient'=> $userId)));
            }
            
            
            
    public function voteMoinsSaleAction($idOffer,Request $request){
          
        
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
                 
        
        $em=$this->getDoctrine()->getManager();
             
        $droitVoteOuPas=$em->getRepository("RsaYasmineClientBundle:Votesperclientandsaleoffer")
               ->checkOneVotePerClientDQL($idOffer,$userId);
            
            
            if($droitVoteOuPas==true){

                //Ajout dans la table VotesPerClientAndRentOffer pr garantir l'unicité par la suite
               
         $votePerClient = new Votesperclientandsaleoffer();
            
          $cl = $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Registereduser')->findClientDQL($userId);
          $votePerClient->setRegistereduserid($cl);
            
       // $votePerClient->setRegistereduserid($idC);
         
          $off= $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Saleoffer')->findOfferDQL($idOffer);
          $votePerClient->setOfferid($off);
            
       //$votePerClient->setOfferid($idOffer);
         $votePerClient->setVoteisplus(0);
            
            //instantiation de l'em
            $em = $this->getDoctrine()->getManager();
            $em->persist($votePerClient);
            $em->flush();
               
               
            
               //Mise à jour de la table RentOffer
                $findRO=$em->getRepository("RsaYasmineClientBundle:Saleoffer")
               ->findOffersNbVotesDQL($idOffer);
                
                $nbVotesMoinsAvant=$findRO->getNbVotesMoins();
                $nbVotesMoinsApres=$nbVotesMoinsAvant+1;
                
                $updateRO=$em->getRepository("RsaYasmineClientBundle:Saleoffer")
               ->updateDQLMoins($idOffer,$nbVotesMoinsApres);
                
                $roDetails=$em->getRepository("RsaYasmineClientBundle:Saleoffer")
               ->findDetailsSaleOffersDQL($idOffer);
       
     
            }
                
            
            else {//refresh ce composant avec ajax pr dire qu'on a déjà voté!
                $roDetails=$em->getRepository("RsaYasmineClientBundle:Saleoffer")
               ->findDetailsSaleOffersDQL($idOffer);
                
                return ($this->render("RsaYasmineClientBundle:SaleOffer:DetailedSaleOfferUniciteVoteError.html.twig"
               ,array('roDetails'=>$roDetails, 'idClient'=> $userId)));
                 }
                 
                $ro=$em->getRepository("RsaYasmineClientBundle:Saleoffer")
               ->findAllSaleOffersDQL();
                
       $paginator=$this->get('knp_paginator');
       $pagination = $paginator->paginate(
        $ro,
        $request->query->get('page', 1)/*page number*/,
        5/*limit per page*/
       );
       
       return ($this->render("RsaYasmineClientBundle:SaleOffer:ListSaleOffersThanks.html.twig"
               ,array('pagination'=>$pagination, 'idClient'=> $userId)));

            }
      /*      
     public function chargerPDFSaleAction($id){
      
       $em=$this->getDoctrine()->getManager();
       $roDetails=$em->getRepository("RsaYasmineClientBundle:Saleoffer")
               ->findDetailsSaleOffersDQL($id);
       
       return ($this->render("RsaYasmineClientBundle:SaleOffer:GeneratePDFSale.html.twig"
               ,array('roDetails'=>$roDetails)));   
         
     }       
       */     
    public function generatePDFSaleAction($id){
        
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>

        $file = 'C:\wamp\www\Pidev\web\etudiants\yasmine\PDF_files\\';
      
      $em=$this->getDoctrine()->getManager();
       $roDetails=$em->getRepository("RsaYasmineClientBundle:Saleoffer")
               ->findDetailsSaleOffersDQL($id);
    
 
  //  if(file_exists ( $file.$id.'.pdf' ))
  //  {  
    /*     $this->get('knp_snappy.pdf')->generateFromHtml(
           $this->renderView(
              'RsaYasmineClientBundle:SaleOffer:GeneratePDFSale.html.twig',
                 array('roDetails'=>$roDetails)
    ),
    $file.$id.'-new'.'pdf'
                   
);*/
   //       return ($this->render("RsaYasmineClientBundle:SaleOffer:DetailedSaleOfferApresGeneration.html.twig"
     //          ,array('roDetails'=>$roDetails)));
   // }
       
       // else 
       // {  
            $file_location = 'SaleOffer_' . $id . '_' . \rand(0, 99999) . \rand(0, 99999) . '.pdf';
   
           $this->get('knp_snappy.pdf')->generateFromHtml(
           $this->renderView(
              'RsaYasmineClientBundle:SaleOffer:GeneratePDFSale.html.twig',
                 array('roDetails'=>$roDetails)
    ), $file . $file_location
                   
);
   // }       
/*   
$pageUrl = $this->generateUrl('rsa_yasmine_detailedRentOffer', array('id'=>$id), true); // use absolute path!

return new Response(
    $this->get('knp_snappy.pdf')->getOutput($pageUrl),
    200,
    array(
        'Content-Type'          => 'application/pdf',
        'Content-Disposition'   => 'attachment; filename="file.pdf"'
    )
);
*/

        $theLink = '../../../etudiants/yasmine/PDF_files/' . $file_location;
       return ($this->render("RsaYasmineClientBundle:SaleOffer:DetailedSaleOfferApresGeneration.html.twig"
               ,array('roDetails'=>$roDetails, 'pdf_link' => $theLink, 'idClient'=> $userId)));
        
       }            
 
    public function generatePieChartVotesSaleAction($id)
    {
          // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>      
        $em=$this->getDoctrine()->getManager();
        $findRO=$em->getRepository("RsaYasmineClientBundle:Saleoffer")
               ->findOffersNbVotesDQL($id);
                
                $nbVotesPlus=$findRO->getNbVotesPlus();

                $nbVotesMoins=$findRO->getNbVotesMoins();

             
    $ob = new Highchart();
    $ob->chart->renderTo('piechart');
    $ob->title->text('The level of appreciation of '.$findRO->getTitle());
$ob->plotOptions->pie(array(
'allowPointSelect' => true,
'cursor' => 'pointer',
'dataLabels' => array('enabled' => false),
'showInLegend' => true
));
$data = array(
array('Number of positive votes', $nbVotesPlus),
array('Number of negative votes', $nbVotesMoins),

);
$ob->series(array(array('type' => 'pie','name' => 'Browser share', 'data' => $data)));
return $this->render('RsaYasmineClientBundle:SaleOffer:pieSaleOffer.html.twig', array(
'chart' => $ob
));
} 
    
}