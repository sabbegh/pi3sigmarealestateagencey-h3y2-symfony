<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Rsa\YasmineClientBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request; 
use Symfony\Component\HttpFoundation\Response;

use Rsa\YasmineClientBundle\Entity\Saleoffer;
use Rsa\YasmineClientBundle\Entity\SaleOfferRepository;

use Rsa\YasmineClientBundle\Entity\Favoriteoffersalebyclient;
use Rsa\YasmineClientBundle\Entity\FavoriteoffersalebyclientRepository;

use Rsa\YasmineClientBundle\Entity\Registereduser;
use Rsa\YasmineClientBundle\Entity\RegistereduserRepository;


class FavoriteSaleOfferController extends Controller{
    
    public function addFavoriteSaleAction($id)
    {
         // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
         
         $favoriteSaleOffer = new Favoriteoffersalebyclient(); 

        //Remplissage de l'objet
            
            $cl = $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Registereduser')->findClientDQL($userId);
            $favoriteSaleOffer->setIdofclient($cl);
            
            $off= $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Saleoffer')->findOfferDQL($id);
            $favoriteSaleOffer->setIdofoffer($off);

            $offerTitle= $this->getDoctrine()->getManager()->getRepository('RsaYasmineClientBundle:Saleoffer')->findOfferDQL($id);
            $title=$offerTitle->getTitle();
            $favoriteSaleOffer->setBookmarktitle($title);
 
            //instantiation de l'em
            $em = $this->getDoctrine()->getManager();
            $em->persist($favoriteSaleOffer);
            $em->flush();
        
         
       $favSO=$em->getRepository("RsaYasmineClientBundle:Favoriteoffersalebyclient")
       ->findFavClientDQL($userId);
       return ($this->render("RsaYasmineClientBundle:SaleOffer:ListMYFavoriteSale.html.twig"
               ,array('FavSO'=>$favSO, 'idClient'=> $userId)));
          }
          
          public function listMYFavoriteSaleOfferAction()
          {
              
          // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
     
       $em=$this->getDoctrine()->getManager();
       $favSO=$em->getRepository("RsaYasmineClientBundle:Favoriteoffersalebyclient")
              // ->findAllMYFavoriteRoDQL($id,$idC);
               ->findFavClientDQL($userId);
       return ($this->render("RsaYasmineClientBundle:SaleOffer:ListMYFavoriteSale.html.twig"
               ,array('FavSO'=>$favSO, 'idClient'=> $userId)));
          }
    
     public function modifyFavoritesSaleOfferAction($id){
      
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
     
        $em=$this->getDoctrine()->getManager();

        $favSO=$em->getRepository("RsaYasmineClientBundle:Favoriteoffersalebyclient")
               ->findMyFavDQL($id);
        
        //exécution de la méthode get qui est implémentée dans la classe mere Controller
        //appel au service request( objet) qui nous permet de récupérer la requête
        $request = $this->get('request');
        //instantiation de CommentSaleOffer
        $favoriteSaleOffer = new Favoriteoffersalebyclient();      
        //$date->setDays(new \DateTime());
        
        //on teste si la requête est porteuse de données ou non 
        if($request->getMethod()=="POST")
        {          

            $Bkmtitle = $request->get('title');
            
            //Remplissage de l'objet
            $favoriteSaleOffer->setBookmarktitle($Bkmtitle);
   
            $em=$this->getDoctrine()->getManager();
            $em->getRepository("RsaYasmineClientBundle:Favoriteoffersalebyclient")
               ->updatelMYFavSoDQL($id,$Bkmtitle);


        return $this->redirectToRoute("rsa_yasmine_listSaleOffers");

        }
            return ($this->render("RsaYasmineClientBundle:SaleOffer:ModifyMyFavoriteSale.html.twig"
               ,array('FavSO'=>$favSO)));  
 
     }
     
     
     public function deleteFavoritesSaleOfferAction($id){
             
        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
     
         $em=$this->getDoctrine()->getManager();
        $em->getRepository("RsaYasmineClientBundle:Favoriteoffersalebyclient")
               ->deletelMYFavSoDQL($id);

       return $this->redirectToRoute("rsa_yasmine_listSaleOffers");
     }
     
    public function searchMYFavoriteSaleOfferAction(){

        // <<<<<<<=================================================================>>>>>>>
        $session = $this->get('session');

        if (!$session->has('connectedUserId')) {
            return $this->render('RsaYassineAccountBundle:Auth:auth.html.twig', array());
        }

        $userId = $session->get('connectedUserId');
        // <<<<<<<=================================================================>>>>>>>
    
    //RECHERCHE NORMALE      
    
      $em =$this->getDoctrine()->getManager();
 
     $favSO=$em->getRepository("RsaYasmineClientBundle:Favoriteoffersalebyclient")
        ->findAllMYFavSoDQL($userId);
      $request=$this->getRequest();
           
             
     if($request->getMethod()=='POST'){
           $searchT = $request->get('searchTitle');
           // $searchT = $request->query->get('searchTitle');
      $em=$this->getDoctrine()->getManager();
       $favSO=$em->getRepository("RsaYasmineClientBundle:Favoriteoffersalebyclient")
               ->searchMYFavSoByTitleCommDQL($searchT,$userId);        
     
     }   

    return ($this->render("RsaYasmineClientBundle:SaleOffer:SearchMYTitleFavSale.html.twig"
      ,array('FavSO'=>$favSO))); 
          
     
     }   
  
}